﻿using System.Collections;
using UnityEngine;

public class CompleteCameraController : MonoBehaviour {

	public GameObject player;
	public GameObject leftPoint;
	private Vector3 offset;
	private float leftBorder;
	private float rightBorder;
	private float screenWidth = 19.2f;

	void Start () {
		offset = transform.position - player.transform.position;
		leftBorder = leftPoint.transform.position.x;
		rightBorder = leftPoint.transform.position.x + screenWidth;
	}

	void LateUpdate () {

		if (player.transform.position.x >= rightBorder) {
			leftBorder = leftBorder + screenWidth;
			rightBorder = rightBorder + screenWidth;
		}

		if (player.transform.position.x >= leftBorder)
			transform.position = player.transform.position + offset;

	}

}
