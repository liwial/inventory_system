﻿using System.Collections.Generic;
using UnityEngine;


public class LevelManager : MonoBehaviour {

    [SerializeField] private string parentName;

	public GameObject spawnPoint;
	public GameObject ground;

	public GameObject npcHolder; 

	private List<GameObject> npcList = new List<GameObject>();
	private int listIndex = 0;

	void Start() {

        SetNPCList();

    }

	void Update () {

		CheckSpawn ();

	}

	private void SetNPCList () { 

        List<GameObject> npcPrefabList = new List<GameObject>(npcHolder.GetComponent<NPCHandling>().NPCList);

        foreach (var n in npcPrefabList)
            SetNPCObject(n, parentName);

    }

	private void SetNPCObject (GameObject prefab, string parentName) {

		GameObject npcObj = (GameObject)Instantiate (prefab, spawnPoint.transform.position, Quaternion.identity);
		npcObj.transform.SetParent (GameObject.Find (parentName).transform, true);
        npcObj.name = prefab.transform.GetChild (0).name;
		npcObj.SetActive (false);
		npcList.Add (npcObj);

	}
		
	private void CheckSpawn() {

		//check if ready to spawn new npc
		if (ground.GetComponent<ScrollingScript> ().WasChange) {

			//object pooling
			PrepareNPCS();

			ground.GetComponent<ScrollingScript> ().WasChange = false;
		}

	}

	public void PrepareNPCS() {

		int index = -1;

		if (!npcList[listIndex].activeInHierarchy) {
			npcList[listIndex].transform.position = spawnPoint.transform.position;
			npcList[listIndex].SetActive (true);
			index = listIndex;
			listIndex++;
			if (listIndex >= npcList.Count)
				listIndex = 0;
		}

		if (index == -1)
			return;

		if (index > 1 && index < npcList.Count) {
			if (npcList [index - 2].activeInHierarchy)
				npcList [index - 2].SetActive (false);
		} else if (index == 0) {
			if (npcList [npcList.Count - 2].activeInHierarchy)
				npcList [npcList.Count - 2].SetActive (false);
		} else if (index == 1) {
			if (npcList [npcList.Count - 1].activeInHierarchy)
				npcList [npcList.Count - 1].SetActive (false);
		}

	}

}
