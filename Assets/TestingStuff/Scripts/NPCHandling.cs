﻿using System.Collections.Generic;
using UnityEngine;

public class NPCHandling : MonoBehaviour {

	[SerializeField] private List<GameObject> npcList;

	public List<GameObject> NPCList {
		get { return npcList; }
	}

}
