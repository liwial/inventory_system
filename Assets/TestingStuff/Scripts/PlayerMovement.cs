﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {

	[SerializeField] private float speed = 20f;
	[SerializeField] private PlayerController controller;
	[SerializeField] private Animator animator;

	private float horizontalMove = 0f;
	bool jump = false;

	void Update () {

		horizontalMove = Input.GetAxis ("Horizontal") * speed;

		animator.SetFloat ("Speed", Mathf.Abs (horizontalMove));

		if (Input.GetButtonDown ("Jump")) {
			//animator.SetBool ("IsJumping", true);
			jump = true;
		}

	}

	void FixedUpdate() {
		controller.Move (horizontalMove * Time.deltaTime, jump);
		jump = false;
	}

	public void OnLanding() {
		//animator.SetBool ("IsJumping", false);
	}

}
