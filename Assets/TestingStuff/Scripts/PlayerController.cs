﻿using UnityEngine;
using UnityEngine.Events;

public class PlayerController : MonoBehaviour {

	//[SerializeField] private float jumpForce = 400f;
	[Range(0, 0.3f)] [SerializeField] private float movementSmoothing = 0.05f;
	[SerializeField] private bool airControl = false;
	[SerializeField] private LayerMask whatIsGround;
	[SerializeField] private Transform groundCheck;
	[SerializeField] private Transform ceillingCheck;

	const float groundedRadius = 0.2f;
	private bool grounded;
	const float ceillingRadius = 0.2f;

	private Rigidbody2D myRigidbody2D;
	private bool facingRight = true;
	private Vector3 velocity = Vector3.zero;

	[Header("Events")]
	[Space]

	public UnityEvent OnLandEvent;


	void Awake() {
		myRigidbody2D = GetComponent<Rigidbody2D> ();

		if (OnLandEvent == null)
			OnLandEvent = new UnityEvent ();
	}

	private void FixedUpdate() {

		bool wasGrounded = grounded;
		grounded = false;

		Collider2D[] colliders = Physics2D.OverlapCircleAll (groundCheck.position, groundedRadius, whatIsGround);

		for (int i = 0; i < colliders.Length; i++) {
			if (colliders [i].gameObject != gameObject) {
				grounded = true;

				if (!wasGrounded)
					OnLandEvent.Invoke ();
			}
		}

	}

	public void Move(float move, bool jump) {

		if (grounded || airControl) {

			Vector3 targetVelocity = new Vector2 (move * 10f, myRigidbody2D.velocity.y);

			myRigidbody2D.velocity = Vector3.SmoothDamp (myRigidbody2D.velocity, targetVelocity, ref velocity, movementSmoothing);

			if (move > 0 && !facingRight)
				Flip ();
			else if (move < 0 && facingRight)
				Flip ();

		}

		if (grounded && jump) {
			grounded = false;
			//myRigidbody2D.AddForce (new Vector2(0, jumpForce));
		}

	}

	private void Flip() {
		facingRight = !facingRight;

		Vector3 theScale = transform.localScale;
		theScale.x *= -1;
		transform.localScale = theScale;
	}

}
