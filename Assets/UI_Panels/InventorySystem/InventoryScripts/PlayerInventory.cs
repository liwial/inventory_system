﻿using UIPanels.Trade;
using UnityEngine;

public class PlayerInventory : MonoBehaviour {

	[HideInInspector]
	public Inventory inventory;

	private ChestInventory chest;
	private Collider2D chestCollider;
	//private Collider2D enemyCollider;


	void Start () {
		inventory = Inventory.Instance;
		chestCollider = null;
		//enemyCollider = null;
	}

	void Update () {

		//if 'F' key pressed check if chest can be opened 
		if (Input.GetKeyDown (KeyCode.F)) {

			if (chestCollider != null && chestCollider.tag == "Chest") {

				if (!chestCollider.GetComponentInParent<ChestScript> ().IsAlive()) {
					InventoryManager.Instance.PromptObject.SetActive (false);

					chestCollider.GetComponentInParent<ChestScript> ().UpdateChestLayout ();

					if (chest != null) {
						if (chest.CanvasGroup.alpha == 0 || chest.CanvasGroup.alpha == 1)
							chest.Open ();
					}
				}
			} else if (chestCollider != null && chestCollider.tag == "ChestSpecial") {
				if (!chestCollider.GetComponentInParent<ChestScriptSpecial> ().IsAlive()) {
					InventoryManager.Instance.PromptObject.SetActive (false);

					chestCollider.GetComponentInParent<ChestScriptSpecial> ().UpdateChestLayout ();

					if (chest != null) {
						if (chest.CanvasGroup.alpha == 0 || chest.CanvasGroup.alpha == 1)
							chest.Open ();
					}
				}
			}
		}

		//if 'T' key pressed check if trader window can be opened 
		if (Input.GetKeyDown (KeyCode.T) && (InventoryManager.Instance.GameUIStates == UIStates.DEFAULT || InventoryManager.Instance.GameUIStates == UIStates.TRADE)) {

			if (chestCollider != null && chestCollider.tag == "Trader") {

                TradeManager.Instance.SetVisibility();

			}
		}
	}

	private void OnTriggerEnter2D(Collider2D other) {

		if (other.GetType () == typeof(BoxCollider2D)) {

			if (other.tag == "Chest") {
				chest = other.GetComponentInParent<ChestScript> ().ChestInventory;
				chestCollider = other;
			}

			if (other.tag == "ChestSpecial") {
				chest = other.GetComponentInParent<ChestScriptSpecial> ().ChestInventory;
				chestCollider = other;
			}
			
			if (other.tag == "Trader") {
				chestCollider = other;
			}
		} else if (other.GetType () == typeof(CircleCollider2D)) {

			//for testing
			//if (other.tag == "Enemy") {
			//	enemyCollider = other;
			//}
		}
	}
		
	private void OnTriggerExit2D(Collider2D other) {

		if (other.GetType () == typeof(BoxCollider2D)) {

			if (other.tag == "Chest" || other.tag == "ChestSpecial") {

				if (chest != null && chest.IsOpen)
					chest.Open ();

				chest = null;

				chestCollider = null;
			}

			if (other.tag == "Trader") {

				chestCollider = null;

			}
		} else if (other.GetType () == typeof(CircleCollider2D)) {

			//for testing
			//if (other.tag == "Enemy") {
			//	enemyCollider = null;
			//}

		}
	}

}
