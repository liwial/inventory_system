﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ChestInventory : Inventory {

	private List<Stack<ItemScript>> chestItems;
	private int chestSlots;

	private static ChestInventory chestInstance;
	public static ChestInventory ChestInstance {
		get { return chestInstance; }
	}

	#region Protected Methods
	protected override void Awake() {
		columns = 5; 
		rows = 4;
		fadeTime = 0.2f;

		if (chestInstance != null && chestInstance != this) {
			Destroy(this);
		} else {
			chestInstance = (ChestInventory)this;
		}
	}

	protected override void CreateLayout(string layer = "front") {

		this.chestSlots = rows * columns;

		if (layer == "front") {

			allGeneralSlots = new List<GameObject> (); 

			for (int y = 0; y < rows; y++) {
				for (int x = 0; x < columns; x++) {

					GameObject newSlot = (GameObject)Instantiate (InventoryManager.Instance.slotChestPrefab);

					int tmpRow = y + 1;
					int tmpCol = x + 1;
					newSlot.name = "ChestSlot" + tmpRow.ToString () + tmpCol.ToString ();
					newSlot.transform.SetParent (this.GetComponent<Transform> ());

					allGeneralSlots.Add (newSlot);

					newSlot.GetComponent<Button> ().onClick.AddListener
					(
						delegate {
							MoveItem (newSlot);
						}
					);

				}
			}
		}
	}

	protected override IEnumerator FadeOut() {
		yield return StartCoroutine (base.FadeOut());

		MoveItemsToChest ();
	}
	#endregion

	#region Public Methods
	public void UpdateLayout(List<Stack<ItemScript>> items) {
		
		this.chestItems = items;

		inventoryWidth = columns * slotSize;
		inventoryHeight = rows * slotSize;

		inventoryRect = GetComponent<RectTransform> ();
		inventoryRect.SetSizeWithCurrentAnchors (RectTransform.Axis.Horizontal, inventoryWidth);
		inventoryRect.SetSizeWithCurrentAnchors (RectTransform.Axis.Vertical, inventoryHeight);

		int index = 0;

		for (int y = 0; y < rows; y++) {

			for (int x = 0; x < columns; x++) {

				GameObject newSlot = allGeneralSlots[index];

				RectTransform slotRect = newSlot.GetComponent<RectTransform> ();

				newSlot.transform.SetParent (this.GetComponent<Transform>());

				slotRect.localPosition = new Vector3 ((slotSize * x), - (slotSize * y));
				slotRect.localScale = new Vector3 (1.0f, 1.0f, 0);

				if (items.Count != 0 && items.Count >= index && items[index].Count > 0) {
					newSlot.GetComponent<SlotChest> ().AddItems (items [index]);
				}

				index++;
			}
		}
	}

	public override void Open() {
		base.Open ();

		if (IsOpen) {
			MoveItemsFromChest ();
			InventoryManager.Instance.ChestInventoryHolder.transform.GetChild (0).gameObject.SetActive (true);
		} else if (!IsOpen) {
			InventoryManager.Instance.ChestInventoryHolder.transform.GetChild (0).gameObject.SetActive (false);
		}

	}

	public override void ShowToolTip (GameObject slot) {

		if (!IsOpen) {

			Slot tmpSlot = slot.GetComponent<Slot> ();

			if (!tmpSlot.IsEmpty && InventoryManager.Instance.HoverObject == null) {

				InventoryManager.Instance.VisualTextObject.text = tmpSlot.CurrentItem.GetTooltip ();
				InventoryManager.Instance.SizeTextObject.text = InventoryManager.Instance.VisualTextObject.text;

				InventoryManager.Instance.TooltipObject.SetActive (true);

				float xPos = tmpSlot.transform.position.x + 0.05f;
				float yPos = tmpSlot.transform.position.y - 0.605f; 

				InventoryManager.Instance.TooltipObject.transform.position = new Vector3 (xPos, yPos, tmpSlot.transform.position.z);
			}

		}

	}

	public void MoveItemsToChest() {

		if (chestItems != null) {

			chestItems.Clear ();

			for (int i = 0; i < chestSlots; i++) {
				SlotChest tmpSlot = allGeneralSlots [i].GetComponent<SlotChest> ();

				if (!tmpSlot.IsEmpty) {
					chestItems.Add (new Stack<ItemScript> (tmpSlot.Items));

					if (!IsOpen)
						tmpSlot.ClearSlot ();
				} else {
					chestItems.Add (new Stack<ItemScript> ());
				}

			}
		}
	}

	public void MoveItemsFromChest() {

		if (chestItems != null) {

			for (int i = 0; i < chestSlots; i++) {
				if (chestItems.Count != 0 && chestItems.Count > i && chestItems[i] != null && chestItems[i].Count > 0) {
					GameObject newSlot = allGeneralSlots [i];
					newSlot.GetComponent<SlotChest> ().AddItems (chestItems [i]);
				}
			}

		}
	}
		
	public override void SaveContent (string contentName){

	}

	public override void LoadContent (string contentName){

	}

	public void CloseChest() {
		if (ChestInventory.ChestInstance.IsOpen)
			ChestInventory.ChestInstance.Open();
		if (Inventory.isMouseInside)
			Inventory.isMouseInside = false;
	}

	public void TakeEverything() {

		foreach (GameObject slotObj in allGeneralSlots) {

			if (!slotObj.GetComponent<SlotChest>().IsEmpty) {
				if (slotObj.GetComponent<SlotChest> ().Items.Peek ().Item.Type != ItemType.GOLD) {
					foreach (ItemScript it in slotObj.GetComponent<SlotChest>().Items)
						Inventory.Instance.AddItem (it);
					slotObj.GetComponent<SlotChest> ().ClearSlot ();
				} else if (slotObj.GetComponent<SlotChest> ().Items.Peek ().Item.Type == ItemType.GOLD) {
					Inventory.Instance.AddGold (slotObj.GetComponent<SlotChest> ().Items.Count);
					slotObj.GetComponent<SlotChest> ().ClearSlot ();
				}
			}
		}
			
		CloseChest ();
	}
		
	public override void MoveItem(GameObject clicked) {

		if (IsSkillSlots ())
			return;

		InventoryManager.Instance.Clicked = clicked;

		if (!InventoryManager.Instance.MovingSlot.IsEmpty) {

			Slot tmp = InventoryManager.Instance.Clicked.GetComponent<SlotChest> ();

			if (tmp.IsEmpty) {
				tmp.AddItems (InventoryManager.Instance.MovingSlot.Items);

				InventoryManager.Instance.MovingSlot.Items.Clear ();
				Destroy (GameObject.Find ("Hover"));
			} else if (!tmp.IsEmpty && InventoryManager.Instance.MovingSlot.CurrentItem.Item.Type == tmp.CurrentItem.Item.Type && tmp.IsAvailable) {
				MergeStacks (InventoryManager.Instance.MovingSlot, tmp);
			}

		}
		else if (InventoryManager.Instance.From == null && !Input.GetKey(KeyCode.LeftShift)) {
			if (!clicked.GetComponent<SlotChest> ().IsEmpty && !GameObject.Find("Hover")) {
				if (clicked.GetComponent<SlotChest> ().Items.Peek ().Item.Type != ItemType.GOLD) {
					InventoryManager.Instance.From = InventoryManager.Instance.Clicked.GetComponent<SlotChest> ();
					InventoryManager.Instance.From.GetComponent<Image> ().color = Color.gray;

					CreateHoverIcon ();

					//idiot-proof tabs
					InventoryManager.Instance.CheckTab ();

				} else if (clicked.GetComponent<SlotChest> ().Items.Peek ().Item.Type == ItemType.GOLD) {
					Inventory.Instance.AddGold (clicked.GetComponent<SlotChest> ().Items.Count);
					clicked.GetComponent<SlotChest> ().ClearSlot ();
				}
			}
		} 
		else if (InventoryManager.Instance.To == null && !Input.GetKey(KeyCode.LeftShift)) {

			if (!InventoryManager.Instance.From.CurrentItem.Item.IsQuestRelated) {

				InventoryManager.Instance.To = InventoryManager.Instance.Clicked.GetComponent<SlotChest> ();
				Destroy (GameObject.Find ("Hover"));

			} else
				Debug.Log ("You cannot move quest related items to chest");
		}


		if (InventoryManager.Instance.To != null && InventoryManager.Instance.From != null) {

			if (!InventoryManager.Instance.To.IsEmpty && InventoryManager.Instance.From.CurrentItem.Item.Type == InventoryManager.Instance.To.CurrentItem.Item.Type && InventoryManager.Instance.To.IsAvailable) {
				MergeStacks (InventoryManager.Instance.From, InventoryManager.Instance.To);
			} else {
				Slot.SwapItems (InventoryManager.Instance.From, InventoryManager.Instance.To);
			}

			InventoryManager.Instance.From.GetComponent<Image> ().color = Color.white;
			InventoryManager.Instance.To = null;
			InventoryManager.Instance.From = null;
			Destroy(GameObject.Find("Hover"));
		}
			
	}
	#endregion
		
}
