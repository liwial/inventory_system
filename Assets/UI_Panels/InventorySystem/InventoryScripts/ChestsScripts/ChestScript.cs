﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChestScript : MonoBehaviour {

	#region Variables
	public MobState mobState;

	//for testing
	public Sprite aliveSprite;
	public Sprite deadSprite;
	//

	protected List<Stack<ItemScript>> allGeneralSlots;
	protected int chestSlots;
	protected bool active;
	#endregion

	#region Properties
	private ChestInventory chestInventory;
	public ChestInventory ChestInventory {
		get { return chestInventory; }
	}
	#endregion
		
	#region Protected Methods
	protected virtual void OnEnable () {
		
		chestSlots = 20;
		allGeneralSlots = new List<Stack<ItemScript>> (chestSlots);
		active = false;

		chestInventory = GameObject.FindObjectOfType<ChestInventory> ();

		LoadRandomChest (1, 6);

		if (mobState == MobState.ALIVE)
			MakeAlive ();
		else if (mobState == MobState.DEAD)
			MakeDead ();
	}

	protected virtual void OnTriggerEnter2D (Collider2D other) {

		if (other.tag == "Player" && !IsAlive()) {

			InventoryManager.Instance.PromptObject.SetActive (true);
			InventoryManager.Instance.PromptObject.GetComponent<RectTransform> ().position = new Vector3(transform.position.x - 1f, transform.position.y - 1f, 0);
			InventoryManager.Instance.PromptObject.GetComponentInChildren<Text>().text = "Press 'F' key \nto open chest";

			if (chestInventory.FadingOut) {
				chestInventory.instantClose = true;
				chestInventory.MoveItemsToChest ();
			}

			active = true;
		}
	}

	protected virtual void OnTriggerExit2D (Collider2D other) {
		if (other.tag == "Player" && !IsAlive()) {
			active = false;
			InventoryManager.Instance.PromptObject.SetActive (false);
		}
	}

	protected virtual void LoadRandomChest (int rangeMin, int rangeMax) {

		int itemQuantity = Random.Range (rangeMin, rangeMax);

		for (int i = 0; i < chestSlots; i++)
			allGeneralSlots.Add (new Stack<ItemScript>());

		BasicItem tmpItem = null;

		for (int i = 0; i < itemQuantity; i++) {

			int randType = Random.Range (0, 6);
			int randomItem;

			GameObject drawnItem = Instantiate (InventoryManager.Instance.itemObject);

			switch (randType) {
			case 0:
				randomItem = Random.Range (0, InventoryManager.Instance.ItemContainer.WeaponsList.Count);
				tmpItem = InventoryManager.Instance.ItemContainer.WeaponsList [randomItem];
				break;
			case 1:
				randomItem = Random.Range (0, InventoryManager.Instance.ItemContainer.ArmorsList.Count);
				tmpItem = InventoryManager.Instance.ItemContainer.ArmorsList[randomItem];
				break;
			case 2:
				randomItem = Random.Range (0, InventoryManager.Instance.ItemContainer.BootsList.Count);
				tmpItem = InventoryManager.Instance.ItemContainer.BootsList[randomItem];
				break;
			case 3:
				randomItem = Random.Range (0, InventoryManager.Instance.ItemContainer.ConsumablesList.Count);
				tmpItem = InventoryManager.Instance.ItemContainer.ConsumablesList[randomItem];
				break;
			case 4:
				randomItem = Random.Range (0, InventoryManager.Instance.ItemContainer.AmmosList.Count);
				tmpItem = InventoryManager.Instance.ItemContainer.AmmosList[randomItem];
				break;
			case 5:
				randomItem = Random.Range (0, InventoryManager.Instance.ItemContainer.SnaresList.Count);
				tmpItem = InventoryManager.Instance.ItemContainer.SnaresList[randomItem];
				break;
			}

			drawnItem.AddComponent<ItemScript> ();
			drawnItem.GetComponent<ItemScript> ().Item = tmpItem;

			allGeneralSlots[i].Push(drawnItem.GetComponent<ItemScript>());

			Destroy (drawnItem);
		}

		//temp for demo
		if (this.name.Contains ("Buc")) {
			GameObject drawnItem = Instantiate (InventoryManager.Instance.itemObject);

			tmpItem = InventoryManager.Instance.ItemContainer.GetPlotRelated ("Dickens Blood");

			drawnItem.AddComponent<ItemScript> ();
			drawnItem.GetComponent<ItemScript> ().Item = tmpItem;

			allGeneralSlots [itemQuantity].Push (drawnItem.GetComponent<ItemScript> ());

			Destroy (drawnItem);

			LoadGoldToChest (itemQuantity+1);
		} else {
			LoadGoldToChest (itemQuantity);
		}
	}
	#endregion

	#region Private Methods
	private void LoadGoldToChest (int position) {
		int goldQuantity = Random.Range (5, 11);

		GameObject goItem = Instantiate (InventoryManager.Instance.itemObject);
		Gold goldItem = new Gold ();

		goItem.AddComponent<ItemScript> ();
		goItem.GetComponent<ItemScript> ().Item = goldItem;

		for (int i = 0; i <= goldQuantity; i++)
			allGeneralSlots[position].Push (goItem.GetComponent<ItemScript>());

		Destroy (goItem);
	}
	#endregion

	#region Public Methods
	public virtual void UpdateChestLayout () {
		chestInventory.UpdateLayout (allGeneralSlots);
	}

	public bool IsAlive() {
		return (mobState == MobState.ALIVE);
	}

	public virtual void MakeDead () {
		mobState = MobState.DEAD;
		transform.GetChild(0).GetComponent<SpriteRenderer> ().sprite = deadSprite;
	}

	//for testing
	public virtual void MakeAlive() {
		allGeneralSlots.Clear ();
		LoadRandomChest (1, 6);

		mobState = MobState.ALIVE;

		transform.GetChild(0).GetComponent<SpriteRenderer> ().sprite = aliveSprite;
	}
	//

	public void SaveContent(string contentName) {

		string content = string.Empty;

		for (int i = 0; i < allGeneralSlots.Count; i++) {

			if (allGeneralSlots [i].Count != 0) {

				ItemScript tmpItemScript = allGeneralSlots [i].Peek();
				int tmpCount = allGeneralSlots [i].Count;

				string type = tmpItemScript.Item.Type.ToString ();
				string name = tmpItemScript.Item.Name;

				string count = tmpCount.ToString ();

				content += i + "-" + type + "-" + name + "-" + count + ";";
			}
		}

		PlayerPrefs.SetString (gameObject.name + "contentName", content);
		PlayerPrefs.Save ();

	}

	public void LoadContent (string contentName) {

		allGeneralSlots = new List<Stack<ItemScript>> ();

		string content = PlayerPrefs.GetString (gameObject.name + "contentName");
		string[] splitContent = content.Split (';');

		for (int i = 0; i < chestSlots; i++)
			allGeneralSlots.Add (new Stack<ItemScript> ());

		if (content != string.Empty) {

			for (int x = 0; x < splitContent.Length - 1; x++) {
				
				string[] splitValues = splitContent [x].Split ('-');
				int index = System.Int32.Parse (splitValues [0]);
				ItemType type = (ItemType)System.Enum.Parse (typeof(ItemType), splitValues [1]);
				string itemName = splitValues [2];
				int amount = System.Int32.Parse (splitValues [3]);

				BasicItem tmpItem = null;

				for (int i = 0; i < amount; i++) {

					GameObject loadedItem = Instantiate (InventoryManager.Instance.itemObject);

					switch (type) {
					case ItemType.WEAPON:
						tmpItem = InventoryManager.Instance.ItemContainer.WeaponsList.Find (basicItem => basicItem.Name == itemName);
						break;
					case ItemType.ARMOR:
						tmpItem = InventoryManager.Instance.ItemContainer.ArmorsList.Find (basicItem => basicItem.Name == itemName);
						break;
					case ItemType.BOOTS:
						tmpItem = InventoryManager.Instance.ItemContainer.BootsList.Find (basicItem => basicItem.Name == itemName);
						break;
					case ItemType.CONSUMABLE:
						tmpItem = InventoryManager.Instance.ItemContainer.ConsumablesList.Find (basicItem => basicItem.Name == itemName);
						break;
					case ItemType.AMMO:
						tmpItem = InventoryManager.Instance.ItemContainer.AmmosList.Find (basicItem => basicItem.Name == itemName);
						break;
					case ItemType.SNARE:
						tmpItem = InventoryManager.Instance.ItemContainer.SnaresList.Find (basicItem => basicItem.Name == itemName);
						break;
					}

					loadedItem.AddComponent<ItemScript> ();
					loadedItem.GetComponent<ItemScript> ().Item = tmpItem;
					allGeneralSlots [index].Push(loadedItem.GetComponent<ItemScript> ());
					Destroy (loadedItem);
				}

			}
		}

		if (active) {
			Debug.Log ("ACTIVE");
			chestInventory.UpdateLayout (allGeneralSlots);
		}
	}
	#endregion

}