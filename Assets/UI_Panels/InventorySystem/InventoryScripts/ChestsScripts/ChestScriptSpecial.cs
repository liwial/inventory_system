﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;

public class ChestScriptSpecial : MonoBehaviour {

	#region Variables
	public MobState mobState;

	//for testing
	public Sprite aliveSprite;
	public Sprite deadSprite;
	//

	protected List<Stack<ItemScript>> allGeneralSlots;
	protected int chestSlots;
	protected bool active;
	#endregion

	#region Properties
	private ChestInventory chestInventory;
	public ChestInventory ChestInventory {
		get { return chestInventory; }
	}
	#endregion

	#region Protected Methods
	protected void OnEnable () {
		
		chestSlots = 20;
		allGeneralSlots = new List<Stack<ItemScript>> (chestSlots);
		active = false;

		chestInventory = GameObject.FindObjectOfType<ChestInventory> ();

		LoadRandomChest (1, 6);

		if (mobState == MobState.ALIVE)
			MakeAlive ();
		else if (mobState == MobState.DEAD)
			MakeDead ();
	}

	protected void OnTriggerEnter2D (Collider2D other) {

		if (other.tag == "Player" && !IsAlive()) {

			InventoryManager.Instance.PromptObject.SetActive (true);
			InventoryManager.Instance.PromptObject.GetComponent<RectTransform> ().position = new Vector3(transform.position.x - 1f, transform.position.y - 1f, 0); //chestInventoryRect.localPosition + new Vector3 (10.0f, 30.0f, 0.0f );
			InventoryManager.Instance.PromptObject.GetComponentInChildren<Text>().text = "Press 'F' key \nto open chest";

			if (chestInventory.FadingOut) {
				chestInventory.instantClose = true;
				chestInventory.MoveItemsToChest ();
			}

			active = true;
		}
	}

	protected void OnTriggerExit2D (Collider2D other) {
		if (other.tag == "Player" && !IsAlive()) {
			active = false;
			InventoryManager.Instance.PromptObject.SetActive (false);
		}
	}
		
	protected void LoadRandomChest (int rangeMin, int rangeMax) {

		for (int i = 0; i < chestSlots; i++)
			allGeneralSlots.Add (new Stack<ItemScript>());

		BasicCharacter basicCharacter = InventoryManager.Instance.CharacterContainer.GetCharacter (this.transform.GetChild(0).name);//(this.name);

		GameObject tmp = Instantiate (InventoryManager.Instance.itemObject);
		tmp.AddComponent<CharacterScript> ();
		CharacterScript newCharacter = tmp.GetComponent<CharacterScript> (); 
		newCharacter.Character = basicCharacter;
		Destroy (tmp);

		int index = 0;

		if (newCharacter.Character != null) {

			foreach (KeyValuePair<string, int> pair in newCharacter.SpecialItems) {
				GameObject tmp2 = Instantiate (InventoryManager.Instance.itemObject);
				tmp2.AddComponent<ItemScript> ();
				ItemScript newItem = tmp2.GetComponent<ItemScript> ();
				newItem = InventoryManager.Instance.FindItemByName (pair.Key);

				index = AddItems (pair.Value, newItem, index);

				Destroy (tmp2);
			}
		}

	}
	#endregion

	#region Private Methods
	private int AddItems(int quantity, ItemScript newItem, int index) {

		if (quantity <= newItem.Item.MaxSize) {
			for (int i = 0; i < quantity; i++)
				allGeneralSlots [index].Push (newItem);
			index++;
		}
		else if (quantity > newItem.Item.MaxSize) {
			int occupiedSlots = Mathf.FloorToInt(quantity / newItem.Item.MaxSize);

			for (int x = 0; x < occupiedSlots; x++) {
				for (int i = 0; i < newItem.Item.MaxSize; i++) {
					allGeneralSlots [index].Push (newItem);
					index++;
				}
			}

			int rest = quantity - ((occupiedSlots) * newItem.Item.MaxSize);

			for (int i = 0; i < rest; i++)
				allGeneralSlots [index].Push (newItem);
			index++;
				
		}

		return index;
	}
	#endregion

	#region Public Methods
	public void UpdateChestLayout () {
		chestInventory.UpdateLayout (allGeneralSlots);
	}

	public bool IsAlive() {
		return (mobState == MobState.ALIVE);
	}

	public void MakeDead () {
		mobState = MobState.DEAD;
		transform.GetChild(0).GetComponent<SpriteRenderer> ().sprite = deadSprite;
	}

	//for testing
	public void MakeAlive() {
		allGeneralSlots.Clear ();
		LoadRandomChest (1, 6);

		mobState = MobState.ALIVE;
		transform.GetChild(0).GetComponent<SpriteRenderer> ().sprite = aliveSprite;
	}
	//

	public void SaveContent(string contentName) {

		string content = string.Empty;

		for (int i = 0; i < allGeneralSlots.Count; i++) {

			if (allGeneralSlots [i].Count != 0) {

				ItemScript tmpItemScript = allGeneralSlots [i].Peek();
				int tmpCount = allGeneralSlots [i].Count;

				string type = tmpItemScript.Item.Type.ToString ();
				string name = tmpItemScript.Item.Name;

				string count = tmpCount.ToString ();

				content += i + "-" + type + "-" + name + "-" + count + ";";
			}
		}

		PlayerPrefs.SetString (gameObject.name + "contentName", content);
		PlayerPrefs.Save ();

	}

	public void LoadContent (string contentName) {

		allGeneralSlots = new List<Stack<ItemScript>> ();

		string content = PlayerPrefs.GetString (gameObject.name + "contentName");
		string[] splitContent = content.Split (';');

		for (int i = 0; i < chestSlots; i++)
			allGeneralSlots.Add (new Stack<ItemScript> ());

		if (content != string.Empty) {

			for (int x = 0; x < splitContent.Length - 1; x++) {

				string[] splitValues = splitContent [x].Split ('-');
				int index = System.Int32.Parse (splitValues [0]);
				ItemType type = (ItemType)System.Enum.Parse (typeof(ItemType), splitValues [1]);
				string itemName = splitValues [2];
				int amount = System.Int32.Parse (splitValues [3]);

				BasicItem tmpItem = null;

				for (int i = 0; i < amount; i++) {

					GameObject loadedItem = Instantiate (InventoryManager.Instance.itemObject);

					switch (type) {
					case ItemType.WEAPON:
						tmpItem = InventoryManager.Instance.ItemContainer.WeaponsList.Find (basicItem => basicItem.Name == itemName);
						break;
					case ItemType.ARMOR:
						tmpItem = InventoryManager.Instance.ItemContainer.ArmorsList.Find (basicItem => basicItem.Name == itemName);
						break;
					case ItemType.BOOTS:
						tmpItem = InventoryManager.Instance.ItemContainer.BootsList.Find (basicItem => basicItem.Name == itemName);
						break;
					case ItemType.CONSUMABLE:
						tmpItem = InventoryManager.Instance.ItemContainer.ConsumablesList.Find (basicItem => basicItem.Name == itemName);
						break;
					case ItemType.AMMO:
						tmpItem = InventoryManager.Instance.ItemContainer.AmmosList.Find (basicItem => basicItem.Name == itemName);
						break;
					case ItemType.SNARE:
						tmpItem = InventoryManager.Instance.ItemContainer.SnaresList.Find (basicItem => basicItem.Name == itemName);
						break;
					}

					loadedItem.AddComponent<ItemScript> ();
					loadedItem.GetComponent<ItemScript> ().Item = tmpItem;
					allGeneralSlots [index].Push(loadedItem.GetComponent<ItemScript> ());
					Destroy (loadedItem);
				}

			}
		}

		if (active) {
			chestInventory.UpdateLayout (allGeneralSlots);
		}
	}
	#endregion

}