﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Testing : MonoBehaviour {

	private GameObject[] chests;
	private GameObject[] chestsSpecial;
	//private GameObject specialChar;


	void Start () {
		GameObject specialChar = (GameObject) Resources.Load("Prefabs/Bad person 01", typeof(GameObject));
		GameObject specialCharObj = Instantiate (specialChar);
		specialCharObj.transform.position = new Vector3 (7, 0);

		int pos = -7;

		for (int i = 0; i < 2; i++) {
			GameObject chest = (GameObject) Resources.Load("Prefabs/ChestPrefab", typeof(GameObject));
			GameObject chestObj = Instantiate (chest);
			chestObj.transform.position = new Vector3 (pos, 0);
			pos += 4;
		}


		chests = GameObject.FindGameObjectsWithTag ("Chest");
		chestsSpecial = GameObject.FindGameObjectsWithTag ("ChestSpecial");

		//int random = Random.Range (0, chests.Length);

		//chests [random].GetComponent<ChestScript> ().MakeDead (); //mobState = MobState.DEAD;

		//for (int i = 0; i < chests.Length; i++)
		//	Debug.Log (chests[i].GetComponent<ChestScript> ().mobState);


		//Debug.Log (chestsSpecial [0].GetComponent<ChestScriptSpecial> ().mobState);
		//Debug.Log (chestsSpecial [0].GetComponent<ChestScriptSpecial> ().IsAlive ());
		//Debug.Log (InventoryManager.Instance.ItemContainer.SpecialCharactersList[0].Item3);
		//MakeThemDead();
	}

	private void MakeThemDead () {

		foreach (GameObject g in chests)
			g.GetComponent<ChestScript> ().MakeDead (); 

		foreach (GameObject g in chestsSpecial)
			g.GetComponent<ChestScriptSpecial> ().MakeDead ();
	}

	void Update () {
		/*
		if (Input.GetKeyDown (KeyCode.Q))
			InventoryOther.Instance.AddItemByNameSB ("Bronze Axe");
		if (Input.GetKeyDown (KeyCode.W))
			InventoryOther.Instance.RemoveItemByNameSB ("Bronze Axe");
		if (Input.GetKeyDown (KeyCode.E))
			Debug.Log(InventoryOther.Instance.IsItemInInventoryByNameSB ("Bronze Axe"));
		if (Input.GetKeyDown (KeyCode.J))
			Debug.Log (InventoryOther.Instance.IsItemEquipedByName ("Bronze Axe"));
		if (Input.GetKeyDown (KeyCode.K))
			InventoryOther.Instance.EquipItemByNameSB ("Bronze Axe");
		
		if (Input.GetKeyDown (KeyCode.L))
			InventoryOther.Instance.BlockUnequipItemByName ("Bronze Axe");
		if (Input.GetKeyDown (KeyCode.V))
			InventoryOther.Instance.UnblockUnequipItemByName ("Bronze Axe");


		if (Input.GetKeyDown (KeyCode.Z))
			Debug.Log (InventoryOther.Instance.IsWeaponless ());
		if (Input.GetKeyDown (KeyCode.X))
			Debug.Log (InventoryOther.Instance.IsWeaponlessCompletely());
		if (Input.GetKeyDown (KeyCode.C))
			Debug.Log (InventoryOther.Instance.IsNaked ());
		*/

		if (Input.GetKeyDown (KeyCode.M))
			MakeThemDead ();

	}
}
