﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterScript : MonoBehaviour {

	public Sprite icon;

	private Dictionary<string, int> specialItems = new Dictionary<string, int> ();
	public Dictionary<string, int> SpecialItems {
		get { return specialItems; }
	}

	private BasicCharacter character;
	public BasicCharacter Character {
		get { return character; }
		set { 
			character = value; 
			if (value != null && value.Name != string.Empty) {

				string tmp = "CharacterIcons/" + value.Icon;

				if (value.Icon != string.Empty)
					icon = (Sprite)Resources.Load (tmp, typeof(Sprite));

				if (value.Item1 != string.Empty)
					specialItems.Add (value.Item1, value.Item1Quantity);
				if (value.Item2 != string.Empty)
					specialItems.Add (value.Item2, value.Item2Quantity);
				if (value.Item3 != string.Empty)
					specialItems.Add (value.Item3, value.Item3Quantity);
				if (value.Item4 != string.Empty)
					specialItems.Add (value.Item4, value.Item4Quantity);
				if (value.Item5 != string.Empty)
					specialItems.Add (value.Item5, value.Item5Quantity);
				if (value.Item6 != string.Empty)
					specialItems.Add (value.Item6, value.Item6Quantity);
				if (value.Item7 != string.Empty)
					specialItems.Add (value.Item7, value.Item7Quantity);
				if (value.Item8 != string.Empty)
					specialItems.Add (value.Item8, value.Item8Quantity);
				if (value.Item9 != string.Empty)
					specialItems.Add (value.Item9, value.Item9Quantity);
				if (value.Item10 != string.Empty)
					specialItems.Add (value.Item10, value.Item10Quantity);
				if (value.Item11 != string.Empty)
					specialItems.Add (value.Item11, value.Item11Quantity);
				if (value.Item12 != string.Empty)
					specialItems.Add (value.Item12, value.Item12Quantity);
				if (value.Item13 != string.Empty)
					specialItems.Add (value.Item13, value.Item13Quantity);
				if (value.Item14 != string.Empty)
					specialItems.Add (value.Item14, value.Item14Quantity);
				if (value.Item15 != string.Empty)
					specialItems.Add (value.Item15, value.Item15Quantity);
				if (value.Item16 != string.Empty)
					specialItems.Add (value.Item16, value.Item16Quantity);
				if (value.Item17 != string.Empty)
					specialItems.Add (value.Item17, value.Item17Quantity);
				if (value.Item18 != string.Empty)
					specialItems.Add (value.Item18, value.Item18Quantity);
				if (value.Item19 != string.Empty)
					specialItems.Add (value.Item19, value.Item19Quantity);
				if (value.Item20 != string.Empty)
					specialItems.Add (value.Item20, value.Item20Quantity);

			}
		}
	}
		
}
