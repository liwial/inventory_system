﻿using System.Collections;
using UnityEngine;


public class BasicCharacter {

	public string Name { get; set; }

	public string Icon { get; set; }

	public int Gold { get; set; }

	public string Item1 { get; set; }
	public string Item2 { get; set; }
	public string Item3 { get; set; }
	public string Item4 { get; set; }
	public string Item5 { get; set; }
	public string Item6 { get; set; }
	public string Item7 { get; set; }
	public string Item8 { get; set; }
	public string Item9 { get; set; }
	public string Item10 { get; set; }
	public string Item11 { get; set; }
	public string Item12 { get; set; }
	public string Item13 { get; set; }
	public string Item14 { get; set; }
	public string Item15 { get; set; }
	public string Item16 { get; set; }
	public string Item17 { get; set; }
	public string Item18 { get; set; }
	public string Item19 { get; set; }
	public string Item20 { get; set; }

	public int Item1Quantity { get; set; }
	public int Item2Quantity { get; set; }
	public int Item3Quantity { get; set; }
	public int Item4Quantity { get; set; }
	public int Item5Quantity { get; set; }
	public int Item6Quantity { get; set; }
	public int Item7Quantity { get; set; }
	public int Item8Quantity { get; set; }
	public int Item9Quantity { get; set; }
	public int Item10Quantity { get; set; }
	public int Item11Quantity { get; set; }
	public int Item12Quantity { get; set; }
	public int Item13Quantity { get; set; }
	public int Item14Quantity { get; set; }
	public int Item15Quantity { get; set; }
	public int Item16Quantity { get; set; }
	public int Item17Quantity { get; set; }
	public int Item18Quantity { get; set; }
	public int Item19Quantity { get; set; }
	public int Item20Quantity { get; set; }

	public BasicCharacter() {
		
	}

}