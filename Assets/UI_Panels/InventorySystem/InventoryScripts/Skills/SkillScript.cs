﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkillScript : MonoBehaviour {

	public Sprite spriteNeutral;
	public Sprite spriteHighlighted;

	private BasicSkill skill;
	public BasicSkill Skill {
		get { return skill; }
		set { 
			skill = value; 
			if (value != null) {
				string tmp1 = "SkillIcons/" + value.SpriteNeutral;
				string tmp2 = "SkillIcons/" + value.SpriteHighlighted;

				spriteNeutral = (Sprite)Resources.Load (tmp1, typeof(Sprite));
				spriteHighlighted = (Sprite)Resources.Load (tmp2, typeof(Sprite));
			}
		}
	}
		
	void Start () {
	}

	public void Use() {
		skill.Use ();
	}

	public string GetTooltip() {
		return skill.GetTooltip ();
	}

	public string GetShortTooltip() {
		return skill.GetShortTooltip ();
	}
}
