﻿using System.Collections;
using UnityEngine;

public abstract class BasicSkill {

	public ItemType Type { get; set; }

	public string WeaponType { get; set; }
	public string Name { get; set; }
	public string Description { get; set; }
	public string SpriteNeutral { get; set; }
	public string SpriteHighlighted { get; set; }

	public int MaxSize { get; set; }
	public bool IsShortcutable { get; set; }
	public int EnergyCost { get; set; }
	public bool IsVisible { get; set; }
	public bool IsActive { get; set; }

	public BasicSkill() {
		
	}

	public abstract void Use ();

	public virtual string GetTooltip() {

		string color = string.Empty;
		string newLine = string.Empty;

		if (Description != string.Empty) {
			newLine = "\n";
		}

		color = "white";

		return string.Format ("<color=" + color + "><size=26>{0}</size></color><size=22><i><color=cyan>" + newLine + "{1}</color></i></size>", Name, Description); 
	}

	public virtual string GetShortTooltip() {
		return string.Format ("<size=22><i><color=cyan>{0}</color></i></size>", Description); 
	}

}