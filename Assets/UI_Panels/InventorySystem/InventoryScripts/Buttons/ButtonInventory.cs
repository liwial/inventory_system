﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ButtonInventory : BasicButton {

	void Awake() {

		base.SetEventTrigger ();

		//this.GetComponent<Button> ().onClick.AddListener
		//(
		//	delegate {
		//		InventoryManager.Instance.OpenInventory();
		//	}
		//);

		EventTrigger.Entry pointerDown = new EventTrigger.Entry ();
		pointerDown.eventID = EventTriggerType.PointerDown;
		pointerDown.callback.AddListener 
		(
			delegate {
				InventoryManager.Instance.OpenInventory();
			}
		);
		this.GetComponent<EventTrigger> ().triggers.Add (pointerDown);

	}

}
