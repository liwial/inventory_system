﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ButtonSkillWindow : BasicButton {

	void Awake() {

		base.SetEventTrigger ();

		EventTrigger.Entry pointerDown = new EventTrigger.Entry ();
		pointerDown.eventID = EventTriggerType.PointerDown;
		pointerDown.callback.AddListener 
		(
			delegate {
				InventoryManager.Instance.OpenSkillWindow();
			}
		);
		this.GetComponent<EventTrigger> ().triggers.Add (pointerDown);

	}

}
