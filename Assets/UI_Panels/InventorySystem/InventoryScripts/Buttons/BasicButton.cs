﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class BasicButton : MonoBehaviour {

	public void SetEventTrigger () {
		
		EventTrigger.Entry entryEnter = new EventTrigger.Entry ();
		entryEnter.eventID = EventTriggerType.PointerEnter;
		entryEnter.callback.AddListener 
		(
			delegate {
				InventoryManager.Instance.ShowButtonTooltip(this.gameObject);
			}
		);
		this.GetComponent<EventTrigger> ().triggers.Add (entryEnter);

		EventTrigger.Entry entryExit = new EventTrigger.Entry ();
		entryExit.eventID = EventTriggerType.PointerExit;
		entryExit.callback.AddListener 
		(
			delegate {
				InventoryManager.Instance.HideButtonTooltip();
			}
		);
		this.GetComponent<EventTrigger> ().triggers.Add (entryExit);

	}

}
