﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ButtonTake : BasicButton {

	void Awake() {

		base.SetEventTrigger ();

		this.GetComponent<Button> ().onClick.AddListener
		(
			delegate {
				ChestInventory.ChestInstance.TakeEverything();
			}
		);

	}
		
}
