﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ButtonQuest : BasicButton {

	public Sprite imageNotPressed;
	public Sprite imagePressed;

	void Awake() {

		base.SetEventTrigger ();

		this.GetComponent<Button> ().onClick.AddListener
		(
			delegate {
				Inventory.Instance.ChangeTabOrder("quest", true);
			}
		);

	}
		
}
