﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ButtonClose : BasicButton {

	void Awake() {

		base.SetEventTrigger ();

		this.GetComponent<Button> ().onClick.AddListener
		(
			delegate {
				ChestInventory.ChestInstance.CloseChest();
			}
		);

	}
		
}
