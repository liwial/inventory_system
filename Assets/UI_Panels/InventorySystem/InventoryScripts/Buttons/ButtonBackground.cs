﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ButtonBackground : MonoBehaviour {

	void Awake() {

		this.GetComponent<Button> ().onClick.AddListener
		(
			delegate {

				if (InventoryManager.Instance.From != null && InventoryManager.Instance.FromSkillWidow())
					return;
				else
					Inventory.Instance.SetQuestion();
			}
		);

	}
		
}
