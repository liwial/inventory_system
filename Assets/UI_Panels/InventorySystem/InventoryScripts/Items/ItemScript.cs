﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//public enum ItemType {WEAPON, ARMOR, BOOTS, CONSUMABLE, AMMO, SNARE, PLOTRELATED, GOLD, SKILL};

public class ItemScript : MonoBehaviour {

	public Sprite spriteNeutral;
	public Sprite spriteHighlighted;
	public Sprite spriteEquiped;

	private BasicItem item;
	public BasicItem Item {
		get { return item; }
		set { 
			item = value; 

			string tmp1 = "ItemIcons/" + value.SpriteNeutral;
			string tmp2 = "ItemIcons/" + value.SpriteHighlighted;
			string tmp3 = "ItemIcons/" + value.SpriteEquiped;

			spriteNeutral = (Sprite)Resources.Load (tmp1, typeof(Sprite));
			spriteHighlighted = (Sprite)Resources.Load (tmp2, typeof(Sprite));
			if (tmp3 != string.Empty)
				spriteEquiped = (Sprite)Resources.Load (tmp3, typeof(Sprite));
		}
	}
		
	void Start () {

	}

	public void Use(Slot slot) {
		item.Use (slot, this);
	}

	public string GetTooltip() {
		return item.GetTooltip ();
	}

	public string GetTooltipTrade() {
		return item.GetTooltipTrade ();
	}
}
