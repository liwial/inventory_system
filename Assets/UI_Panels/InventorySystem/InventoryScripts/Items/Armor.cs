﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Armor : BasicItem {

	public int PiercingDamageProtection { get; set; }
	public int CuttingDamageProtection { get; set; }
	public int ShockProtection { get; set; }
	public int FireProtection { get; set; }
	public RUN_STAMINA_USAGE RunStaminaUsage { get; set; }
	public int CriticalHitDefence { get; set; }

	public Armor() {

	}

	public override void Use(Slot slot, ItemScript item) {
		InventoryManager.Instance.ShowMessage ("Equiped " + Name);
	}

	public override string GetTooltip () {
		string stats = string.Empty;

		if (PiercingDamageProtection != 0)
			stats += "\n+" + PiercingDamageProtection.ToString() + " piercing damage protection";
		if (CuttingDamageProtection != 0)
			stats += "\n+" + CuttingDamageProtection.ToString() + " cutting damage protection";
		if (ShockProtection != 0)
			stats += "\n+" + ShockProtection.ToString() + " shock protection";
		if (FireProtection != 0)
			stats += "\n+" + FireProtection.ToString() + " fire protection";
		
		stats += "\nRunStaminaUsage: " + RunStaminaUsage.ToString();

		if (CriticalHitDefence != 0)
			stats += "\n+" + CriticalHitDefence.ToString() + "% critical hit defence";

		string itemTip = base.GetTooltip();

		return string.Format ("{0}" + "<size=20>{1}</size>", itemTip, stats);
	}
		
}
