﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Equipment : MonoBehaviour {

	public static Dictionary<ItemScript,Slot> equippedWeapon  = new Dictionary<ItemScript, Slot>();
	public static Dictionary<ItemScript,Slot> equippedArmor = new Dictionary<ItemScript, Slot>();
	public static Dictionary<ItemScript,Slot> equippedBoots = new Dictionary<ItemScript, Slot>();
	public static Dictionary<ItemScript,Slot> equippedAmmo = new Dictionary<ItemScript, Slot>();

	#region Properties
	private static bool weaponChangedSkill = false;
	public static bool WeaponChangedSkill {
		get { return weaponChangedSkill; }
		set { weaponChangedSkill = value; }
	}

	private static bool weaponChangedSB = false;
	public static bool WeaponChangedSB {
		get { return weaponChangedSB; }
		set { weaponChangedSB = value; }
	}

	private static bool weaponChangedQueue = false;
	public static bool WeaponChangedQueue {
		get { return weaponChangedQueue; }
		set { weaponChangedQueue = value; }
	}

	private static bool wasAnotherCopy = false;
	public static bool WasAnotherCopy {
		get { return wasAnotherCopy; }
		set { wasAnotherCopy = value; } 
	}
	#endregion

	#region Private Methods
	private static bool IsTheSame (Dictionary <ItemScript,Slot> dict, Slot slot) {
		foreach (KeyValuePair<ItemScript,Slot> pair in dict) {
			if (pair.Value == slot)
				return true;
		}
		return false;
	}

	private static void EquipSprites (Dictionary <ItemScript,Slot> dict, ItemScript item, Slot slot) {
		string equipedString = "ItemIcons/" + item.Item.SpriteEquiped;
		string highlightString = "ItemIcons/" + item.Item.SpriteHighlighted;

		Sprite equiped = (Sprite)Resources.Load (equipedString, typeof(Sprite));
		Sprite highlighted = (Sprite)Resources.Load (highlightString, typeof(Sprite));

		slot.ChangeSprite (equiped, highlighted);
		dict.Add (item, slot);
		slot.isEquiped = true;
		item.Item.Use (slot, item);
	}

	private static void JustEquip(Dictionary <ItemScript,Slot> dict, ItemScript item, Slot slot) {
		if (dict.Count != 0) {

			if (IsTheSame (dict, slot)) {

				if (!slot.cannotUnequip) {
					dict.Clear ();
					InventoryManager.Instance.ShowMessage (item.Item.Name + " is no longer equipped");
				} else
					Debug.Log ("This item cannot be unequipped");

			} else {
				if (dict == equippedWeapon) {

					string weaponName = string.Empty;

					foreach (KeyValuePair <ItemScript, Slot> kv in equippedWeapon)
						weaponName = kv.Key.Item.Name;
					
					if (weaponName == item.Item.Name) {
						wasAnotherCopy = true;
					}
				}

				dict.Clear ();
				EquipSprites (dict, item, slot);
				weaponChangedSkill = true;
				weaponChangedSB = true;
				weaponChangedQueue = true;
			}

		} else
			EquipSprites (dict, item, slot);
	}
		
	private static void IfOtherEquipped(ItemScript item) {
		
		ItemType itemType = item.Item.Type;

		switch (itemType) {
		case ItemType.WEAPON:
			if (equippedWeapon.Count != 0) {
				foreach(KeyValuePair<ItemScript,Slot> pair in equippedWeapon) {
					
					if (!pair.Value.cannotUnequip) {

						string neutralString = "ItemIcons/" + pair.Key.Item.SpriteNeutral;
						string highlightString = "ItemIcons/" + pair.Key.Item.SpriteHighlighted;

						Sprite neutral = (Sprite)Resources.Load (neutralString, typeof(Sprite));
						Sprite highlighted = (Sprite)Resources.Load (highlightString, typeof(Sprite));

						pair.Value.ChangeSprite (neutral, highlighted);
						pair.Value.isEquiped = false;
					} else
						Debug.Log ("This item cannot be unequipped");
				}
			}
			break;
		case ItemType.ARMOR:
			if (equippedArmor.Count != 0) {
				foreach(KeyValuePair<ItemScript,Slot> pair in equippedArmor) {
					if (!pair.Value.cannotUnequip) {

						string neutralString = "ItemIcons/" + pair.Key.Item.SpriteNeutral;
						string highlightString = "ItemIcons/" + pair.Key.Item.SpriteHighlighted;

						Sprite neutral = (Sprite)Resources.Load (neutralString, typeof(Sprite));
						Sprite highlighted = (Sprite)Resources.Load (highlightString, typeof(Sprite));

						pair.Value.ChangeSprite (neutral, highlighted);
						pair.Value.isEquiped = false;
					}
				}
			}
			break;
		case ItemType.BOOTS:
			if (equippedBoots.Count != 0) {
				foreach(KeyValuePair<ItemScript,Slot> pair in equippedBoots) {
					if (!pair.Value.cannotUnequip) {

						string neutralString = "ItemIcons/" + pair.Key.Item.SpriteNeutral;
						string highlightString = "ItemIcons/" + pair.Key.Item.SpriteHighlighted;

						Sprite neutral = (Sprite)Resources.Load (neutralString, typeof(Sprite));
						Sprite highlighted = (Sprite)Resources.Load (highlightString, typeof(Sprite));

						pair.Value.ChangeSprite (neutral, highlighted);
						pair.Value.isEquiped = false;
					}
				}
			}
			break;
		case ItemType.AMMO:
			if (equippedAmmo.Count != 0) {
				foreach(KeyValuePair<ItemScript,Slot> pair in equippedAmmo) {
					if (!pair.Value.cannotUnequip) {

						string neutralString = "ItemIcons/" + pair.Key.Item.SpriteNeutral;
						string highlightString = "ItemIcons/" + pair.Key.Item.SpriteHighlighted;

						Sprite neutral = (Sprite)Resources.Load (neutralString, typeof(Sprite));
						Sprite highlighted = (Sprite)Resources.Load (highlightString, typeof(Sprite));

						pair.Value.ChangeSprite (neutral, highlighted);
						pair.Value.isEquiped = false;
					}
				}
			}
			break;
		default:
			Debug.Log ("This type of item cannot be equipped");
			break;
		}
	}
	#endregion

	#region Public Methods
	public static string EquippedWeaponName() {

		string equippedWeaponName = string.Empty;

		foreach (KeyValuePair <ItemScript, Slot> kv in equippedWeapon)
			equippedWeaponName = kv.Key.Item.Name;

		return equippedWeaponName;

	}

	public static string EquippedWeaponType() {

		string equippedWeaponType = string.Empty;

		foreach (KeyValuePair <ItemScript, Slot> kv in equippedWeapon)
			equippedWeaponType = kv.Key.Item.WeaponType;

		return equippedWeaponType;

	}

	public static void EquipItem(ItemScript item, Slot slot) {

		// unequipping item from the same category if exists
		IfOtherEquipped (item); // resets sprites to normal

		ItemType itemType = item.Item.Type;

		switch (itemType) {
		case ItemType.WEAPON:
			JustEquip (equippedWeapon, item, slot);
			break;
		case ItemType.ARMOR:
			JustEquip (equippedArmor, item, slot);
			break;
		case ItemType.BOOTS:
			JustEquip (equippedBoots, item, slot);
			break;
		case ItemType.AMMO:
			JustEquip (equippedAmmo, item, slot);
			break;
		default:
			Debug.Log ("This type of item cannot be equipped");
			break;
		}
	}

	public static void SaveContent (string contentName) {

		string content = string.Empty;

		if (equippedWeapon.Count != 0) {
			foreach (KeyValuePair<ItemScript, Slot> kv in equippedWeapon)
				content += kv.Key.Item.Name.ToString() + "-" + kv.Value.ToString() + ";";  
		}
		if (equippedArmor.Count != 0) {
			foreach (KeyValuePair<ItemScript, Slot> kv in equippedArmor)
				content += kv.Key.Item.Name.ToString() + "-" + kv.Value.ToString() + ";";  
		}
		if (equippedBoots.Count != 0) {
			foreach (KeyValuePair<ItemScript, Slot> kv in equippedBoots)
				content += kv.Key.Item.Name.ToString() + "-" + kv.Value.ToString() + ";";  
		}
		if (equippedAmmo.Count != 0) {
			foreach (KeyValuePair<ItemScript, Slot> kv in equippedAmmo)
				content += kv.Key.Item.Name.ToString() + "-" + kv.Value.ToString() + ";";  
		} 

		PlayerPrefs.SetString (contentName, content);
		PlayerPrefs.Save ();
	}

	public static void LoadContent (string contentName) {

		string content = PlayerPrefs.GetString (contentName);
		string[] splitContent = content.Split (';');

		for (int x = 0; x < splitContent.Length - 1; x++) {
			string[] splitValues = splitContent [x].Split ('-');
			string itemName = splitValues [0];

			bool inInventory = InventoryOther.Instance.EquipItemByName (itemName);

			if (!inInventory)
				InventoryOther.Instance.EquipItemByNameSB (itemName);
		}

	}
	#endregion

}
