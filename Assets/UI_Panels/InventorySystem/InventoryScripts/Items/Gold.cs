﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gold : BasicItem {

	public int Dmg { get; set; }

	public Gold() {
		this.Type = ItemType.GOLD;

		this.Name = "Gold";
		this.Description = "";

		this.SpriteNeutral = "gold";
		this.SpriteHighlighted = "goldH";
		this.SpriteEquiped = "gold";

		this.MaxSize = 1000;

		this.IsStackable = true;
		this.IsShortcutable = false;
		this.IsQuestRelated = false;
		this.IsWearable = false;
	}

	public override void Use(Slot slot, ItemScript item) {
		//Debug.Log ("Used " + Name);
	}

	public override string GetTooltip () {
		return string.Format ("<color=yellow><size=26>{0}</size></color>", Name);
	}
		
}
