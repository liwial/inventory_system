﻿using System.Collections;
using UnityEngine;

public abstract class BasicItem {

	public ItemType Type { get; set; }

	public string Name { get; set; }
	public string Description { get; set; }

	public string SpriteNeutral { get; set; }
	public string SpriteHighlighted { get; set; }
	public string SpriteEquiped { get; set; }

	public int MaxSize { get; set; }

	public bool IsStackable { get; set; }
	public bool IsShortcutable { get; set; }
	public bool IsQuestRelated { get; set; }
	public bool IsWearable { get; set; }

	public string WeaponType { get; set; }

	public int Value { get; set; }
	public int ValueTrade { get; set; }
	//public string Stats { get; set; }

	public BasicItem() {
		
	}
		
	public abstract void Use (Slot slot, ItemScript item);

	public virtual string GetTooltip() {

		string color = string.Empty;
		string newLine = string.Empty;

		if (Description != string.Empty) {
			newLine = "\n";
		}

		color = "white";

		return string.Format ("<color=" + color + "><size=26>{0}</size></color><size=22><i><color=lime>" + newLine + "{1}</color></i></size>", Name, Description); 
	}

	public string GetTooltipTrade() {
		return string.Format ("<color=" + "red" + "><size=26>{0}</size></color><size=22><i><color=lime>\n{1}\n{2}</color></i></size>", Name, "Real value: " + Value, "Trade value: " + ValueTrade); 

	}

}
