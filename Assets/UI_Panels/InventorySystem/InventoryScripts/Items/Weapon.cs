﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Weapon : BasicItem {

	public int DmgMin { get; set; } 
	public int DmgMax { get; set; } 
	public int ArmorPiercing { get; set; } 
	public int CHC { get; set; } 
	public int CHCM { get; set; } 
	public int CHM { get; set; }
	public int EnergyCost { get; set; }

	public Weapon() {
		
	}

	public override void Use(Slot slot, ItemScript item) {
		InventoryManager.Instance.ShowMessage ("Equiped " + Name);
	}

	public override string GetTooltip () {
		string stats = string.Empty;

		if (DmgMin != 0)
			stats += "\n+" + DmgMin.ToString() + " minimal damage";

		if (DmgMax != 0)
			stats += "\n+" + DmgMax.ToString() + " maximal damage";

		if (ArmorPiercing != 0)
			stats += "\n+" + ArmorPiercing.ToString() + " armor piercing";

		//if (CHC != 0)
		//	stats += "\n+" + CHC.ToString() + "% critical hit chance";

		if (CHM != 0)
			stats += "\n+" + CHM.ToString() + "% critical hit multiplicator";

		if (EnergyCost != 0)
			stats += "\n+" + EnergyCost.ToString() + " energy cost";

		string itemTip = base.GetTooltip();

		return string.Format ("{0}" + "<size=20>{1}</size>", itemTip, stats);
	}

}
