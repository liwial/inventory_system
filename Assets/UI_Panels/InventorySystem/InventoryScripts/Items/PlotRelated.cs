﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlotRelated : BasicItem {

	public PlotRelated() {

	}

	public override void Use(Slot slot, ItemScript item) {
		InventoryManager.Instance.ShowMessage ("Used " + Name);
	}

	public override string GetTooltip () {
		string itemTip = base.GetTooltip();
		return itemTip;
	}

}
