﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Consumable : BasicItem {

	public int FoodPoints { get; set; }
	public int StaminaPoints { get; set; }
	public int HealthPoints { get; set; }
	public int TraumaPoints { get; set; }
	public int WildernesSkillLevel { get; set; }

	public Consumable() {

	}

	public override void Use(Slot slot, ItemScript item) {
		InventoryManager.ShowMessageStatic("Used " + Name);

		//AddFoodPoints
		//AddStaminaPoints
		//AddHPPoints
	}

	public override string GetTooltip () {
		string stats = string.Empty;

		if (FoodPoints != 0)
			stats += "\n+" + FoodPoints.ToString() + " food";

		if (StaminaPoints != 0)
			stats += "\n+" + StaminaPoints.ToString() + " stamina";

		if (HealthPoints != 0)
			stats += "\n+" + HealthPoints.ToString() + " health";

		string itemTip = base.GetTooltip();

		return string.Format ("{0}" + "<size=20>{1}</size>", itemTip, stats);
	}

}
