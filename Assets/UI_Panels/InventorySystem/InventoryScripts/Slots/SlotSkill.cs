﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;


public class SlotSkill : Slot {

	#region Properties
	private Stack<SkillScript> skills;
	public Stack<SkillScript> Skills {
		get { return skills; }
		set { skills = value; }
	}

	public bool IsEmptySkill {
		get { return skills.Count == 0; }
	}

	public SkillScript CurrentSkill {
		get { return skills.Peek (); }
	}
	#endregion

	#region Protected Methods
	protected override void Awake () {
		skills = new Stack<SkillScript> ();
		base.Awake ();
	}
	#endregion

	#region Public Methods
	public void AssignSprites(Stack<SkillScript> skill) {
		ChangeSprite (skill.Peek().spriteNeutral, skill.Peek().spriteHighlighted);
	}

	public override void ClearSlot() {
		ChangeSprite (slotEmpty, slotHightlighted);
	}

	public override void UseItem () {

		// slot got skill on it
		if (!IsEmptySkill) {

			//only active skill can be added to queue
			if (Skills.Peek ().Skill.WeaponType == Equipment.EquippedWeaponType())
				Queue.QueueInstance.AddSkill(skills);

		}
		//slot got item on it
		else 
			base.UseItem ();
	}
		
	public override void OnPointerClick(PointerEventData eventData) {
		if (eventData.button == PointerEventData.InputButton.Right)
			UseItem ();
	}
	#endregion
}
