﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class Slot : MonoBehaviour, IPointerClickHandler {
	
	#region Variables
	public bool isEquiped;
	public bool cannotUnequip;
	public Sprite slotEmpty;
	public Sprite slotHightlighted;

	protected Text stackTxt;
	#endregion

	#region Properties
	public Stack<ItemScript> items;
	public Stack<ItemScript> Items {
		get { return items; }
		set { items = value; }
	}

	public bool IsEmpty {
		get { return items.Count == 0; }
	}

	public bool IsAvailable {
		get { return CurrentItem.Item.MaxSize > items.Count; }
	}

	public ItemScript CurrentItem {
		get { return items.Peek (); }
	}
	#endregion

	#region Protected Methods
	protected virtual void Awake() {
		items = new Stack<ItemScript> ();
		stackTxt = GetComponentInChildren<Text> ();
	}

	protected virtual void Start () {

		isEquiped = false;
		cannotUnequip = false;

		items = new Stack<ItemScript> ();
		RectTransform slotRect = GetComponent<RectTransform> ();
		RectTransform txtRect = GetComponent<RectTransform> ();

		int txtScaleFactor = (int)(slotRect.sizeDelta.x * 0.5f);

		stackTxt.resizeTextMaxSize = txtScaleFactor;
		stackTxt.resizeTextMinSize = txtScaleFactor;

		txtRect.SetSizeWithCurrentAnchors (RectTransform.Axis.Horizontal, slotRect.sizeDelta.x);
		txtRect.SetSizeWithCurrentAnchors (RectTransform.Axis.Vertical, slotRect.sizeDelta.y);
	}
	#endregion

	#region Public Methods
	public virtual void AddItem(ItemScript item) {

		if (IsEmpty && !item.Item.IsQuestRelated) {
			transform.parent.GetComponent<Inventory> ().EmptySlots--;
		} else {
			//
		}

		items.Push (item);

		if (items.Count > 1)
			stackTxt.text = items.Count.ToString ();

		ChangeSprite (item.spriteNeutral, item.spriteHighlighted);
	}

	public void AddItems(Stack<ItemScript> _items) {
		this.items = new Stack<ItemScript> (_items);
		stackTxt.text = _items.Count > 1 ? _items.Count.ToString () : string.Empty;

		ChangeSprite (CurrentItem.spriteNeutral, CurrentItem.spriteHighlighted);
	}

	public void AddItemsEquiped(Stack<ItemScript> _items) {
		this.items = new Stack<ItemScript> (_items);
		stackTxt.text = _items.Count > 1 ? _items.Count.ToString () : string.Empty;

		ChangeSprite (_items.Peek().spriteEquiped, _items.Peek().spriteHighlighted);
	}
		
	public void ChangeSprite(Sprite neutral, Sprite highlight) {
		GetComponent<Image> ().sprite = neutral;

		SpriteState st = new SpriteState ();

		st.highlightedSprite = highlight;
		st.pressedSprite = neutral;

		GetComponent<Button> ().spriteState = st;
	}

	public virtual void UseItem () {
		if (!IsEmpty) {

			ItemScript tmp = items.Peek ();

			// if item can be equiped
			if (tmp.Item.IsWearable) {
				Equipment.EquipItem (tmp, this);
			} else {
				tmp.Item.Use (this, tmp);
				items.Pop ().Use (this);
				stackTxt.text = items.Count > 1 ? items.Count.ToString () : string.Empty;
			}

			if (IsEmpty) {
				ChangeSprite (slotEmpty, slotHightlighted);
				transform.parent.GetComponent<Inventory> ().EmptySlots++;
			}
				
		}
	}

	public virtual void ClearSlot() {
		items.Clear ();
		ChangeSprite (slotEmpty, slotHightlighted);
		stackTxt.text = string.Empty;

		isEquiped = false;
		cannotUnequip = false;

		if (transform.parent != null) {
			transform.parent.GetComponent<Inventory> ().EmptySlots++;
		}
	}

	public Stack<ItemScript> RemoveItems(int amount) {
		Stack<ItemScript> tmp = new Stack<ItemScript> ();

		for (int i = 0; i < amount; i++) {
			tmp.Push (items.Pop ());
		}
		stackTxt.text = items.Count > 1 ? items.Count.ToString() : string.Empty;
		return tmp;
	}

	public ItemScript RemoveItem() {
		ItemScript tmp;
		tmp = items.Pop ();

		stackTxt.text = items.Count > 1 ? items.Count.ToString () : string.Empty;
		return tmp;
	}

	public virtual void OnPointerClick(PointerEventData eventData) {

		if (eventData.button == PointerEventData.InputButton.Right && !GameObject.Find ("Hover") && Inventory.Instance.IsOpen) {
			UseItem ();
		}
		else if (eventData.button == PointerEventData.InputButton.Left && Input.GetKey (KeyCode.LeftShift) && !IsEmpty && !GameObject.Find ("Hover")) {
			Vector2 position;
			RectTransformUtility.ScreenPointToLocalPointInRectangle (InventoryManager.Instance.canvas.transform as RectTransform, Input.mousePosition, InventoryManager.Instance.canvas.worldCamera, out position);
			InventoryManager.Instance.SelectStackSize.SetActive (true);
			InventoryManager.Instance.SelectStackSize.transform.position = InventoryManager.Instance.canvas.transform.TransformPoint (position);
			InventoryManager.Instance.SetStackInfo (items.Count, -90.0f);
		}
	}
		
	public static void SwapItems (Slot from, Slot to) {

		if (from.isEquiped && !to.isEquiped) {

			//remembering items from old slot 'To' 
			Stack<ItemScript> tmpToStack = new Stack<ItemScript> (to.Items);

			//slot 'To' - adding items (equipped sprites) from 'From' slot
			to.AddItemsEquiped (from.Items);

			ItemScript tmpFrom = from.Items.Peek ();

			//slot to - equiping item
			Equipment.EquipItem (tmpFrom, to);

			//slot from - adding items or clearing slot 
			if (tmpToStack.Count == 0) {
				to.transform.parent.GetComponent<Inventory> ().EmptySlots--;
				from.ClearSlot ();
			} else {
				from.AddItems (tmpToStack);
			}
				
		} else if (!from.isEquiped && to.isEquiped) {

			//remembering items from old slot 'From' 
			Stack<ItemScript> tmpFromStack = new Stack<ItemScript> (from.Items);

			//slot from - adding items (equipped sprites) from 'To' slot
			from.AddItemsEquiped (to.Items);

			ItemScript tmpTo = to.Items.Peek ();

			//slot from - equiping item
			Equipment.EquipItem (tmpTo, from);

			//slot to - adding items (normal sprites)
			to.AddItems (tmpFromStack);
		} else if (from.isEquiped && to.isEquiped) {

			//remembering items from old slot 'To' 
			Stack<ItemScript> tmpToStack = new Stack<ItemScript> (InventoryManager.Instance.To.Items);
			//remembering items from old slot 'From' 
			Stack<ItemScript> tmpFromStack = new Stack<ItemScript> (InventoryManager.Instance.From.Items);

			//dequip slot 'From' 
			Equipment.EquipItem (InventoryManager.Instance.From.Items.Peek(), from);
			//dequip slot 'To'
			Equipment.EquipItem (InventoryManager.Instance.To.Items.Peek(), to);

			//slot to - adding items (equipped sprites)
			InventoryManager.Instance.To.AddItemsEquiped (tmpFromStack);
			//slot from - adding items (equipped sprites) from 'To' slot
			InventoryManager.Instance.From.AddItemsEquiped (tmpToStack);

			//slot from - equiping item
			Equipment.EquipItem (InventoryManager.Instance.From.Items.Peek(), from);
			//slot to - equiping item
			Equipment.EquipItem (InventoryManager.Instance.To.Items.Peek(), to);

		} else {
			Stack<ItemScript> tmpToStack = new Stack<ItemScript> (to.Items);
			to.AddItems (from.Items);

			if (tmpToStack.Count == 0) {
				to.transform.parent.GetComponent<Inventory> ().EmptySlots--;
				from.ClearSlot ();
			} else {
				from.AddItems (tmpToStack);
			}

		}

		// for InventoryOther
		bool tmpCannotUnequip = from.cannotUnequip;
		from.cannotUnequip = to.cannotUnequip;
		to.cannotUnequip = tmpCannotUnequip;

	}
	#endregion

}
