﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class SlotQueue : Slot {

	private Stack<SkillScript> skills;

	public Stack<SkillScript> Skills {
		get { return skills; }
		set { skills = value; }
	}

	public bool IsEmptySkill {
		get { return skills.Count == 0; }
	}

	public SkillScript CurrentSkill {
		get { return skills.Peek (); }
	}

	protected override void Awake () {
		base.Awake ();
	}
		
	public void AddSkill(Stack<SkillScript> othSkill) {
		skills = new Stack<SkillScript> ();
		skills.Push (othSkill.Peek());

		ChangeSprite (othSkill.Peek().spriteNeutral, othSkill.Peek().spriteHighlighted);
	}

	public override void ClearSlot() {
		skills.Clear ();
		ChangeSprite (slotEmpty, slotHightlighted);
	}
		
}
