﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;


public class SlotShortcutBar : Slot {

	#region Properties
	private Stack<SkillScript> skills;
	public Stack<SkillScript> Skills {
		get { return skills; }
		set { skills = value; }
	}

	public bool IsEmptySkill {
		get { return skills.Count == 0; }
	}

	public SkillScript CurrentSkill {
		get { return skills.Peek (); }
	}
	#endregion

	#region Protected Methods
	protected override void Awake () {
		skills = new Stack<SkillScript> ();

		GetComponent<RectTransform> ().sizeDelta = new Vector2 (GetComponent<RectTransform> ().sizeDelta.x * 2, GetComponent<RectTransform> ().sizeDelta.y * 2);

		base.Awake ();
	}
	#endregion
		
	#region Public Methods
	public void AddSkill(Stack<SkillScript> othSkill) {
		skills.Push (othSkill.Peek());
		ChangeSprite (othSkill.Peek().spriteNeutral, othSkill.Peek().spriteHighlighted);
	}

	public override void ClearSlot() {
		items.Clear ();
		skills.Clear ();
		ChangeSprite (slotEmpty, slotHightlighted);
		stackTxt.text = string.Empty;
	}

	public override void UseItem () {
		
		// slot got skill on it
		if (!IsEmptySkill) {

			//only active skill can be added to queue
			if (Skills.Peek ().Skill.WeaponType == Equipment.EquippedWeaponType())
				Queue.QueueInstance.AddSkill(skills);

		}
		//slot got item on it
		else 
			base.UseItem ();
	}

	public override void OnPointerClick(PointerEventData eventData) {

		if (eventData.button == PointerEventData.InputButton.Right)
			UseItem ();
		else if (eventData.button == PointerEventData.InputButton.Left && Input.GetKey (KeyCode.LeftShift) && !IsEmpty && !GameObject.Find ("Hover")) {
			Vector2 position;
			RectTransformUtility.ScreenPointToLocalPointInRectangle (InventoryManager.Instance.canvas.transform as RectTransform, Input.mousePosition, InventoryManager.Instance.canvas.worldCamera, out position);
			InventoryManager.Instance.SelectStackSize.SetActive (true);
			InventoryManager.Instance.SelectStackSize.transform.position = InventoryManager.Instance.canvas.transform.TransformPoint (position);
			InventoryManager.Instance.SetStackInfo (items.Count, 130.0f);
		}
	}
	#endregion

}
