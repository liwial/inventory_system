﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;
using System.Xml.Serialization;
using System.IO;


public enum MobState {ALIVE, DEAD};

public class InventoryManager : MonoBehaviour {

	#region InventoryManager Instance 
	private static InventoryManager instance;
	public static InventoryManager Instance {
		get {
			if (instance == null)
				instance = FindObjectOfType<InventoryManager> ();
			return instance;
		}
	}
	#endregion

	#region Prefabs
	public GameObject slotPrefab;
	public GameObject slotChestPrefab;
	public GameObject iconPrefab; // for Hover Icon
	public GameObject tooltipPrefab;
	public GameObject messagePrefab;
	public GameObject stackSizePrefab;
	public GameObject questionPrefab;
	public GameObject promptPrefab;
	public GameObject movingSlotPrefab;

	public GameObject inventoryHolderPrefab;
	public GameObject chestInventoryHolderPrefab;
	public GameObject shortcutBarHolderPrefab;
	public GameObject skillWindowHolderPrefab;

	public GameObject quitButtonPrefab;
	#endregion

	#region Public fields
	public Canvas canvas;
	public EventSystem eventSystem;

	[HideInInspector]
	public Text goldTxt;
	[HideInInspector]
	public Color lightGray = new Color (0.5f, 0.5f, 0.5f, 0.5f);

	public GameObject itemObject;
	#endregion

	#region Private fields
	private List<GameObject> messages = new List<GameObject>();
	private int maxMessages;
	private float messageHeight;
	private Vector3 messagePosition;
	#endregion

	#region Properties
	private GameObject selectStackSize;
	public GameObject SelectStackSize {
		get { return selectStackSize; }
		set { selectStackSize = value; }
	}

	private Text stackText;
	public Text StackText {
		get { return stackText; }
		set { stackText = value; }
	}
		
	private GameObject hoverObject; 
	public GameObject HoverObject {
		get { return hoverObject; }
		set { hoverObject = value; }
	}

	private GameObject tooltipObject;
	public GameObject TooltipObject {
		get { return tooltipObject; }
		set { tooltipObject = value; }
	}

	private Text sizeTextObject;
	public Text SizeTextObject {
		get { return sizeTextObject; }
		set { sizeTextObject = value; }
	}

	private Text visualTextObject;
	public Text VisualTextObject {
		get { return visualTextObject; }
		set { visualTextObject = value; }
	} 

	private GameObject questionObject;
	public GameObject QuestionObject {
		get { return questionObject; }
		set { questionObject = value; }
	}

	private GameObject promptObject;
	public GameObject PromptObject {
		get { return promptObject; }
		set { promptObject = value; }
	}

	private Slot movingSlotObject;
	public Slot MovingSlot {
		get { return movingSlotObject; }
		set { movingSlotObject = value; }
	}
		
	private Slot from; 
	public Slot From {
		get { return from; }
		set { from = value; }
	}

	private Slot to;
	public Slot To {
		get { return to; }
		set { to = value; }
	}

	private GameObject clicked;
	public GameObject Clicked {
		get { return clicked; }
		set { clicked = value; }
	}


	private int splitAmount;
	public int SplitAmount {
		get { return splitAmount; }
		set { splitAmount = value; }
	}

	private int maxStackCount;
	public int MaxStackCount {
		get { return maxStackCount; }
		set { maxStackCount = value; }
	}
		
	private ItemContainer itemContainer = new ItemContainer();
	public ItemContainer ItemContainer {
		get { return itemContainer; }
		set { itemContainer = value; }
	}

	private SkillContainer skillContainer = new SkillContainer();
	public SkillContainer SkillContainer {
		get { return skillContainer; }
		set { skillContainer = value; }
	}

	private CharacterContainer characterContainer = new CharacterContainer();
	public CharacterContainer CharacterContainer {
		get { return characterContainer; }
		set { characterContainer = value; }
	}
		
	private GameObject inventoryHolder;
	public GameObject InventoryHolder {
		get { return inventoryHolder; }
		set { inventoryHolder = value; }
	}

	private GameObject chestInventoryHolder;
	public GameObject ChestInventoryHolder {
		get { return chestInventoryHolder; }
		set { chestInventoryHolder = value; }
	}

	private GameObject shortcutBarHolder;
	public GameObject ShortcutBarHolder {
		get { return shortcutBarHolder; }
		set { shortcutBarHolder = value; }
	}

	private GameObject skillWindowObject;
	public GameObject SkillWindowObject {
		get { return skillWindowObject; }
		set { skillWindowObject = value; }
	}

	private Vector3 chestInventoryPosition;
	public Vector3 ChestInventoryPosition {
		get { return chestInventoryPosition; }
	}

	private Vector3 skillWindowPositionInvisible;
	public Vector3 SkillWindowPositionInvisible {
		get { return skillWindowPositionInvisible; }
	}

	private Vector3 skillWindowPositionVisible;
	public Vector3 SkillWindowPositionVisible {
		get { return skillWindowPositionVisible; }
	}

	private UIStates gameUIStates;
	public UIStates GameUIStates {
		get { return gameUIStates; }
		set { gameUIStates = value; }
	}
		
	private GameObject quitButton;
	public GameObject QuitButton {
		get { return quitButton; }
		set { quitButton = value; }
	}
	#endregion

	#region Private Methods
	void Awake() {
		SetVectors ();
		LoadFromXMLS ();
		InstantiatePrefabs ();
	}

	void Start() {
		gameUIStates = UIStates.DEFAULT;
		maxMessages = 3;
		GameObject messageObject = (GameObject)Instantiate (messagePrefab, transform.position, Quaternion.identity);
		messageHeight = messageObject.GetComponent<RectTransform> ().rect.height;
		messagePosition = messageObject.GetComponent<RectTransform> ().position;
        Destroy(messageObject);

		Inventory.Instance.SetFirstTabOrder ();
	}

	void Update () {

		if (gameUIStates == UIStates.DEFAULT) {
			if (Input.GetKeyDown (KeyCode.I))
				OpenInventory ();
			if (Input.GetKeyDown (KeyCode.P))
				OpenSkillWindow ();
		}
	}

	private void SetVectors() {
		chestInventoryPosition = new Vector3 (0, 20, 0);

		skillWindowPositionInvisible = new Vector3 (60.0f, 1000.0f, 0);  
		skillWindowPositionVisible = new Vector3 (60.0f, 20f, 0);  

	}

	private void LoadFromXMLS () {
		Type[] itemTypes = { typeof(Weapon), typeof(Armor), typeof(Boots), typeof(Ammo), typeof(Consumable), typeof(Snare), typeof(PlotRelated) };
		XmlSerializer serializerItem = new XmlSerializer (typeof (ItemContainer), itemTypes);
		TextReader textReaderItem = new StreamReader (Application.streamingAssetsPath + "/XMLS/Items.xml");
		itemContainer = (ItemContainer)serializerItem.Deserialize (textReaderItem);
		textReaderItem.Close ();

		Type[] skillTypes = { typeof(Skill) }; 
		XmlSerializer serializerSkill = new XmlSerializer (typeof (SkillContainer), skillTypes);
		TextReader textReaderSkill = new StreamReader (Application.streamingAssetsPath + "/XMLS/Skills.xml");
		skillContainer = (SkillContainer)serializerSkill.Deserialize (textReaderSkill);
		textReaderSkill.Close ();

		Type[] characterTypes = { typeof(BasicCharacter) }; 
		XmlSerializer serializerCharacter = new XmlSerializer (typeof (CharacterContainer), characterTypes);
		TextReader textReaderCharacter = new StreamReader (Application.streamingAssetsPath + "/XMLS/Characters.xml");
		characterContainer = (CharacterContainer)serializerCharacter.Deserialize (textReaderCharacter);
		textReaderCharacter.Close ();
	}

	private void SetPrefabParameters(GameObject objectName, string name, Vector2 anchorMin, Vector2 anchorMax, Vector2 pivot, Vector3 anchoredPosition) {
		objectName.SetActive (true);
		objectName.name = name;

		objectName.GetComponent<RectTransform> ().SetParent (GameObject.Find ("UICanvas").transform, true);

		objectName.GetComponent<RectTransform> ().anchorMin = anchorMin;
		objectName.GetComponent<RectTransform> ().anchorMax = anchorMax;
		objectName.GetComponent<RectTransform> ().pivot = pivot;

		objectName.GetComponent<RectTransform> ().anchoredPosition = anchoredPosition;
		objectName.GetComponent<RectTransform> ().localScale = new Vector3 (1.0f, 1.0f, 1.0f);
	}
		
	private void InstantiatePrefabs() {

		inventoryHolder = (GameObject)Instantiate (inventoryHolderPrefab, transform.position, Quaternion.identity);
		SetPrefabParameters (inventoryHolder, "InventoryHolder", new Vector2 (0, 1.0f), new Vector2 (0, 1.0f), new Vector2 (0, 1.0f), new Vector3 (40.0f, -320.0f, 0));

		goldTxt = inventoryHolder.gameObject.transform.GetChild (0).GetChild (2).GetChild (1).gameObject.GetComponent<Text>();

		chestInventoryHolder = (GameObject)Instantiate (chestInventoryHolderPrefab, transform.position, Quaternion.identity);
		SetPrefabParameters (chestInventoryHolder, "ChestInventoryHolder", new Vector2 (0.5f, 0.5f), new Vector2 (0.5f, 0.5f), new Vector2 (0.5f, 0.5f), chestInventoryPosition); 

		chestInventoryHolder.transform.GetChild (0).gameObject.SetActive (false);

		skillWindowObject = (GameObject)Instantiate (skillWindowHolderPrefab, transform.position, Quaternion.identity);
		SetPrefabParameters (skillWindowObject, "SkillWindowHolder", new Vector2 (1.0f, 0.5f), new Vector2 (1.0f, 0.5f), new Vector2 (0.5f, 0.5f), skillWindowPositionInvisible); 

		shortcutBarHolder = (GameObject)Instantiate (shortcutBarHolderPrefab, transform.position, Quaternion.identity);
		SetPrefabParameters (shortcutBarHolder, "ShortcutBarHolder", new Vector2 (0.5f, 0), new Vector2 (0.5f, 0), new Vector2 (0.5f, 0), new Vector3 (0.0f, 32f, 0));   

		tooltipObject = (GameObject)Instantiate (tooltipPrefab, transform.position, Quaternion.identity);
		tooltipObject.transform.SetParent (GameObject.Find ("UICanvas").transform, true);
		tooltipObject.transform.localScale = new Vector3 (1.0f, 1.0f, 1.0f);
		tooltipObject.name = "Tooltip";
		tooltipObject.SetActive (false);

		sizeTextObject = tooltipObject.GetComponentInChildren<Text>();
		visualTextObject = sizeTextObject.transform.GetChild (1).GetComponent<Text> ();

		selectStackSize = (GameObject)Instantiate (stackSizePrefab, transform.position, Quaternion.identity);
		selectStackSize.transform.SetParent (GameObject.Find ("UICanvas").transform, true);
		selectStackSize.transform.localScale = new Vector3 (1.0f, 1.0f, 1.0f);
		selectStackSize.name = "SelectStackSize";
		selectStackSize.SetActive (false);

		stackText = selectStackSize.gameObject.transform.GetChild (2).GetChild (0).gameObject.GetComponent<Text>();

		questionObject = (GameObject)Instantiate (questionPrefab, transform.position, Quaternion.identity);
		questionObject.transform.SetParent (GameObject.Find ("UICanvas").transform, true);
		questionObject.transform.localScale = new Vector3 (1.0f, 1.0f, 1.0f);
		questionObject.name = "Question";
		questionObject.SetActive (false);

		promptObject = (GameObject)Instantiate (promptPrefab, transform.position, Quaternion.identity);
		promptObject.transform.SetParent (GameObject.Find ("UICanvas").transform, true);
		promptObject.transform.localScale = new Vector3 (1.0f, 1.0f, 1.0f);
		promptObject.name = "Prompt";
		promptObject.SetActive (false);

		quitButton = (GameObject)Instantiate (quitButtonPrefab, transform.position, Quaternion.identity);
		SetPrefabParameters (quitButton, "ButtonQuit", new Vector2 (0, 1.0f), new Vector2 (0, 1.0f), new Vector2 (0, 1.0f), new Vector3 (60.0f, -10.0f, 0));

	}

	private IEnumerator FadeMessage(GameObject messageObject, int duration) {

		CanvasGroup canvasGroup = messageObject.GetComponent<CanvasGroup> ();

		float rate = 1.0f / duration;
		int startAlpha = 1;
		int endAlpha = 0;
		float progress = 0.0f;

		while (progress < 1.0) {
			canvasGroup.alpha = Mathf.Lerp (startAlpha, endAlpha, progress);
			progress += rate * Time.deltaTime;

			yield return null;
		}

		messages.Remove (messageObject);
		Destroy(messageObject);
	}

	private void CheckPreviousMessages() {
		if (messages.Count >= maxMessages) {
			int excess = messages.Count - maxMessages;

			for (int i = 0; i <= excess; i++) {
				GameObject toDeactivate = messages [messages.Count - 1];
				toDeactivate.SetActive (false);
				toDeactivate.transform.localPosition = new Vector3 (-370.0f, 1000.0f, 0.0f); // just somewhere outside of the screen

				messages.RemoveAt (messages.Count - 1);
			}
		}

		foreach (GameObject message in messages) {
			Vector3 prevPosition = message.transform.localPosition;
			message.transform.localPosition = new Vector3 (prevPosition.x, prevPosition.y + messageHeight, prevPosition.z);
		}
	}
	#endregion

	#region Public Methods
	public void ShowMessage(string message) {
		CheckPreviousMessages ();

		GameObject messageObject = (GameObject)Instantiate (messagePrefab, transform.position, Quaternion.identity);
		messageObject.SetActive (true);
		messageObject.GetComponent<Text> ().text = message;
		messageObject.transform.SetParent (GameObject.Find ("InventoryCanvas").transform, true);
		messageObject.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (messagePosition.x, messagePosition.y);
		messageObject.transform.localScale = new Vector3 (1.0f, 1.0f, 1.0f);

		messages.Insert (0, messageObject);

		StartCoroutine (FadeMessage (messageObject, 5));
	}

	public static void ShowMessageStatic (string message) {
		InventoryManager.Instance.ShowMessage(message);
	}

	public void SetStackInfo(int maxStackCount, float deltaY) {
		InventoryManager.Instance.selectStackSize.SetActive (true);
		float xPos = InventoryManager.Instance.selectStackSize.transform.localPosition.x + 70.0f;
		float yPos = InventoryManager.Instance.selectStackSize.transform.localPosition.y + deltaY; //- 90.0f;
		InventoryManager.Instance.selectStackSize.transform.localPosition = new Vector3(xPos, yPos, InventoryManager.Instance.selectStackSize.transform.localPosition.z);

		tooltipObject.SetActive (false);

		InventoryManager.Instance.SplitAmount = 0;
		this.maxStackCount = maxStackCount;
		stackText.text = splitAmount.ToString ();
	}

	public void ShowButtonTooltip (GameObject button) {

		string msg = string.Empty;
		bool canDo = false;

		if (button.name == "GeneralButton") {
			if (Inventory.Instance.IsOpen) { 
				msg = "Show general items";
				canDo = true;
			}
		} else if (button.name == "QuestButton") {
			if (Inventory.Instance.IsOpen) { 
			msg = "Show quest items";
				canDo = true;
			}
		} else if (button.name == "TakeButton") {
			if (button.transform.parent.parent.GetComponent<CanvasGroup>().alpha > 0) {
				msg = "Take everything";
				canDo = true;
			}
		} else if (button.name == "CloseButton") {
			if (button.transform.parent.parent.GetComponent<CanvasGroup>().alpha > 0) {
			msg = "Close window";
				canDo = true;
			}
		} else if (button.name == "InvButton") {
			msg = "Open/close inventory";
			canDo = true;
		} else if (button.name == "SkillButton") {
			msg = "Open/close skill window";
			canDo = true;
		}

		if (canDo) {
			InventoryManager.Instance.visualTextObject.text = string.Format ("<size=24><i><color=white>{0}</color></i></size>", msg);

			InventoryManager.Instance.sizeTextObject.text = InventoryManager.Instance.visualTextObject.text;

			InventoryManager.Instance.TooltipObject.SetActive (true);

			float xPos = button.transform.position.x + 0.05f;
			float yPos = button.transform.position.y + 0.6f; 

			InventoryManager.Instance.TooltipObject.transform.position = new Vector3 (xPos, yPos, button.transform.position.z);
			}

	}

	public void HideButtonTooltip () {
		InventoryManager.Instance.TooltipObject.SetActive (false);
	}
		
	public void OpenInventory () {
		Inventory.Instance.Open ();
	}

    //
    public void GetInventoryAndSaveForTrade() {
        Inventory.Instance.SaveContentForTrade();
    }
    //


	public void OpenSkillWindow () {
		SkillWindow.SkillInstance.Open ();
	}
		
	public void Save () {
		Inventory.Instance.SaveContent ("general");
		Inventory.Instance.SaveContent ("quest");

		ShortcutBar.ShortcutBarInstance.SaveContent ("main");

		Equipment.SaveContent ("equipped");

		Queue.QueueInstance.SaveContent ("queue");
	}

	public void Load () {
		Inventory.Instance.LoadContent ("general");
		Inventory.Instance.LoadContent ("quest");

		ShortcutBar.ShortcutBarInstance.LoadContent ("main");

		Equipment.LoadContent ("equipped");

		Queue.QueueInstance.LoadContent ("queue");
	}

	public bool FromExists() {
		if (from != null)
			return true;
		else
			return false;
	}

	public bool FromSkillWidow() {
		if (from.GetComponent<SlotSkill>() != null)
			return true;
		else
			return false;
	}

	public bool FromShortcutWindow() {
		if (from.GetComponent<SlotShortcutBar>() != null)
			return true;
		else
			return false;
	}
		
	public ItemScript FindItemByName (string name) {

		BasicItem bi;

		if (InventoryManager.Instance.ItemContainer.GetWeapon (name) != null)
			bi = InventoryManager.Instance.ItemContainer.GetWeapon (name);
		else if (InventoryManager.Instance.ItemContainer.GetArmor (name) != null)
			bi = InventoryManager.Instance.ItemContainer.GetArmor (name);
		else if (InventoryManager.Instance.ItemContainer.GetBoots (name) != null)
			bi = InventoryManager.Instance.ItemContainer.GetBoots (name);
		else if (InventoryManager.Instance.ItemContainer.GetAmmo (name) != null)
			bi = InventoryManager.Instance.ItemContainer.GetAmmo (name);
		else if (InventoryManager.Instance.ItemContainer.GetConsumable (name) != null)
			bi = InventoryManager.Instance.ItemContainer.GetConsumable (name);
		else if (InventoryManager.Instance.ItemContainer.GetSnare (name) != null)
			bi = InventoryManager.Instance.ItemContainer.GetSnare (name);
		else if (InventoryManager.Instance.ItemContainer.GetPlotRelated (name) != null)
			bi = InventoryManager.Instance.ItemContainer.GetPlotRelated (name);
		else
			bi = null;

		if (bi != null) {
			GameObject tmp = Instantiate (InventoryManager.Instance.itemObject);
			tmp.AddComponent<ItemScript> ();
			ItemScript newItem = tmp.GetComponent<ItemScript> ();
			newItem.Item = bi;
			Destroy (tmp);
			return newItem;
		}
		else
			return null;
	}

	public void CheckTab() {
		if (InventoryManager.Instance.From != null && !InventoryManager.Instance.From.IsEmpty) {
			if (InventoryManager.Instance.From.CurrentItem.Item.IsQuestRelated) {
				Inventory.Instance.ChangeTabOrder ("quest", false);
			} else if (!InventoryManager.Instance.From.CurrentItem.Item.IsQuestRelated) {
				Inventory.Instance.ChangeTabOrder ("general", false);
			}
		}
	}
	#endregion
}
