﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterContainer {

	private List<BasicCharacter> charactersList = new List<BasicCharacter> ();

	public List<BasicCharacter> SpecialCharactersList {
		get { return charactersList; }
		set { charactersList = value; }
	}

	public BasicCharacter GetCharacter (string name) {
		return charactersList.Find (basicCharacter => basicCharacter.Name == name);
	}

}
