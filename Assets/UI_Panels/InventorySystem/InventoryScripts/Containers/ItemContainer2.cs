﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemContainer2 {

	private List<Weapon> weaponsList = new List<Weapon>();
	private List<Armor> armorsList = new List<Armor>();
	private List<Boots> bootsList = new List<Boots>();
	private List<Ammo> ammosList = new List<Ammo>();
	private List<Consumable> consumablesList = new List<Consumable>();
	private List<Snare> snaresList = new List<Snare>();
	private List<PlotRelated> plotRelatedList = new List<PlotRelated> ();

	public List<Weapon> WeaponsList {
		get { return weaponsList; }
		set { weaponsList = value; }
	}

	public List<Armor> ArmorsList {
		get { return armorsList; }
		set { armorsList = value; }
	}

	public List<Boots> BootsList {
		get { return bootsList; }
		set { bootsList = value; }
	}

	public List<Ammo> AmmosList {
		get { return ammosList; }
		set { ammosList = value; }
	}

	public List<Consumable> ConsumablesList {
		get { return consumablesList; }
		set { consumablesList = value; }
	}

	public List<Snare> SnaresList {
		get { return snaresList; }
		set { snaresList = value; }
	}

	public List<PlotRelated> PlotRelatedList {
		get { return plotRelatedList; }
		set { plotRelatedList = value; }
	}
		
	public Weapon GetWeapon (string name) {
		return weaponsList.Find (basicItem => basicItem.Name == name);
	}

	public Armor GetArmor (string name) {
		return armorsList.Find (basicItem => basicItem.Name == name);
	}

	public Boots GetBoots (string name) {
		return bootsList.Find (basicItem => basicItem.Name == name);
	}

	public Ammo GetAmmo (string name) {
		return ammosList.Find (basicItem => basicItem.Name == name);
	}

	public Consumable GetConsumable (string name) {
		return consumablesList.Find (basicItem => basicItem.Name == name);
	}

	public Snare GetSnare (string name) {
		return snaresList.Find (basicItem => basicItem.Name == name);
	}

	public PlotRelated GetPlotRelated (string name) {
		return plotRelatedList.Find (basicItem => basicItem.Name == name);
	}

}
