﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemContainer {

	private List<BasicItem> weaponsList = new List<BasicItem>();
	private List<BasicItem> armorsList = new List<BasicItem>();
	private List<BasicItem> bootsList = new List<BasicItem>();
	private List<BasicItem> ammosList = new List<BasicItem>();
	private List<BasicItem> consumablesList = new List<BasicItem>();
	private List<BasicItem> snaresList = new List<BasicItem>();
	private List<BasicItem> plotRelatedList = new List<BasicItem> ();

	public List<BasicItem> WeaponsList {
		get { return weaponsList; }
		set { weaponsList = value; }
	}

	public List<BasicItem> ArmorsList {
		get { return armorsList; }
		set { armorsList = value; }
	}

	public List<BasicItem> BootsList {
		get { return bootsList; }
		set { bootsList = value; }
	}

	public List<BasicItem> AmmosList {
		get { return ammosList; }
		set { ammosList = value; }
	}

	public List<BasicItem> ConsumablesList {
		get { return consumablesList; }
		set { consumablesList = value; }
	}

	public List<BasicItem> SnaresList {
		get { return snaresList; }
		set { snaresList = value; }
	}

	public List<BasicItem> PlotRelatedList {
		get { return plotRelatedList; }
		set { plotRelatedList = value; }
	}
		
	public BasicItem GetWeapon (string name) {
		return weaponsList.Find (basicItem => basicItem.Name == name);
	}

	public BasicItem GetArmor (string name) {
		return armorsList.Find (basicItem => basicItem.Name == name);
	}

	public BasicItem GetBoots (string name) {
		return bootsList.Find (basicItem => basicItem.Name == name);
	}

	public BasicItem GetAmmo (string name) {
		return ammosList.Find (basicItem => basicItem.Name == name);
	}

	public BasicItem GetConsumable (string name) {
		return consumablesList.Find (basicItem => basicItem.Name == name);
	}

	public BasicItem GetSnare (string name) {
		return snaresList.Find (basicItem => basicItem.Name == name);
	}

	public BasicItem GetPlotRelated (string name) {
		return plotRelatedList.Find (basicItem => basicItem.Name == name);
	}

}
