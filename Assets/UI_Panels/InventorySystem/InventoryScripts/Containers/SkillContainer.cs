﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkillContainer {

	private List<BasicSkill> skillsList = new List<BasicSkill>();

	public List<BasicSkill> SkillsList {
		get { return skillsList; }
		set { skillsList = value; }
	}

	public BasicSkill GetSkill (string name) {
		return skillsList.Find (basicSkill => basicSkill.Name == name);
	}

}
