﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;


    public class Inventory : MonoBehaviour {

        #region Inventory Instance
        private static Inventory instance;
        public static Inventory Instance {
            get { return instance; }
        }
        #endregion

        #region Variables
        [HideInInspector]
        public bool instantClose = false;
        [HideInInspector]
        public static bool isMouseInside = false;

        protected int columns;
        protected int rows;
        protected float fadeTime;
        protected float slotSize = 64.0f;

        protected RectTransform inventoryRect;
        protected float inventoryWidth, inventoryHeight;
        protected List<GameObject> allGeneralSlots, allQuestSlots;
        protected float hoverYOffset;
        protected bool fadingIn;

        private float tabYOffset = 1080.0f;
        private bool isFrontTab = true;
        private bool isBackTab = false;
        private int gold = 0;
        #endregion

        #region Properties
        protected CanvasGroup canvasGroup;
        public CanvasGroup CanvasGroup {
            get { return canvasGroup; }
        }

        protected bool fadingOut;
        public bool FadingOut {
            get { return fadingOut; }
        }

        private bool isOpen;
        public bool IsOpen {
            get { return isOpen; }
            set { isOpen = value; }
        }

        protected int emptySlots;
        public int EmptySlots {
            get { return emptySlots; }
            set { emptySlots = value; }
        }

        protected int emptySlotsQuest;
        public int EmptySlotsQuest {
            get { return emptySlotsQuest; }
            set { emptySlotsQuest = value; }
        }
        #endregion

        #region Protected Methods
        protected virtual void Awake() {
            columns = 8;
            rows = 8;
            fadeTime = 1.0f;

            isOpen = false;

            if (instance != null && instance != this) {
                Destroy(this);
            } else {
                instance = (Inventory)this;
            }
        }

        protected virtual void Start() {
            hoverYOffset = slotSize * 0.2f;
            isOpen = false;
            canvasGroup = transform.parent.GetComponent<CanvasGroup>();
            canvasGroup.interactable = false;
            canvasGroup.blocksRaycasts = false;

        CreateLayout("front");
            CreateLayout("back");

            if (!InventoryManager.Instance.MovingSlot) {
                CreateMovingSlot();
            }
        }

        protected virtual void CreateLayout(string layer) {

            List<GameObject> allSlots = new List<GameObject>();
            float yOffset = 0.0f;

            if (layer == "front") {
                allGeneralSlots = new List<GameObject>();
                allSlots = allGeneralSlots;
                yOffset = 0.0f;
            } else if (layer == "back") {
                allQuestSlots = new List<GameObject>();
                allSlots = allQuestSlots;
                yOffset = tabYOffset;
            }

            emptySlots = columns * rows;
            emptySlotsQuest = emptySlots;

            inventoryWidth = columns * slotSize;
            inventoryHeight = rows * slotSize;

            inventoryRect = GetComponent<RectTransform>();
            inventoryRect.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, inventoryWidth);
            inventoryRect.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, inventoryHeight);

            for (int y = 0; y < rows; y++) {

                for (int x = 0; x < columns; x++) {

                    GameObject newSlot = (GameObject)Instantiate(InventoryManager.Instance.slotPrefab);
                    RectTransform slotRect = newSlot.GetComponent<RectTransform>();

                    int tmpRow = y + 1;
                    int tmpCol = x + 1;

                    newSlot.name = "Slot" + tmpRow.ToString() + tmpCol.ToString();
                    newSlot.transform.SetParent(this.GetComponent<Transform>()); 

                    slotRect.localPosition = new Vector3((slotSize * x), -(slotSize * y + yOffset)); 
                    slotRect.localScale = new Vector3(1.0f, 1.0f, 0);

                    allSlots.Add(newSlot);

                    newSlot.GetComponent<Button>().onClick.AddListener
                    (
                        delegate {
                            MoveItem(newSlot);
                        }
                    );

                    EventTrigger.Entry entry = new EventTrigger.Entry();
                    entry.eventID = EventTriggerType.PointerEnter;
                    entry.callback.AddListener((eventdata) => {
                        ShowToolTip(newSlot);
                    });
                    newSlot.GetComponent<EventTrigger>().triggers.Add(entry);

                    entry.eventID = EventTriggerType.PointerExit;
                    entry.callback.AddListener((eventdata) => {
                        HideToolTip();
                    });
                    newSlot.GetComponent<EventTrigger>().triggers.Add(entry);

                }
            }
        }

        protected virtual bool PlaceEmpty(ItemScript item) {

            if (emptySlots > 0 && !item.Item.IsQuestRelated) {
                foreach (GameObject slot in allGeneralSlots) {
                    Slot tmp = slot.GetComponent<Slot>();

                    if (tmp.IsEmpty) {
                        tmp.AddItem(item);
                        return true;
                    }
                }
            } else if (emptySlotsQuest > 0 && item.Item.IsQuestRelated) {
                foreach (GameObject slot in allQuestSlots) {
                    Slot tmp = slot.GetComponent<Slot>();

                    if (tmp.IsEmpty) {
                        tmp.AddItem(item);
                        return true;
                    }
                }
            }

            return false;
        }

        protected void CreateHoverIcon() {

            InventoryManager.Instance.HoverObject = (GameObject)Instantiate(InventoryManager.Instance.iconPrefab);
            InventoryManager.Instance.HoverObject.GetComponent<Image>().sprite = InventoryManager.Instance.Clicked.GetComponent<Image>().sprite;
            InventoryManager.Instance.HoverObject.GetComponent<Image>().color = InventoryManager.Instance.Clicked.GetComponent<Image>().color;

            InventoryManager.Instance.HoverObject.name = "Hover";

            RectTransform hoverTransform = InventoryManager.Instance.HoverObject.GetComponent<RectTransform>();
            RectTransform clickedTransform = InventoryManager.Instance.Clicked.GetComponent<RectTransform>();

            hoverTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, clickedTransform.sizeDelta.x);
            hoverTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, clickedTransform.sizeDelta.y);

            InventoryManager.Instance.HoverObject.transform.SetParent(GameObject.Find("InventoryCanvas").transform, true);

            InventoryManager.Instance.HoverObject.transform.localScale = new Vector3(1.0f, 1.0f, 0);

            if (!InventoryManager.Instance.MovingSlot.IsEmpty)
                InventoryManager.Instance.HoverObject.transform.GetChild(0).GetComponent<Text>().text = InventoryManager.Instance.MovingSlot.Items.Count > 1 ? InventoryManager.Instance.MovingSlot.Items.Count.ToString() : string.Empty;
            else if (InventoryManager.Instance.Clicked.GetComponent<Slot>().Items.Count > 1)
                InventoryManager.Instance.HoverObject.transform.GetChild(0).GetComponent<Text>().text = InventoryManager.Instance.Clicked.GetComponent<Slot>().Items.Count.ToString();
        }

        protected virtual IEnumerator FadeOut() {
            if (!fadingOut) {
                fadingOut = true;
                fadingIn = false;
                StopCoroutine("FadeIn");

                float rate = 1.0f / fadeTime;
                float progress = 0.0f;

                float startAlpha;
                CanvasGroup cg;

                if (canvasGroup != null) {
                    cg = canvasGroup;
                    startAlpha = canvasGroup.alpha;
                } else {
                    cg = transform.parent.parent.GetComponent<CanvasGroup>();
                    startAlpha = cg.alpha;
                }

                while (progress < 1.0) {
                    cg.alpha = Mathf.Lerp(startAlpha, 0, progress);
                    progress += rate * Time.deltaTime;

                    if (instantClose)
                        break;

                    yield return null;
                }

                cg.alpha = 0;
                cg.interactable = false;
                cg.blocksRaycasts = false; 

                fadingOut = false;

                instantClose = false;
            }
        }

        protected IEnumerator FadeIn() {
            if (!fadingIn) {
                fadingIn = true;
                fadingOut = false;
                StopCoroutine("FadeOut");

                float rate = 1.0f / fadeTime;
                float progress = 0.0f;

                float startAlpha;
                CanvasGroup cg;

                if (canvasGroup != null) {
                    cg = canvasGroup;
                    startAlpha = canvasGroup.alpha;
                } else {
                    cg = transform.parent.parent.GetComponent<CanvasGroup>();
                    startAlpha = cg.alpha;
                }

                while (progress < 1.0) {
                    cg.alpha = Mathf.Lerp(startAlpha, 1, progress);
                    progress += rate * Time.deltaTime;
                    yield return null;
                }

                cg.alpha = 1;
                cg.interactable = true;
                cg.blocksRaycasts = true;

                fadingIn = false;
            }
        }
        #endregion

        #region Private Methods
        void Update() {
            if (InventoryManager.Instance.HoverObject != null) {
                Vector2 position;
                RectTransformUtility.ScreenPointToLocalPointInRectangle(InventoryManager.Instance.canvas.transform as RectTransform, Input.mousePosition, InventoryManager.Instance.canvas.worldCamera, out position);
                position.Set(position.x + hoverYOffset, position.y - hoverYOffset);
                InventoryManager.Instance.HoverObject.transform.position = InventoryManager.Instance.canvas.transform.TransformPoint(position);
                InventoryManager.Instance.HoverObject.transform.localScale = new Vector3(1.8f, 1.8f, 0);
            }
        }

        private void CreateMovingSlot() {
            GameObject movingSlotObj = (GameObject)Instantiate(InventoryManager.Instance.movingSlotPrefab, transform.position, Quaternion.identity);
            movingSlotObj.transform.SetParent(GameObject.Find("UICanvas").transform, true);
            movingSlotObj.transform.localPosition = new Vector3(0.0f, tabYOffset);
            movingSlotObj.transform.localScale = new Vector3(1.0f, 1.0f, 0);
            InventoryManager.Instance.MovingSlot = movingSlotObj.GetComponent<Slot>();
            InventoryManager.Instance.MovingSlot.name = "MovingSlot";
        }

        private void PutItemBack() {
            if (InventoryManager.Instance.From != null) {
                Destroy(GameObject.Find("Hover"));
                InventoryManager.Instance.From.GetComponent<Image>().color = Color.white;
                InventoryManager.Instance.From = null;
            } else if (!InventoryManager.Instance.MovingSlot.IsEmpty) {
                Destroy(GameObject.Find("Hover"));
                foreach (ItemScript item in InventoryManager.Instance.MovingSlot.Items) {
                    InventoryManager.Instance.Clicked.GetComponent<Slot>().AddItem(item);
                }
                InventoryManager.Instance.MovingSlot.ClearSlot();
                InventoryManager.Instance.MovingSlot.name = "MovingSlot";
            }
            InventoryManager.Instance.SelectStackSize.SetActive(false);
        }
        #endregion

        #region Public Methods
        public void DestroyItem(bool destroy) {

            if (destroy && InventoryManager.Instance.From != null) {

                if (!InventoryManager.Instance.From.GetComponent<Slot>().CurrentItem.Item.IsQuestRelated)
                    emptySlots++;
                else if (InventoryManager.Instance.From.GetComponent<Slot>().CurrentItem.Item.IsQuestRelated)
                    emptySlotsQuest++;

                InventoryManager.Instance.From.GetComponent<Image>().color = Color.white;
                InventoryManager.Instance.From.ClearSlot();
                Destroy(GameObject.Find("Hover"));
                InventoryManager.Instance.To = null;
                InventoryManager.Instance.From = null;
            } else if (destroy && !InventoryManager.Instance.MovingSlot.IsEmpty) {
                InventoryManager.Instance.MovingSlot.ClearSlot();
                InventoryManager.Instance.MovingSlot.name = "MovingSlot";
                Destroy(GameObject.Find("Hover"));
            }

            InventoryManager.Instance.QuestionObject.SetActive(false);
        }

        public virtual void SetQuestion() {
            if (InventoryManager.Instance.From != null) {
                InventoryManager.Instance.QuestionObject.SetActive(true);

                Vector2 position;
                RectTransformUtility.ScreenPointToLocalPointInRectangle(InventoryManager.Instance.canvas.transform as RectTransform, Input.mousePosition, InventoryManager.Instance.canvas.worldCamera, out position);
                position.Set(position.x + hoverYOffset, position.y + 50.0f);
                InventoryManager.Instance.QuestionObject.transform.position = InventoryManager.Instance.canvas.transform.TransformPoint(position);
            } else if (!InventoryManager.Instance.MovingSlot.IsEmpty) {
                InventoryManager.Instance.QuestionObject.SetActive(true);

                Vector2 position;
                RectTransformUtility.ScreenPointToLocalPointInRectangle(InventoryManager.Instance.canvas.transform as RectTransform, Input.mousePosition, InventoryManager.Instance.canvas.worldCamera, out position);
                position.Set(position.x + hoverYOffset, position.y + 50.0f);
                InventoryManager.Instance.QuestionObject.transform.position = InventoryManager.Instance.canvas.transform.TransformPoint(position);
            }
        }

        public void PointerEnter() {
            if (canvasGroup.alpha > 0)
                isMouseInside = true;
        }

        public void PointerExit() {
            isMouseInside = false;
        }

        public virtual void Open() {
            if (canvasGroup.alpha > 0) {
                StartCoroutine("FadeOut");
                PutItemBack();
                HideToolTip();
                isOpen = false;
                canvasGroup.blocksRaycasts = false;
                canvasGroup.interactable = false;
            } else {
                StartCoroutine("FadeIn");
                isOpen = true;
                canvasGroup.blocksRaycasts = true;
                canvasGroup.interactable = true;
            }
        }

        public virtual void ShowToolTip(GameObject slot) {

            if (!IsOpen) {

                Slot tmpSlot = slot.GetComponent<Slot>();

                if (!tmpSlot.IsEmpty && InventoryManager.Instance.HoverObject == null) {

                    InventoryManager.Instance.VisualTextObject.text = tmpSlot.CurrentItem.GetTooltip();
                    InventoryManager.Instance.SizeTextObject.text = InventoryManager.Instance.VisualTextObject.text;

                    InventoryManager.Instance.TooltipObject.SetActive(true);

                    float xPos = tmpSlot.transform.position.x + 0.05f;
                    float yPos = tmpSlot.transform.position.y - 0.7f;//1.15f; 

                    InventoryManager.Instance.TooltipObject.transform.position = new Vector3(xPos, yPos, tmpSlot.transform.position.z);
                }
            }
        }

        public void HideToolTip() {
            InventoryManager.Instance.TooltipObject.SetActive(false);
        }

        public void AddGold(int goldAmount) {
            gold = gold + goldAmount;
            InventoryManager.Instance.goldTxt.text = gold.ToString();
        }

        public virtual bool AddItem(ItemScript item) {

            if (item.Item.MaxSize == 1) {
                PlaceEmpty(item);
                return true;
            } else {

                List<GameObject> allSlots = new List<GameObject>();

                if (item.Item.IsQuestRelated)
                    allSlots = allQuestSlots;
                else
                    allSlots = allGeneralSlots;

                foreach (GameObject slot in allSlots) {

                    Slot tmp = slot.GetComponent<Slot>();

                    if (!tmp.IsEmpty) {
                        if (tmp.CurrentItem.Item.Name == item.Item.Name && tmp.IsAvailable) {

                            if (!InventoryManager.Instance.MovingSlot.IsEmpty && InventoryManager.Instance.Clicked.GetComponent<Slot>() == tmp.GetComponent<Slot>()) {
                                continue;
                            } else {
                                tmp.AddItem(item);
                                return true;
                            }
                        }
                    }
                }

                if (!item.Item.IsQuestRelated) {
                    if (emptySlots > 0) {
                        return PlaceEmpty(item);
                    }
                } else if (item.Item.IsQuestRelated) {
                    if (emptySlotsQuest > 0) {
                        return PlaceEmpty(item);
                    }
                }
            }
            return false;
        }

        public bool IsSkillSlots() {
            if (InventoryManager.Instance.From != null) {

                if (InventoryManager.Instance.FromSkillWidow())
                    return true;
                else if (InventoryManager.Instance.FromShortcutWindow() && !InventoryManager.Instance.From.GetComponent<SlotShortcutBar>().IsEmptySkill)
                    return true;
                else
                    return false;
            } else
                return false;
        }

        public virtual void MoveItem(GameObject clicked) {

            if (IsSkillSlots())
                return;

            InventoryManager.Instance.Clicked = clicked;

            if (!InventoryManager.Instance.MovingSlot.IsEmpty) {
                Slot tmp = InventoryManager.Instance.Clicked.GetComponent<Slot>();

                if (tmp.IsEmpty) {
                    tmp.AddItems(InventoryManager.Instance.MovingSlot.Items);

                    InventoryManager.Instance.MovingSlot.Items.Clear();
                    InventoryManager.Instance.MovingSlot.name = "MovingSlot";
                    Destroy(GameObject.Find("Hover"));
                } else if (!tmp.IsEmpty && InventoryManager.Instance.MovingSlot.CurrentItem.Item.Type == tmp.CurrentItem.Item.Type && tmp.IsAvailable) {
                    MergeStacks(InventoryManager.Instance.MovingSlot, tmp);
                }

            } else if (InventoryManager.Instance.From == null && InventoryManager.Instance.Clicked.transform.parent.GetComponent<Inventory>().IsOpen && !Input.GetKey(KeyCode.LeftShift)) {
                if (!clicked.GetComponent<Slot>().IsEmpty && !GameObject.Find("Hover")) {
                    InventoryManager.Instance.From = InventoryManager.Instance.Clicked.GetComponent<Slot>();
                    InventoryManager.Instance.From.GetComponent<Image>().color = Color.gray;

                    CreateHoverIcon();
                }
            } else if (InventoryManager.Instance.To == null && !Input.GetKey(KeyCode.LeftShift)) {
                InventoryManager.Instance.To = InventoryManager.Instance.Clicked.GetComponent<Slot>();
                Destroy(GameObject.Find("Hover"));
            }


            if (InventoryManager.Instance.To != null && InventoryManager.Instance.From != null) {
                if (!InventoryManager.Instance.To.IsEmpty && InventoryManager.Instance.From.CurrentItem.Item.Type == InventoryManager.Instance.To.CurrentItem.Item.Type && InventoryManager.Instance.To.IsAvailable) {
                    if (InventoryManager.Instance.From.CurrentItem.Item.Type == ItemType.AMMO) {
                        if (InventoryManager.Instance.From.CurrentItem.Item.Name != InventoryManager.Instance.To.CurrentItem.Item.Name)
                            Slot.SwapItems(InventoryManager.Instance.From, InventoryManager.Instance.To);
                        else
                            MergeStacks(InventoryManager.Instance.From, InventoryManager.Instance.To);
                    } else
                        MergeStacks(InventoryManager.Instance.From, InventoryManager.Instance.To);
                } else {
                    Slot.SwapItems(InventoryManager.Instance.From, InventoryManager.Instance.To);
                }

                InventoryManager.Instance.From.GetComponent<Image>().color = Color.white;
                InventoryManager.Instance.To = null;
                InventoryManager.Instance.From = null;
                Destroy(GameObject.Find("Hover"));
            }
        }

        public GameObject FindFirstFreeSlot() {

            foreach (GameObject g in allGeneralSlots) {
                if (g.GetComponent<Slot>().IsEmpty) {
                    return g;
                }
            }

            return null;
        }

        public void SplitStack() {

            InventoryManager.Instance.SelectStackSize.SetActive(false);
            if (InventoryManager.Instance.SplitAmount == InventoryManager.Instance.MaxStackCount) {
                if (InventoryManager.Instance.Clicked.GetComponent<SlotShortcutBar>() != null) {
                    ShortcutBar.ShortcutBarInstance.MoveItem(InventoryManager.Instance.Clicked);
                } else if (InventoryManager.Instance.Clicked.GetComponent<Slot>() != null) {
                    MoveItem(InventoryManager.Instance.Clicked);
                }
            } else if (InventoryManager.Instance.SplitAmount > 0) {

                if (InventoryManager.Instance.Clicked.GetComponent<SlotShortcutBar>() != null) {
                    InventoryManager.Instance.MovingSlot.Items = InventoryManager.Instance.Clicked.GetComponent<SlotShortcutBar>().RemoveItems(InventoryManager.Instance.SplitAmount);
                    InventoryManager.Instance.MovingSlot.name = InventoryManager.Instance.Clicked.GetComponent<SlotShortcutBar>().name;
                } else if (InventoryManager.Instance.Clicked.GetComponent<Slot>() != null) {
                    InventoryManager.Instance.MovingSlot.Items = InventoryManager.Instance.Clicked.GetComponent<Slot>().RemoveItems(InventoryManager.Instance.SplitAmount);
                    InventoryManager.Instance.MovingSlot.name = InventoryManager.Instance.Clicked.GetComponent<Slot>().name;
                }

                CreateHoverIcon();
            }
        }

        public void ChangeStackText(int i) {
            InventoryManager.Instance.SplitAmount += i;

            if (InventoryManager.Instance.SplitAmount < 0)
                InventoryManager.Instance.SplitAmount = 0;
            if (InventoryManager.Instance.SplitAmount > InventoryManager.Instance.MaxStackCount)
                InventoryManager.Instance.SplitAmount = InventoryManager.Instance.MaxStackCount;

            InventoryManager.Instance.StackText.text = InventoryManager.Instance.SplitAmount.ToString();
        }

        public void MergeStacks(Slot source, Slot destination) {

            if (source.name == destination.name) {
                if (!InventoryManager.Instance.MovingSlot.IsEmpty) {
                    Destroy(GameObject.Find("Hover"));
                    foreach (ItemScript item in InventoryManager.Instance.MovingSlot.Items) {
                        InventoryManager.Instance.Clicked.GetComponent<Slot>().AddItem(item);
                    }
                    InventoryManager.Instance.MovingSlot = null;
                    CreateMovingSlot();
                }
                InventoryManager.Instance.SelectStackSize.SetActive(false);
            } else {

                int max = destination.CurrentItem.Item.MaxSize - destination.Items.Count;
                int count = source.Items.Count < max ? source.Items.Count : max;

                for (int i = 0; i < count; i++) {
                    destination.AddItem(source.RemoveItem());
                }
                if (source.Items.Count == 0) {
                    source.ClearSlot();
                    Destroy(GameObject.Find("Hover"));
                }
            }
        }

        public void SetFirstTabOrder() {
            Transform generalButton = this.transform.parent.GetChild(0).GetChild(0);
            Transform questButton = this.transform.parent.GetChild(0).GetChild(1);
            generalButton.GetComponent<Image>().sprite = generalButton.GetComponent<ButtonGeneral>().imagePressed;
            questButton.GetComponent<Image>().sprite = questButton.GetComponent<ButtonQuest>().imageNotPressed;
        }

        public void ChangeTabOrder(string whichTab, bool isButton) {

            Transform generalButton = this.transform.parent.GetChild(0).GetChild(0);
            Transform questButton = this.transform.parent.GetChild(0).GetChild(1);

            if (isButton)
                PutItemBack();

            if (whichTab == "quest") {

                if (!isBackTab) {

                    foreach (GameObject slot in allGeneralSlots) {
                        RectTransform slotRect = slot.GetComponent<RectTransform>();
                        slotRect.localPosition = new Vector3(slotRect.localPosition.x, slotRect.localPosition.y - tabYOffset, slotRect.localPosition.z);
                    }
                    foreach (GameObject slot in allQuestSlots) {
                        RectTransform slotRect = slot.GetComponent<RectTransform>();
                        slotRect.localPosition = new Vector3(slotRect.localPosition.x, slotRect.localPosition.y + tabYOffset, slotRect.localPosition.z);
                    }

                    isBackTab = true;
                    isFrontTab = false;

                    generalButton.GetComponent<Image>().sprite = generalButton.GetComponent<ButtonGeneral>().imageNotPressed;
                    questButton.GetComponent<Image>().sprite = questButton.GetComponent<ButtonQuest>().imagePressed;
                }
            } else if (whichTab == "general") {

                if (!isFrontTab) {

                    foreach (GameObject slot in allGeneralSlots) {
                        RectTransform slotRect = slot.GetComponent<RectTransform>();
                        slotRect.localPosition = new Vector3(slotRect.localPosition.x, slotRect.localPosition.y + tabYOffset, slotRect.localPosition.z);
                    }
                    foreach (GameObject slot in allQuestSlots) {
                        RectTransform slotRect = slot.GetComponent<RectTransform>();
                        slotRect.localPosition = new Vector3(slotRect.localPosition.x, slotRect.localPosition.y - tabYOffset, slotRect.localPosition.z);
                    }

                    isFrontTab = true;
                    isBackTab = false;

                    generalButton.GetComponent<Image>().sprite = generalButton.GetComponent<ButtonGeneral>().imagePressed;
                    questButton.GetComponent<Image>().sprite = questButton.GetComponent<ButtonQuest>().imageNotPressed;
                }
            }
        }

        public bool FindAnyWeapon() {

            for (int i = 0; i < allGeneralSlots.Count; i++) {
                Slot tmpSlot = allGeneralSlots[i].GetComponent<Slot>();

                if (!tmpSlot.IsEmpty) {
                    if (tmpSlot.CurrentItem.Item.Type == ItemType.WEAPON)
                        return true;
                }
            }

            if (allQuestSlots != null) {
                for (int i = 0; i < allQuestSlots.Count; i++) {
                    Slot tmpSlot = allQuestSlots[i].GetComponent<Slot>();

                    if (!tmpSlot.IsEmpty) {
                        if (tmpSlot.CurrentItem.Item.Type == ItemType.WEAPON)
                            return tmpSlot;
                    }
                }
            }

            return false;
        }

        public virtual Slot FindItem(ItemScript item) {

            for (int i = 0; i < allGeneralSlots.Count; i++) {
                Slot tmpSlot = allGeneralSlots[i].GetComponent<Slot>();

                if (!tmpSlot.IsEmpty) {
                    if (tmpSlot.CurrentItem.Item.Name == item.Item.Name)
                        return tmpSlot;
                }
            }
            for (int i = 0; i < allQuestSlots.Count; i++) {
                Slot tmpSlot = allQuestSlots[i].GetComponent<Slot>();

                if (!tmpSlot.IsEmpty) {
                    if (tmpSlot.CurrentItem.Item.Name == item.Item.Name)
                        return tmpSlot;
                }
            }

            return null;
        }

        public bool RemoveItemFromSlot(ItemScript item) {
            Slot targetSlot;

            if (FindItem(item) != null) {
                targetSlot = FindItem(item);
                targetSlot.ClearSlot();
                return true;
            } else
                return false;
        }

        public bool EquipItemFromSlot(ItemScript item) {
            Slot targetSlot;

            if (FindItem(item) != null) {
                targetSlot = FindItem(item);
                targetSlot.UseItem();
                return true;
            } else
                return false;
        }

        public void SaveContentForTrade() {
            string content = string.Empty;
            for (int i = 0; i < allGeneralSlots.Count; i++) {
                Slot tmp = allGeneralSlots[i].GetComponent<Slot>();

                if (!tmp.IsEmpty)
                    content += i + "-" + tmp.CurrentItem.Item.Type.ToString() + "-" + tmp.CurrentItem.Item.Name + "-" + tmp.Items.Count.ToString() + "-" + tmp.isEquiped.ToString() + ";";

                PlayerPrefs.SetString("heroTrade", content);
                PlayerPrefs.Save();
            }
        }

        public virtual void SaveContent(string contentName) {
            List<GameObject> tabContent = new List<GameObject>();

            if (contentName == "general")
                tabContent = allGeneralSlots;
            else if (contentName == "quest")
                tabContent = allQuestSlots;

            string content = string.Empty;
            for (int i = 0; i < tabContent.Count; i++) {
                Slot tmp = tabContent[i].GetComponent<Slot>();

                if (!tmp.IsEmpty)
                    content += i + "-" + tmp.CurrentItem.Item.Type.ToString() + "-" + tmp.CurrentItem.Item.Name + "-" + tmp.Items.Count.ToString() + ";";

                PlayerPrefs.SetString(contentName, content);
                PlayerPrefs.SetString("goldQuantity", gold.ToString());
                PlayerPrefs.Save();
            }
        }

        public virtual void LoadContent(string contentName) {

            List<GameObject> tabContent = new List<GameObject>();

            if (contentName == "general")
                tabContent = allGeneralSlots;
            else if (contentName == "quest")
                tabContent = allQuestSlots;

            string content = PlayerPrefs.GetString(contentName);
            string[] splitContent = content.Split(';');

            for (int x = 0; x < splitContent.Length - 1; x++) {
                string[] splitValues = splitContent[x].Split('-');
                int index = System.Int32.Parse(splitValues[0]);
                ItemType type = (ItemType)System.Enum.Parse(typeof(ItemType), splitValues[1]);
                string itemName = splitValues[2];
                int amount = System.Int32.Parse(splitValues[3]);

                BasicItem tmpItem = null;

                for (int i = 0; i < amount; i++) {

                    GameObject loadedItem = Instantiate(InventoryManager.Instance.itemObject);

                    switch (type) {
                        case ItemType.WEAPON:
                            tmpItem = InventoryManager.Instance.ItemContainer.WeaponsList.Find(basicItem => basicItem.Name == itemName);
                            break;
                        case ItemType.ARMOR:
                            tmpItem = InventoryManager.Instance.ItemContainer.ArmorsList.Find(basicItem => basicItem.Name == itemName);
                            break;
                        case ItemType.BOOTS:
                            tmpItem = InventoryManager.Instance.ItemContainer.BootsList.Find(basicItem => basicItem.Name == itemName);
                            break;
                        case ItemType.CONSUMABLE:
                            tmpItem = InventoryManager.Instance.ItemContainer.ConsumablesList.Find(basicItem => basicItem.Name == itemName);
                            break;
                        case ItemType.AMMO:
                            tmpItem = InventoryManager.Instance.ItemContainer.AmmosList.Find(basicItem => basicItem.Name == itemName);
                            break;
                        case ItemType.SNARE:
                            tmpItem = InventoryManager.Instance.ItemContainer.SnaresList.Find(basicItem => basicItem.Name == itemName);
                            break;
                    }

                    loadedItem.AddComponent<ItemScript>();
                    loadedItem.GetComponent<ItemScript>().Item = tmpItem;
                    tabContent[index].GetComponent<Slot>().AddItem(loadedItem.GetComponent<ItemScript>());
                    Destroy(loadedItem);
                }

            }

            string goldQuantity = PlayerPrefs.GetString("goldQuantity");
            gold = System.Int32.Parse(goldQuantity);
            InventoryManager.Instance.goldTxt.text = gold.ToString();
        }
        #endregion

    }
