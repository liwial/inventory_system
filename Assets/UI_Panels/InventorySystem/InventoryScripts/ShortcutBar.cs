﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShortcutBar : Inventory {

	#region ShortcutBar Instance
	private static ShortcutBar shortcutBarInstance;
	public static ShortcutBar ShortcutBarInstance {
		get { return shortcutBarInstance; }
	}
	#endregion

	#region Protected Methods
	protected override void Awake () {
		if (shortcutBarInstance != null && shortcutBarInstance != this) {
			Destroy(this);
		} else {
			shortcutBarInstance = (ShortcutBar)this;
		}

		Start ();
	}

	protected override void Start() {
		int childCount = transform.childCount;
		allGeneralSlots = new List<GameObject> (childCount);

		for (int i = 0; i < childCount; i++)
			allGeneralSlots.Add (transform.GetChild (i).gameObject);

		base.Start ();

        canvasGroup.interactable = true;
        canvasGroup.blocksRaycasts = true;
    }

	protected override void CreateLayout (string layer = "front"){

	} 
	#endregion

	#region Public Methods
	public override void SaveContent (string contentName){
		List<GameObject> tabContent = new List<GameObject>();

		tabContent = allGeneralSlots;

		string content = string.Empty;
		for (int i = 0; i < tabContent.Count; i++) {
			SlotShortcutBar tmp = tabContent [i].GetComponent<SlotShortcutBar> ();

			if (!tmp.IsEmpty)
				content += i + "-" + tmp.CurrentItem.Item.Type.ToString() + "-" + tmp.CurrentItem.Item.Name + "-" + tmp.Items.Count.ToString() + ";";
			else if (!tmp.IsEmptySkill)
				content += i + "-" + tmp.CurrentSkill.Skill.Type.ToString() + "-" + tmp.CurrentSkill.Skill.Name + "-" + tmp.Skills.Count.ToString() + ";";

			PlayerPrefs.SetString (contentName, content);
			PlayerPrefs.Save ();
		}
	}

	public override void LoadContent (string contentName) {
		List<GameObject> tabContent = new List<GameObject>();

		tabContent = allGeneralSlots;

		string content = PlayerPrefs.GetString (contentName);
		string[] splitContent = content.Split (';');

		for (int x = 0; x < splitContent.Length - 1; x++) {
			string[] splitValues = splitContent [x].Split ('-');
			int index = System.Int32.Parse (splitValues [0]);
			ItemType type = (ItemType)System.Enum.Parse (typeof(ItemType), splitValues [1]);
			string itemName = splitValues [2];
			int amount = System.Int32.Parse (splitValues [3]);

			BasicItem tmpItem = null;
			BasicSkill tmpSkill = null;

			for (int i = 0; i < amount; i++) {

				GameObject loadedItem = Instantiate (InventoryManager.Instance.itemObject);

				switch (type) {
				case ItemType.WEAPON:
					tmpItem = InventoryManager.Instance.ItemContainer.WeaponsList.Find (basicItem => basicItem.Name == itemName);
					break;
				case ItemType.ARMOR:
					tmpItem = InventoryManager.Instance.ItemContainer.ArmorsList.Find (basicItem => basicItem.Name == itemName);
					break;
				case ItemType.BOOTS:
					tmpItem = InventoryManager.Instance.ItemContainer.BootsList.Find (basicItem => basicItem.Name == itemName);
					break;
				case ItemType.CONSUMABLE:
					tmpItem = InventoryManager.Instance.ItemContainer.ConsumablesList.Find (basicItem => basicItem.Name == itemName);
					break;
				case ItemType.AMMO:
					tmpItem = InventoryManager.Instance.ItemContainer.AmmosList.Find (basicItem => basicItem.Name == itemName);
					break;
				case ItemType.SNARE:
					tmpItem = InventoryManager.Instance.ItemContainer.SnaresList.Find (basicItem => basicItem.Name == itemName);
					break;
				case ItemType.SKILL:
					tmpSkill = InventoryManager.Instance.SkillContainer.GetSkill (itemName);
					break;
				}

				if (tmpItem != null) {
					loadedItem.AddComponent<ItemScript> ();
					loadedItem.GetComponent<ItemScript> ().Item = tmpItem;
					tabContent [index].GetComponent<SlotShortcutBar> ().AddItem (loadedItem.GetComponent<ItemScript> ());
					Destroy (loadedItem);
					tmpItem = null;
				} else if (tmpSkill != null) {
					loadedItem.AddComponent<SkillScript> ();
					loadedItem.GetComponent<SkillScript> ().Skill = tmpSkill;
					Stack <SkillScript> tmpStack = new Stack<SkillScript> ();
					tmpStack.Push (loadedItem.GetComponent<SkillScript> ());
					tabContent [index].GetComponent<SlotShortcutBar> ().AddSkill (tmpStack);
					Destroy (loadedItem);
					tmpSkill = null;
				}
			}
		}
				
	}

	public override void MoveItem(GameObject clicked) {

		InventoryManager.Instance.Clicked = clicked;

		if (InventoryManager.Instance.FromExists ()) {

			if (InventoryManager.Instance.FromSkillWidow ()) {
				MoveSkillSlot (clicked);
			} else if (InventoryManager.Instance.FromShortcutWindow ()) {

				SlotShortcutBar fromSlot = InventoryManager.Instance.From.GetComponent<SlotShortcutBar> ();

				if (!fromSlot.IsEmptySkill) {
					MoveSkillSlot (clicked);
				} else if (!fromSlot.IsEmpty) {

					if (!clicked.GetComponent<SlotShortcutBar> ().IsEmptySkill)
						MoveItemOnSkillSlot (clicked);
					else
						MoveItemSlot (clicked);
				}
			} else {
				if (!clicked.GetComponent<SlotShortcutBar> ().IsEmptySkill)
					MoveItemOnSkillSlot (clicked);
				else
					MoveItemSlot (clicked);
			}

		} else if (!clicked.GetComponent<SlotShortcutBar> ().IsEmptySkill) {
			MoveSkillSlot (clicked);
		} else {
			MoveItemSlot (clicked);
		}

	}

	public override void ShowToolTip (GameObject slot) {
		SlotShortcutBar tmpSlot = slot.GetComponent<SlotShortcutBar> ();

		if (!tmpSlot.IsEmpty && InventoryManager.Instance.HoverObject == null) {

			InventoryManager.Instance.VisualTextObject.text = tmpSlot.CurrentItem.GetTooltip ();
			InventoryManager.Instance.SizeTextObject.text = InventoryManager.Instance.VisualTextObject.text;

			InventoryManager.Instance.TooltipObject.SetActive (true);

			float xPos = tmpSlot.transform.position.x + 0.05f;
			float yPos = tmpSlot.transform.position.y + 2.0f; 

			InventoryManager.Instance.TooltipObject.transform.position = new Vector3 (xPos, yPos, tmpSlot.transform.position.z);

		} else if (!tmpSlot.IsEmptySkill && InventoryManager.Instance.HoverObject == null) {

			InventoryManager.Instance.VisualTextObject.text = tmpSlot.Skills.Peek ().Skill.GetTooltip ();
			InventoryManager.Instance.SizeTextObject.text = InventoryManager.Instance.VisualTextObject.text;

			InventoryManager.Instance.TooltipObject.SetActive (true);

			float xPos = tmpSlot.transform.position.x + 0.05f;
			float yPos = tmpSlot.transform.position.y + 2.0f; 

			InventoryManager.Instance.TooltipObject.transform.position = new Vector3 (xPos, yPos, tmpSlot.transform.position.z);
		}
	}

	public bool AddItemByForce (ItemScript item) {

		if (item.Item.MaxSize == 1) {
			foreach (GameObject sb in allGeneralSlots) {
				if (sb.GetComponent<SlotShortcutBar> ().IsEmpty && sb.GetComponent<SlotShortcutBar> ().IsEmptySkill) {
					sb.GetComponent<SlotShortcutBar> ().AddItem (item);
					return true;
				}
			}
		}

		return false;
	}

	public override Slot FindItem (ItemScript item) {
		for (int i = 0; i < allGeneralSlots.Count; i++) {
			SlotShortcutBar tmpSlot = allGeneralSlots [i].GetComponent<SlotShortcutBar> ();

			if (!tmpSlot.IsEmpty && tmpSlot.IsEmptySkill) {
				if (tmpSlot.CurrentItem.Item.Name == item.Item.Name)
					return tmpSlot;
			}
		}

		return null;
	}
	#endregion

	#region Private Methods
	private void MoveItemOnSkillSlot(GameObject clicked) {

		SlotShortcutBar clickedSlot = clicked.GetComponent<SlotShortcutBar> ();

		if (clickedSlot != null)
			clicked.GetComponent<SlotShortcutBar> ().ClearSlot ();

		clicked.GetComponent<Slot>().AddItems (InventoryManager.Instance.From.GetComponent<Slot>().Items);

		InventoryManager.Instance.From.GetComponent<Image> ().color = Color.white;
		InventoryManager.Instance.From.GetComponent<Slot> ().ClearSlot ();

		InventoryManager.Instance.To = null;
		InventoryManager.Instance.From = null;
		Destroy (GameObject.Find ("Hover"));
	}

	private void MoveItemSlot (GameObject clicked) {

		if (!InventoryManager.Instance.MovingSlot.IsEmpty) {

			Slot tmp = clicked.GetComponent<SlotShortcutBar> ();

			if (tmp.IsEmpty) {
				tmp.AddItems (InventoryManager.Instance.MovingSlot.Items);

				InventoryManager.Instance.MovingSlot.Items.Clear ();
				Destroy (GameObject.Find ("Hover"));
			} else if (!tmp.IsEmpty && InventoryManager.Instance.MovingSlot.CurrentItem.Item.Type == tmp.CurrentItem.Item.Type && tmp.IsAvailable) {
				MergeStacks (InventoryManager.Instance.MovingSlot, tmp);
			}

		} else if (InventoryManager.Instance.From == null && !Input.GetKey(KeyCode.LeftShift)) {

			if (!clicked.GetComponent<SlotShortcutBar> ().IsEmpty && !GameObject.Find("Hover")) {
				InventoryManager.Instance.From = clicked.GetComponent<SlotShortcutBar> ();
				InventoryManager.Instance.From.GetComponent<Image> ().color = Color.gray;

				CreateHoverIcon ();
			}

			//idiot-proof tabs
			InventoryManager.Instance.CheckTab ();

		} else if (InventoryManager.Instance.To == null && !Input.GetKey(KeyCode.LeftShift)) {

			if (InventoryManager.Instance.From.CurrentItem.Item.Type != ItemType.ARMOR && InventoryManager.Instance.From.CurrentItem.Item.Type != ItemType.BOOTS) {

				InventoryManager.Instance.To = InventoryManager.Instance.Clicked.GetComponent<SlotShortcutBar> ();
				Destroy (GameObject.Find ("Hover"));

			} else
				Debug.Log ("You cannot move armor or boots to shortcut bar");
		}
			
		if (InventoryManager.Instance.To != null && InventoryManager.Instance.From != null) {

			if (!InventoryManager.Instance.To.IsEmpty && InventoryManager.Instance.From.CurrentItem.Item.Type == InventoryManager.Instance.To.CurrentItem.Item.Type && InventoryManager.Instance.To.IsAvailable) {
				if (InventoryManager.Instance.From.CurrentItem.Item.Name != InventoryManager.Instance.To.CurrentItem.Item.Name)
					Slot.SwapItems (InventoryManager.Instance.From, InventoryManager.Instance.To);
				else
					MergeStacks (InventoryManager.Instance.From, InventoryManager.Instance.To);
			} else {
				Slot.SwapItems (InventoryManager.Instance.From, InventoryManager.Instance.To);
			}

			InventoryManager.Instance.From.GetComponent<Image> ().color = Color.white;
			InventoryManager.Instance.To = null;
			InventoryManager.Instance.From = null;
			Destroy(GameObject.Find("Hover"));
		}
	}

	private void SkillSlotGrayOrNot () {
		if (Equipment.equippedWeapon.Count != 0) {

			if (Equipment.EquippedWeaponType () == InventoryManager.Instance.From.GetComponent<SlotSkill> ().Skills.Peek ().Skill.WeaponType) {
				InventoryManager.Instance.From.GetComponent<Image> ().color = Color.white;
			} else {
				InventoryManager.Instance.From.GetComponent<Image> ().color = InventoryManager.Instance.lightGray;
			}
		}
		else
			InventoryManager.Instance.From.GetComponent<Image> ().color = InventoryManager.Instance.lightGray;
	}

	private void MoveSkillSlot (GameObject clicked) {

		SlotShortcutBar clickedSlot = clicked.GetComponent<SlotShortcutBar> ();

		if (clickedSlot.IsEmptySkill && clickedSlot.IsEmpty) {

			if (InventoryManager.Instance.FromShortcutWindow ()) {
				clickedSlot.AddSkill (InventoryManager.Instance.From.GetComponent<SlotShortcutBar> ().Skills);

				InventoryManager.Instance.From.ClearSlot ();
				InventoryManager.Instance.From.GetComponent<Image> ().color = Color.white;
			} else {
				clickedSlot.AddSkill (InventoryManager.Instance.From.GetComponent<SlotSkill> ().Skills);
				SkillSlotGrayOrNot ();
			}

			InventoryManager.Instance.From = null;
			Destroy (GameObject.Find ("Hover"));
		} else if (clickedSlot.IsEmptySkill && !clickedSlot.IsEmpty) {

			GameObject g = Inventory.Instance.FindFirstFreeSlot ();
			g.GetComponent<Slot> ().AddItems (clickedSlot.Items);

			clickedSlot.ClearSlot ();

			if (InventoryManager.Instance.FromSkillWidow ()) {
				clickedSlot.AddSkill (InventoryManager.Instance.From.GetComponent<SlotSkill> ().Skills);
			} else {
				clickedSlot.AddSkill (InventoryManager.Instance.From.GetComponent<SlotShortcutBar> ().Skills);
				InventoryManager.Instance.From.ClearSlot ();
			}

			InventoryManager.Instance.From.GetComponent<Image> ().color = Color.white;
			InventoryManager.Instance.From = null;
			Destroy (GameObject.Find ("Hover"));
		} else if (!clickedSlot.IsEmptySkill && clickedSlot.IsEmpty) {

			GameObject hover = GameObject.Find ("Hover");

			if (hover == null) {
				InventoryManager.Instance.From = clicked.GetComponent<SlotShortcutBar> ();
				InventoryManager.Instance.From.GetComponent<Image> ().color = Color.gray;
				CreateHoverIcon ();
			} else {
				if (InventoryManager.Instance.FromShortcutWindow ()) {
					clickedSlot.AddSkill (InventoryManager.Instance.From.GetComponent<SlotShortcutBar> ().Skills);
					InventoryManager.Instance.From.ClearSlot ();
				} else {
					clickedSlot.AddSkill (InventoryManager.Instance.From.GetComponent<SlotSkill> ().Skills);
				}

				InventoryManager.Instance.From.GetComponent<Image> ().color = Color.white;
				InventoryManager.Instance.From = null;
				Destroy (GameObject.Find ("Hover"));
			}
		}
	}
		
	void Update () {
		if (Input.GetKeyDown (KeyCode.Alpha1))
			allGeneralSlots [0].GetComponent<Slot> ().UseItem ();
		if (Input.GetKeyDown (KeyCode.Alpha2))
			allGeneralSlots [1].GetComponent<Slot> ().UseItem ();
		if (Input.GetKeyDown (KeyCode.Alpha3))
			allGeneralSlots [2].GetComponent<Slot> ().UseItem ();
		if (Input.GetKeyDown (KeyCode.Alpha4))
			allGeneralSlots [3].GetComponent<Slot> ().UseItem ();
		if (Input.GetKeyDown (KeyCode.Alpha5))
			allGeneralSlots [4].GetComponent<Slot> ().UseItem ();
		if (Input.GetKeyDown (KeyCode.Alpha6))
			allGeneralSlots [5].GetComponent<Slot> ().UseItem ();
		if (Input.GetKeyDown (KeyCode.Alpha7))
			allGeneralSlots [6].GetComponent<Slot> ().UseItem ();
		if (Input.GetKeyDown (KeyCode.Alpha8))
			allGeneralSlots [7].GetComponent<Slot> ().UseItem ();
		if (Input.GetKeyDown (KeyCode.Alpha9))
			allGeneralSlots [8].GetComponent<Slot> ().UseItem ();
		if (Input.GetKeyDown (KeyCode.Alpha0))
			allGeneralSlots [9].GetComponent<Slot> ().UseItem ();


		if (Equipment.WeaponChangedSB) {

			foreach (GameObject i in allGeneralSlots) {

				if (!i.GetComponent<SlotShortcutBar> ().IsEmptySkill) {
					if (i.GetComponent<SlotShortcutBar> ().Skills.Peek ().Skill.WeaponType == Equipment.EquippedWeaponType ())
						i.GetComponent<SlotShortcutBar> ().GetComponent<Image> ().color = Color.white;
					else
						i.GetComponent<SlotShortcutBar> ().GetComponent<Image> ().color = InventoryManager.Instance.lightGray;
				}

			}

			if (Equipment.WeaponChangedSB)
				Equipment.WeaponChangedSB = false;

		} else if (Equipment.equippedWeapon.Count != 0) {

			foreach (GameObject i in allGeneralSlots) {

				if (!i.GetComponent<SlotShortcutBar> ().IsEmptySkill) {

					if (Equipment.EquippedWeaponName() != string.Empty) {
						if (i.GetComponent<SlotShortcutBar> ().Skills.Peek ().Skill.WeaponType == Equipment.EquippedWeaponType())
							i.GetComponent<SlotShortcutBar> ().GetComponent<Image> ().color = Color.white;
						else
							i.GetComponent<SlotShortcutBar> ().GetComponent<Image> ().color = InventoryManager.Instance.lightGray;
					}
				}

			}
		} else if (Equipment.equippedWeapon.Count == 0) {
			
			foreach (GameObject i in allGeneralSlots) {
				if (!i.GetComponent<SlotShortcutBar> ().IsEmptySkill) {
					i.GetComponent<SlotShortcutBar> ().GetComponent<Image> ().color = InventoryManager.Instance.lightGray;
				}
			}

		}
	}
	#endregion

}
