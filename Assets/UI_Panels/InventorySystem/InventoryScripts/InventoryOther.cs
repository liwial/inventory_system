﻿using System.Collections;
using UnityEngine;

public interface IInventory<T> {

	#region Basic Methods only for Inventory
	// adding item to Inventory
	bool AddItem (T item);

	// removing item from Inventory
	bool RemoveItem (T item);

	// checking if item is in Inventory
	bool IsItemInInventory (T item, int count = 1);

	// equipping item from Inventory 
	bool EquipItem (T item);
	#endregion

	#region Basic Methods for Inventory or/and Shortcut Bar
	// adding item to Shortcut Bar
	bool AddItemSB (T item);

	// removing item from Inventory or Shortcut Bar
	bool RemoveItemSB (T item);

	// checking if item is in Inventory or in Shortcut Bar
	bool IsItemInInventorySB (T item, int count = 1);

	// equipping item from Shortcut Bar
	bool EquipItemSB (T item);
	#endregion

	#region Others Methods
	// checking if item is equipped
	bool IsItemEquiped (T item);

	// blocking unequipping item
	bool BlockUnequipItem (T item);

	// unblocking unequipping item
	bool UnblockUnequipItem (T item);

	// checking if player got any weapon equipped
	bool IsWeaponless ();

	// checking if player got any weapon in inventory and in shortcut bar
	bool IsWeaponlessCompletely ();

	// checking if player got anything equipped (weapon, armor, boots, ammo)
	bool IsNaked ();
	#endregion
}


public class InventoryOther : MonoBehaviour, IInventory<ItemScript> {

	#region InventoryOther Instance 
	private static InventoryOther instance;
	public static InventoryOther Instance {
		get { 
			if (instance == null) {
				instance = GameObject.FindObjectOfType<InventoryOther> ();
			}
			return InventoryOther.instance; 
		}
	}
	#endregion

	#region Private Ancillary Methods
	// setting that item can or cannot be unequipped
	private bool ChangeEquipState (ItemScript item, bool equipState) {
		if (item.Item.Type == ItemType.WEAPON) {
			if (Equipment.equippedWeapon.Count != 0) {
				foreach (ItemScript i in Equipment.equippedWeapon.Keys) {
					if (i.Item.Name == item.Item.Name) {
						Slot slotBlock = Equipment.equippedWeapon [i];
						slotBlock.cannotUnequip = equipState;
						Debug.Log (slotBlock.cannotUnequip);
						return true;
					}
				}
				return false;
			} else
				return false;
		}
		else if (item.Item.Type == ItemType.ARMOR) {
			if (Equipment.equippedArmor.Count != 0) {
				foreach (ItemScript i in Equipment.equippedArmor.Keys) {
					if (i.Item.Name == item.Item.Name) {
						Slot slotBlock = Equipment.equippedArmor [i];
						slotBlock.cannotUnequip = equipState;
						return true;
					}
				}
				return false;
			} else
				return false;
		}
		else if (item.Item.Type == ItemType.BOOTS) {
			if (Equipment.equippedBoots.Count != 0) {
				foreach (ItemScript i in Equipment.equippedBoots.Keys) {
					if (i.Item.Name == item.Item.Name) {
						Slot slotBlock = Equipment.equippedBoots [i];
						slotBlock.cannotUnequip = equipState;
						return true;
					}
				}
				return false;
			} else
				return false;
		}
		else if (item.Item.Type == ItemType.AMMO) {
			if (Equipment.equippedAmmo.Count != 0) {
				foreach (ItemScript i in Equipment.equippedAmmo.Keys) {
					if (i.Item.Name == item.Item.Name) {
						Slot slotBlock = Equipment.equippedAmmo [i];
						slotBlock.cannotUnequip = equipState;
						return true;
					}
				}
				return false;
			} else
				return false;
		}	
		else
			return false;
	}

	// finding Itemscript with given name
	private ItemScript FindItemByName (string name) {
		return InventoryManager.Instance.FindItemByName (name);
	}
	#endregion

	#region Basic Methods for Inventory plus methods byName
	public bool AddItem (ItemScript item) {
		return Inventory.Instance.AddItem (item);
	}

	public bool AddItemByName (string name) {
		
		ItemScript itemScript = FindItemByName (name);

		if (itemScript != null) {
			return AddItem (itemScript);
		} else
			return false;

	}


	public bool RemoveItem (ItemScript item) {
		return Inventory.Instance.RemoveItemFromSlot (item);
	}

	public bool RemoveItemByName (string name) {
		
		ItemScript itemScript = FindItemByName (name);

		if (itemScript != null) {
			return RemoveItem (itemScript);
		} else
			return false;

	}


	public bool IsItemInInventory (ItemScript item, int count = 1) {
		if (Inventory.Instance.FindItem(item) != null) {
			if (Inventory.Instance.FindItem (item).Items.Count == count)
				return true;
			else
				return false;
		}
		else
			return false;
	}

	public bool IsItemInInventoryByName (string name, int count = 1) {
		ItemScript itemScript = FindItemByName (name);

		if (itemScript != null) {
			return IsItemInInventory (itemScript, count);
		} else
			return false;
	}


	public bool EquipItem (ItemScript item) {
		return Inventory.Instance.EquipItemFromSlot(item);
	}

	public bool EquipItemByName (string name) {
		ItemScript itemScript = FindItemByName (name);

		if (itemScript != null) {
			return EquipItem (itemScript);
		} else
			return false;
	}
	#endregion

	#region Basic Methods for Inventory or/and Shortcut Bar plus methods byName
	public bool AddItemSB (ItemScript item) {
		return ShortcutBar.ShortcutBarInstance.AddItemByForce (item);
	}

	public bool AddItemByNameSB (string name) {

		ItemScript itemScript = FindItemByName (name);

		if (itemScript != null) {
			return AddItemSB (itemScript);
		} else
			return false;

	}


	public bool RemoveItemSB (ItemScript item) {
		if (RemoveItem (item)) // item was in Inventory, no need to check Shortcut Bar
			return true;
		else
			return ShortcutBar.ShortcutBarInstance.RemoveItemFromSlot (item);
	}

	public bool RemoveItemByNameSB (string name) {

		ItemScript itemScript = FindItemByName (name);

		if (itemScript != null) {
			return RemoveItemSB (itemScript);
		} else
			return false;

	}


	public bool IsItemInInventorySB (ItemScript item, int count = 1) {
		if (IsItemInInventory (item, count)) { // item found in Inventory, no need to look further
			return true;
		} else {
			if (ShortcutBar.ShortcutBarInstance.FindItem (item) != null) {
				if (ShortcutBar.ShortcutBarInstance.FindItem (item).Items.Count == count)
					return true;
				else
					return false;
			} else
				return false;
		}
	}

	public bool IsItemInInventoryByNameSB (string name, int count = 1) {
		ItemScript itemScript = FindItemByName (name);

		if (itemScript != null) {
			return IsItemInInventorySB (itemScript, count);
		} else
			return false;
	}


	public bool EquipItemSB (ItemScript item) {
		return ShortcutBar.ShortcutBarInstance.EquipItemFromSlot(item);
	}

	public bool EquipItemByNameSB (string name) {
		ItemScript itemScript = FindItemByName (name);

		if (itemScript != null) {
			return EquipItemSB (itemScript);
		} else
			return false;
	}
	#endregion

	#region Other Methods
	public bool IsItemEquiped (ItemScript item) {

		if (item.Item.Type == ItemType.WEAPON) {
			if (Equipment.equippedWeapon.Count != 0) {
				foreach (ItemScript i in Equipment.equippedWeapon.Keys) {
					if (i.Item.Name == item.Item.Name)
						return true;
				}
				return false;
			} else
				return false;
		}
		else if (item.Item.Type == ItemType.ARMOR) {
			if (Equipment.equippedArmor.Count != 0) {
				foreach (ItemScript i in Equipment.equippedArmor.Keys) {
					if (i.Item.Name == item.Item.Name)
						return true;
				}
				return false;
			} else
				return false;
		}
		else if (item.Item.Type == ItemType.BOOTS) {
			if (Equipment.equippedBoots.Count != 0) {
				foreach (ItemScript i in Equipment.equippedBoots.Keys) {
					if (i.Item.Name == item.Item.Name)
						return true;
				}
				return false;
			} else
				return false;
		}
		else if (item.Item.Type == ItemType.AMMO) {
			if (Equipment.equippedAmmo.Count != 0) {
				foreach (ItemScript i in Equipment.equippedAmmo.Keys) {
					if (i.Item.Name == item.Item.Name)
						return true;
				}
				return false;
			} else
				return false;
		}	
		else
			return false;
	}

	public bool IsItemEquipedByName (string name) {
		ItemScript itemScript = FindItemByName (name);

		if (itemScript != null) {
			return IsItemEquiped (itemScript);
		} else
			return false;
	}


	public bool BlockUnequipItem (ItemScript item) {
		return ChangeEquipState (item, true);
	}

	public bool BlockUnequipItemByName (string name) {
		ItemScript itemScript = FindItemByName (name);

		if (itemScript != null) {
			return ChangeEquipState (itemScript, true);
		} else
			return false;
	}


	public bool UnblockUnequipItem (ItemScript item) {
		return ChangeEquipState (item, false);
	}

	public bool UnblockUnequipItemByName (string name) {
		ItemScript itemScript = FindItemByName (name);

		Debug.Log (itemScript.Item.Name);

		if (itemScript != null) {
			return ChangeEquipState (itemScript, false);
		} else
			return false;
	}


	public bool IsWeaponless () {
		if (Equipment.equippedWeapon.Count == 0)
			return true;
		else
			return false;
	}
		
	public bool IsWeaponlessCompletely() {
		if (!Inventory.Instance.FindAnyWeapon () && !ShortcutBar.ShortcutBarInstance.FindAnyWeapon())
			return true;
		else
			return false;
	}

	public bool IsNaked () {
		if (Equipment.equippedWeapon.Count == 0 && Equipment.equippedArmor.Count == 0) {
			if (Equipment.equippedBoots.Count == 0 && Equipment.equippedAmmo.Count == 0)
				return true;
			else
				return false;
		}
		else
			return false;
	}
	#endregion

}
