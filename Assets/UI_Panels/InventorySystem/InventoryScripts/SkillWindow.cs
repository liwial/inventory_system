﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SkillWindow : Inventory {

	private List<GameObject> skillContentSlots = new List<GameObject>();
	private float skillSlotSize;
	private int activeCount;

	private static SkillWindow skillInstance;
	public static SkillWindow SkillInstance {
		get { return skillInstance; }
	}

	#region Protected Methods
	protected override void Awake () {
		fadeTime = 1.0f;

		activeCount = 0;

		if (skillInstance != null && skillInstance != this) {
			Destroy(this);
		} else {
			skillInstance = (SkillWindow)this;
		}
	}

	protected override void Start () {
		
		Transform skillContent = transform.GetChild (0);
		int childCount = skillContent.childCount;

		//preparing list of skill slots 
		for (int i = 0; i < childCount; i++) {
			skillContentSlots.Add (skillContent.transform.GetChild (i).gameObject);
		}

		skillSlotSize = slotSize;

		AssignSkills ();

		base.Start();

		IsOpen = false;
	}

	protected override void CreateLayout (string layer = "front"){

	}
	#endregion

	#region Public Methods
	public override void Open() {
		base.Open ();

		if (IsOpen)
			InventoryManager.Instance.SkillWindowObject.GetComponent<RectTransform> ().anchoredPosition = InventoryManager.Instance.SkillWindowPositionVisible; 
		else if (!IsOpen)
			InventoryManager.Instance.SkillWindowObject.GetComponent<RectTransform> ().anchoredPosition = InventoryManager.Instance.SkillWindowPositionInvisible;   

	}

	public override void MoveItem(GameObject clicked) {

		InventoryManager.Instance.Clicked = clicked;

		if (InventoryManager.Instance.MovingSlot.IsEmpty) {

			if (InventoryManager.Instance.From == null && !Input.GetKey (KeyCode.LeftShift)) {

				InventoryManager.Instance.From = InventoryManager.Instance.Clicked.GetComponent<SlotSkill> ();
				InventoryManager.Instance.From.GetComponent<Image> ().color = Color.gray;

				CreateHoverIcon ();
			} else if (InventoryManager.Instance.To == null && !Input.GetKey (KeyCode.LeftShift)) {

				if (InventoryManager.Instance.From.GetComponent<SlotSkill> ()) {

					if (Equipment.equippedWeapon.Count != 0) {

						if (Equipment.EquippedWeaponType () == InventoryManager.Instance.From.GetComponent<SlotSkill> ().Skills.Peek ().Skill.WeaponType) {
							InventoryManager.Instance.From.GetComponent<Image> ().color = Color.white;
						} else {
							InventoryManager.Instance.From.GetComponent<Image> ().color = InventoryManager.Instance.lightGray;
						}
					}
					else
						InventoryManager.Instance.From.GetComponent<Image> ().color = InventoryManager.Instance.lightGray;

					InventoryManager.Instance.From = null;
					Destroy (GameObject.Find ("Hover"));
				}
			}
		}
	}

	public override void ShowToolTip (GameObject slot) {

		if (IsOpen) {

			SlotSkill tmpSlot = slot.GetComponent<SlotSkill> ();

			InventoryManager.Instance.VisualTextObject.text = tmpSlot.Skills.Peek ().Skill.GetShortTooltip ();
			InventoryManager.Instance.SizeTextObject.text = InventoryManager.Instance.VisualTextObject.text;

			InventoryManager.Instance.TooltipObject.SetActive (true);

			float xPos = tmpSlot.transform.position.x - 2.0f;
			float yPos = tmpSlot.transform.position.y - 0.3f; 

			InventoryManager.Instance.TooltipObject.transform.position = new Vector3 (xPos, yPos, tmpSlot.transform.position.z);

		}
	}
	#endregion

	#region Private Methods
	private void AssignSkills() {
		int skillCount = InventoryManager.Instance.SkillContainer.SkillsList.Count;

		Stack<SkillScript> tmpSkills = new Stack<SkillScript> ();

		//protection - checking if slot count is equal to skill count
		if (skillCount == skillContentSlots.Count) {

			//assigning skills to slots
			for (int i = 0; i < skillCount; i++) {

				GameObject g = new GameObject ();
				g.AddComponent<SkillScript> ();

				g.GetComponent<SkillScript> ().Skill = InventoryManager.Instance.SkillContainer.SkillsList [i];

				tmpSkills.Push (g.GetComponent<SkillScript> ());

				skillContentSlots [i].GetComponent<SlotSkill> ().Skills = new Stack<SkillScript>(tmpSkills); //AddSkillToStack (tmpSkills);
				skillContentSlots [i].GetComponent<SlotSkill> ().AssignSprites(tmpSkills);

				skillContentSlots [i].transform.GetChild (1).gameObject.GetComponent<Text> ().text = skillContentSlots [i].GetComponent<SlotSkill> ().Skills.Peek ().Skill.Name;

				Destroy (g);

				tmpSkills.Clear ();
			}

			foreach (GameObject i in skillContentSlots) {
				if (Equipment.EquippedWeaponName() == string.Empty) {
					i.GetComponent<SlotSkill> ().GetComponent<Image> ().color = InventoryManager.Instance.lightGray;
				}
			}

			foreach (GameObject i in skillContentSlots) {
				if (!i.GetComponent<SlotSkill> ().Skills.Peek ().Skill.IsVisible) {
					i.SetActive (false);
				}
				else
					activeCount++;

				if (Equipment.EquippedWeaponName() != string.Empty) {
					if (i.GetComponent<SlotSkill> ().Skills.Peek ().Skill.WeaponType == Equipment.EquippedWeaponType()) {
						i.GetComponent<SlotSkill> ().GetComponent<Image> ().color = Color.white;
					}
				}
			}

			float skillWindowHeight = (skillSlotSize + 10.0f) * activeCount + 10.0f;
			this.transform.GetChild (0).gameObject.GetComponent<RectTransform> ().sizeDelta = new Vector2 (this.GetComponent<RectTransform>().sizeDelta.x, skillWindowHeight);

		} else
			Debug.Log ("Error with skill slot count");
	}
		
	void Update () {

		if (Equipment.WeaponChangedSkill) {
			
			foreach (GameObject i in skillContentSlots) {

				if (Equipment.EquippedWeaponName() != string.Empty) {
					if (i.GetComponent<SlotSkill> ().Skills.Peek ().Skill.WeaponType == Equipment.EquippedWeaponType())
						i.GetComponent<SlotSkill> ().GetComponent<Image> ().color = Color.white;
					else
						i.GetComponent<SlotSkill> ().GetComponent<Image> ().color = InventoryManager.Instance.lightGray;
				}

			}

			if (Equipment.WeaponChangedSkill)
				Equipment.WeaponChangedSkill = false;
			
		} else if (Equipment.equippedWeapon.Count != 0) {

			foreach (GameObject i in skillContentSlots) {

				if (Equipment.EquippedWeaponName() != string.Empty) {
					if (i.GetComponent<SlotSkill> ().Skills.Peek ().Skill.WeaponType == Equipment.EquippedWeaponType())
						i.GetComponent<SlotSkill> ().GetComponent<Image> ().color = Color.white;
				}

			}
		} else if (Equipment.equippedWeapon.Count == 0) {
			foreach (GameObject i in skillContentSlots)
				i.GetComponent<SlotSkill> ().GetComponent<Image> ().color = InventoryManager.Instance.lightGray;
		}


		if (IsOpen) {

			activeCount = 0;

			foreach (GameObject i in skillContentSlots) {
				if (!i.GetComponent<SlotSkill> ().Skills.Peek ().Skill.IsVisible) {
					i.SetActive (false);
				} else
					activeCount++;
			}

			float skillWindowHeight = (skillSlotSize + 10.0f) * activeCount + 10.0f;
			this.transform.GetChild (0).gameObject.GetComponent<RectTransform> ().sizeDelta = new Vector2 (this.GetComponent<RectTransform>().sizeDelta.x, skillWindowHeight);
		}
	}
	#endregion

}
