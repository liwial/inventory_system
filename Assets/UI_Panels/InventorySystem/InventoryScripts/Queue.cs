﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Queue : Inventory {

	private static Queue queueInstance;
	public static Queue QueueInstance {
		get { return queueInstance; }
	}

	#region Protected Methods
	protected override void Awake () {

		if (queueInstance != null && queueInstance != this) {
			Destroy(this);
		} else {
			queueInstance = (Queue)this;
		}

		Start ();
	}

	protected override void Start() {

		int childCount = transform.childCount;
		allGeneralSlots = new List<GameObject> (childCount);

		for (int i = 0; i < childCount; i++) {
			allGeneralSlots.Add (transform.GetChild (i).gameObject);
			transform.GetChild (i).gameObject.SetActive (false);
		}

		base.Start ();
	}

	protected override void CreateLayout (string layer = "front"){
		
	}
	#endregion

	#region Public Methods
	public override void SaveContent (string contentName){
		List<GameObject> tabContent = new List<GameObject>();

		tabContent = allGeneralSlots;

		string content = string.Empty;
		for (int i = 0; i < tabContent.Count; i++) {
			SlotQueue tmp = tabContent [i].GetComponent<SlotQueue> ();

			if (!tmp.IsEmptySkill)
				content += i + "-" + tmp.CurrentSkill.Skill.Type.ToString() + "-" + tmp.CurrentSkill.Skill.Name + "-" + tmp.Skills.Count.ToString() + ";";

			PlayerPrefs.SetString (contentName, content);
			PlayerPrefs.Save ();
		}
	}

	public override void LoadContent (string contentName){
		List<GameObject> tabContent = new List<GameObject>();

		tabContent = allGeneralSlots;

		string content = PlayerPrefs.GetString (contentName);
		string[] splitContent = content.Split (';');

		for (int x = 0; x < splitContent.Length - 1; x++) {
			string[] splitValues = splitContent [x].Split ('-');
			int index = System.Int32.Parse (splitValues [0]);
			ItemType type = (ItemType)System.Enum.Parse (typeof(ItemType), splitValues [1]);
			string itemName = splitValues [2];
			int amount = System.Int32.Parse (splitValues [3]);

			BasicSkill tmpSkill = null;

			for (int i = 0; i < amount; i++) {

				GameObject loadedItem = Instantiate (InventoryManager.Instance.itemObject);

				switch (type) {
				case ItemType.SKILL:
					tmpSkill = InventoryManager.Instance.SkillContainer.GetSkill (itemName);
					break;
				}
					
				loadedItem.AddComponent<SkillScript> ();
				loadedItem.GetComponent<SkillScript> ().Skill = tmpSkill;
				Stack <SkillScript> tmpStack = new Stack<SkillScript> ();
				tmpStack.Push (loadedItem.GetComponent<SkillScript> ());
				tabContent [index].GetComponent<SlotQueue> ().AddSkill (tmpStack);
				tabContent [index].SetActive (true);
				Destroy (loadedItem);
				tmpSkill = null;
			}
		}
	}
		
	public void AddSkill(Stack<SkillScript> skills) {

		//find first free slot and add skill there
		foreach (GameObject g in allGeneralSlots) {
			if (!g.activeSelf) {
				g.GetComponent<SlotQueue> ().AddSkill (skills);
				g.SetActive (true);
				break;
			}
		}

	}

	public override void ShowToolTip (GameObject slot) {
		SlotQueue tmpSlot = slot.GetComponent<SlotQueue> ();

		if (!tmpSlot.IsEmptySkill && InventoryManager.Instance.HoverObject == null) {
			InventoryManager.Instance.VisualTextObject.text = tmpSlot.Skills.Peek ().Skill.GetTooltip ();
			InventoryManager.Instance.SizeTextObject.text = InventoryManager.Instance.VisualTextObject.text;

			InventoryManager.Instance.TooltipObject.SetActive (true);

			float xPos = tmpSlot.transform.position.x + 0.05f;
			float yPos = tmpSlot.transform.position.y + 0.7f; 

			InventoryManager.Instance.TooltipObject.transform.position = new Vector3 (xPos, yPos, tmpSlot.transform.position.z);
		}
	}
	#endregion

	void Update() {

		if (Equipment.WeaponChangedQueue) {

			if (Equipment.WasAnotherCopy) {
				Equipment.WasAnotherCopy = false;
			}
			else if (!Equipment.WasAnotherCopy) {

				if (allGeneralSlots != null) {
					foreach (GameObject g in allGeneralSlots) {
						if (g.activeSelf) {
							g.GetComponent<SlotQueue> ().ClearSlot ();
							g.SetActive (false);
						} else
							break;
					}
				}
			}
			Equipment.WeaponChangedQueue = false;
		} 
		else if (Equipment.equippedWeapon.Count == 0) {
			
			foreach (GameObject g in allGeneralSlots) {
				if (g.activeSelf) {
					g.GetComponent<SlotQueue> ().ClearSlot ();
					g.SetActive (false);
				} else
					break;
			}

			Equipment.WasAnotherCopy = false;
		}

		if (Input.GetKeyDown (KeyCode.R)) {

			int skillsQuantity = 0;

			foreach (GameObject g in allGeneralSlots) {
				if (g.activeSelf) {
					skillsQuantity++;
				}
				else
					break;
			}

			if (skillsQuantity > 0) {

				InventoryManager.Instance.ShowMessage (allGeneralSlots [0].GetComponent<SlotQueue> ().Skills.Peek().Skill.Name + " was used");

				allGeneralSlots [0].GetComponent<SlotQueue> ().ClearSlot ();

				for (int i = 1; i < skillsQuantity; i++) {
					allGeneralSlots [i - 1].GetComponent<SlotQueue> ().AddSkill (allGeneralSlots [i].GetComponent<SlotQueue> ().Skills);
				}
				
				allGeneralSlots [skillsQuantity - 1].GetComponent<SlotQueue> ().ClearSlot ();
				allGeneralSlots [skillsQuantity - 1].SetActive (false);
			}
		}
	}

}
