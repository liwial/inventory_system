﻿using System.Collections;
using UIPanels.CharacterStats;
using UIPanels.Gameplay;
using UnityEngine;

namespace UIPanels.Bars {

    public class BarsManager : MonoBehaviour {

        public GameObject healthObject;
        public GameObject staminaObject;
        public GameObject foodObject;

        public CanvasGroup canvasGroup;

        private StatsManager mainOtherLabelsManager;

        private bool isVisible = true;


        void Start() {

            mainOtherLabelsManager= transform.parent.Find("CharacterStatsHolder").Find("MainOtherLabelsHolder").GetComponent<StatsManager>();

            StartCoroutine(SetNeededStatValues());

        }


        void Update() {

        }

        private IEnumerator SetNeededStatValues() {

            yield return StartCoroutine(mainOtherLabelsManager.MakeSureStatsAreSetFirst());

            healthObject.GetComponent<BarScriptController>().MaxValue = mainOtherLabelsManager.GetStatValueSecond("HealthStat");
            healthObject.GetComponent<BarScriptController>().Value = mainOtherLabelsManager.GetStatValue("HealthStat");
            healthObject.GetComponent<BarScriptController>().BarName = "Health";

            staminaObject.GetComponent<BarScriptController>().MaxValue = mainOtherLabelsManager.GetStatValueSecond("StaminaStat");
            staminaObject.GetComponent<BarScriptController>().Value = mainOtherLabelsManager.GetStatValue("StaminaStat");
            staminaObject.GetComponent<BarScriptController>().BarName = "Stamina";


            //for (int i = 0; i < 7; i++)
            //    mainOtherLabelsManager.IncreaseStatValueSecond("FoodStat");

            foodObject.GetComponent<BarScriptController>().MaxValue = mainOtherLabelsManager.GetStatValueSecond("FoodStat");
            foodObject.GetComponent<BarScriptController>().Value = mainOtherLabelsManager.GetStatValue("FoodStat");
            foodObject.GetComponent<BarScriptController>().BarName = "Alimentation";

            //areStatsSet = true;
        }

        public void SetVisibility() {

            isVisible = !isVisible;

            UIGameplayManager.SetCanvasGroup(canvasGroup, isVisible);

        }
    }

}