﻿using UnityEngine;

namespace UIPanels.Bars {

    public class BarScriptController : MonoBehaviour {

        public BarScriptView barScriptView;

        public bool isFoodBar;
        private float fillAmount;

        private int valueToShow;

        #region Properties
        public string BarName { get; set; }

        public float MaxValue { get; set; }

        public float Value {
            set { valueToShow = (int) value;
                fillAmount = MapValue(value, 0, MaxValue, 0, 1); }
        }

        //public int ValueToShow {
        //    get { return valueToShow; }
        //    set { valueToShow = value; }
        //}
        #endregion


        void Update() {

            if (!isFoodBar)
                barScriptView.HandleBar(fillAmount);
            else
                barScriptView.HandleFoodBar(fillAmount);

            //TODO method for decreasing value over time for FOOD - here or in stats
            //if in stats, then there is a need to update Value every time it changes

        }


        private float MapValue(float value, float inMin, float inMax, float outMin, float outMax) {

            return ((value - inMin) * (outMax - outMin) / (inMax - inMin) + outMin);

        }


        public void ShowBarTooltip() {

            barScriptView.ShowBarTooltip(BarName, valueToShow, MaxValue);

        }

        public void HideBarTooltip() {

            barScriptView.HideBarTooltip();

        }

    }

}