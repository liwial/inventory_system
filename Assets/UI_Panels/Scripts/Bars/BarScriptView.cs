﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace UIPanels.Bars {

    public class BarScriptView : MonoBehaviour {

        [SerializeField] private Image contentImage;
        //[SerializeField] private TextMeshProUGUI barNameText;
        [SerializeField] private TextMeshProUGUI barText;

        private float lerpSpeed;

        private Color[] colors;


        void Start() {

            lerpSpeed = 2f;

            colors = new Color[] {
            StatParameters.barColor0,
            StatParameters.barColor1,
            StatParameters.barColor2,
            StatParameters.barColor3,
            StatParameters.barColor4,
            StatParameters.barColor5
            };

            //barNameText.alpha = 0;

            barText.alpha = 0;

        }


        private Color GetColor(float fillAmount) {

            if (fillAmount <= 0)
                return colors[0];
            else if (fillAmount > 0 && fillAmount < 0.2f)
                return colors[1];
            else if (fillAmount >= 0.2f && fillAmount < 0.4f)
                return colors[2];
            else if (fillAmount >= 0.4f && fillAmount < 0.6f)
                return colors[3];
            else if (fillAmount >= 0.6f && fillAmount < 0.8f)
                return colors[4];
            else if (fillAmount >= 80)
                return colors[5];

            return Color.black;

        }

        private void SetColor(float fillAmount) {

            contentImage.color = Color.Lerp(colors[0], colors[colors.Length - 1], fillAmount);

        }


        public void ShowBarTooltip(string name, float value, float maxValue) {

            //string msg = barNameText.text.ToString() + " " + value.ToString() + "/" + maxValue.ToString();
            barText.text = name + ": " + value.ToString() + "/" + maxValue.ToString(); //string.Format("<size=24><i><color=white>{0}</color></i></size>", msg);
            barText.alpha = 255f;

            //barNameText.alpha = 255f;

        }

        public void HideBarTooltip() {

            barText.alpha = 0;
            //barNameText.alpha = 0;

        }

        public void HandleFoodBar(float fillAmount) {

            if (fillAmount != contentImage.fillAmount) {
                contentImage.fillAmount = Mathf.Lerp(contentImage.fillAmount, fillAmount, Time.deltaTime * lerpSpeed);
                contentImage.color = GetColor(fillAmount);
            }

        }

        public void HandleBar(float fillAmount) {

            if (fillAmount != contentImage.fillAmount)
                contentImage.fillAmount = Mathf.Lerp(contentImage.fillAmount, fillAmount, Time.deltaTime * lerpSpeed);

            contentImage.color = Color.Lerp(colors[0], colors[colors.Length - 1], fillAmount);
        }

    }

}