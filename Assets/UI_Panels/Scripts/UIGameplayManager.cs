﻿using UnityEngine;
using UIPanels.Bars;
using UIPanels.CharacterStats;
using System.IO;


namespace UIPanels.Gameplay {

    public class UIGameplayManager : MonoBehaviour {

        public GameObject characterStatsHolder;
        public GameObject barsHolder;

        private static string statsSaveFolderPath;
        private static string tradeSaveFolderPath;


        public static string StatsSaveFolderPath {
            get { return statsSaveFolderPath; }
        }

        public static string TradeSaveFolderPath {
            get { return tradeSaveFolderPath; }
        }


        void Awake() {

            PrepareSaveFolders();

        }

        void Update() {

            if (Input.GetKeyDown(KeyCode.C) && (InventoryManager.Instance.GameUIStates == UIStates.DEFAULT || InventoryManager.Instance.GameUIStates == UIStates.CHARACTER_STATS)) {

                characterStatsHolder.GetComponent<CharacterStatsManager>().SetVisibility();
                barsHolder.GetComponent<BarsManager>().SetVisibility();

            }

        }

        private void PrepareSaveFolders() {

            string UISystemFolderPath = Path.Combine(Application.persistentDataPath, "UISystem");

            statsSaveFolderPath = Path.Combine(UISystemFolderPath, "CharacterStats");
            tradeSaveFolderPath = Path.Combine(UISystemFolderPath, "Traders");

            if (!Directory.Exists(statsSaveFolderPath))
                Directory.CreateDirectory(statsSaveFolderPath);

            if (!Directory.Exists(tradeSaveFolderPath))
                Directory.CreateDirectory(tradeSaveFolderPath);

        }

        public static CanvasGroup SetCanvasGroup(CanvasGroup canvasGroup, bool isVisible) {

            if (isVisible) {
                canvasGroup.alpha = 1;
                canvasGroup.interactable = true;
                canvasGroup.blocksRaycasts = true;
            } else {
                canvasGroup.alpha = 0;
                canvasGroup.interactable = false;
                canvasGroup.blocksRaycasts = false;
            }

            return canvasGroup;

        }


    }
}