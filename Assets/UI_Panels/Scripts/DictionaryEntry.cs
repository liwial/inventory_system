﻿using System;
using UIPanels.Inventory;

namespace UIPanels.Trade { 

    [Serializable]
    public class DictionaryEntry {

        public Item item;
        public int quantity;

        public DictionaryEntry(Item item, int quantity) {
            this.item = item;
            this.quantity = quantity;
        }

    }

}
