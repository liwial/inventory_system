﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class StatParameters {

	public static float monsterKnowledgeMod = 0.05f;
	public static float battleEnduranceMod = 1;
	public static float runEnduranceMod = 1;

	#region Damages Percentages
	public static float trapDmgPercent = 0.1f;
	public static float alchemyDmgPercent = 0.2f;
	public static float daggerDmgPercent = 0.08f;
	public static float swordDmgPercent = 0.08f;
	public static float axeDmgPercent = 0.08f;
	public static float spearDmgPercent = 0.08f;
	public static float javelinDmgPercent = 0.08f;
	public static float arrowDmgPercent = 0.08f;
	#endregion

	#region Damages Ints for weapons
	//public static int alchemyDmgInt = 1;
	public static int daggerDmgInt = 1;
	public static int swordDmgInt = 1;
	public static int axeDmgInt = 1;
	public static int spearDmgInt = 1;
	public static int javelinDmgInt = 1;
	public static int arrowDmgInt = 1;
	#endregion

	#region Damages Regular Ambush for weapons
	public static float daggerDmgRegularAmbush = 0.15f;
	public static float swordDmgRegularAmbush = 0.05f;
	public static float axeDmgRegularAmbush = 0.05f;
	public static float spearDmgRegularAmbush = 0.05f;
	public static float javelinDmgRegularAmbush = 0.1f;
	public static float arrowDmgRegularAmbush = 0.1f;
	#endregion

	#region Damages Hunter's Ambush for weapons
	public static float daggerDmgHuntersAmbush = 0.3f;
	public static float swordDmgHuntersAmbush = 0;
	public static float axeDmgHuntersAmbush = 0;
	public static float spearDmgHuntersAmbush = 0;
	public static float javelinDmgHuntersAmbush = 0;
	public static float arrowDmgHuntersAmbush = 0.2f;
	#endregion

	#region Damages Warrior's Ambush for weapons
	public static float daggerDmgWarriorsAmbush = 0;
	public static float swordDmgWarriorsAmbush = 0.1f;
	public static float axeDmgWarriorsAmbush = 0.1f;
	public static float spearDmgWarriorsAmbush = 0.1f;
	public static float javelinDmgWarriorsAmbush = 0.2f;
	public static float arrowDmgWarriorsAmbush = 0;
	#endregion

	#region Lists
	public static List<float> daggerAmbushList = new List<float> () {daggerDmgRegularAmbush, daggerDmgHuntersAmbush, daggerDmgWarriorsAmbush};
	public static List<float> swordAmbushList = new List<float> () {swordDmgRegularAmbush, swordDmgHuntersAmbush, swordDmgWarriorsAmbush};  
	public static List<float> axeAmbushList = new List<float> () {axeDmgRegularAmbush, axeDmgHuntersAmbush, axeDmgWarriorsAmbush}; 
	public static List<float> spearAmbushList = new List<float> () {spearDmgRegularAmbush, spearDmgHuntersAmbush, spearDmgWarriorsAmbush};
	public static List<float> javelinAmbushList = new List<float> () {javelinDmgRegularAmbush, javelinDmgHuntersAmbush, javelinDmgWarriorsAmbush};
	public static List<float> arrowAmbushList = new List<float> () {arrowDmgRegularAmbush, arrowDmgHuntersAmbush, arrowDmgWarriorsAmbush};  
	#endregion

	#region Variables for calculating Archetype and Skill points
	public static int valueMaxArchetype;
	public static int valueMaxSkill;
	#endregion

	#region Variables and Properties
	private static bool isAmbush = false;
	public static bool IsAmbush {
		get { return isAmbush; }
		set { isAmbush = value; }
	}

	private static bool isHuntersAmbush = false;
	public static bool IsHuntersAmbush {
		get { return isHuntersAmbush; }
		set { isHuntersAmbush = value; }
	}

	private static bool isWarriorsAmbush = false;
	public static bool IsWarriorsAmbush {
		get { return isWarriorsAmbush; }
		set { isWarriorsAmbush = value; }
	}
	#endregion


	#region Colors
	public static Color barColor0 = new Color32(110, 13, 37, 255);
	public static Color barColor1 = new Color32(254, 93, 38, 255);
	public static Color barColor2 = new Color32(242, 192, 120, 255);
	public static Color barColor3 = new Color32(250, 237, 202, 255);
	public static Color barColor4 = new Color32(193, 219, 179, 255);
	public static Color barColor5 = new Color32(126, 188, 137, 255);

	#endregion


	/*
	#region Colors
	public static Color barColor0 = Color.red;
	public static Color barColor1 = Color.magenta;
	public static Color barColor2 = Color.yellow;
	public static Color barColor3 = Color.cyan;
	public static Color barColor4 = Color.green;
	public static Color barColor5 = Color.grey;
	#endregion
	*/

}
