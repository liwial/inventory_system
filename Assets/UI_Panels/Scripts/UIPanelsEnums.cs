﻿namespace UIPanels {

    public enum UIGameplayWindow { MAIN, TRADE, STATS }

	public enum StatType { ONE_VALUE, MINMAX_VALUE, INTERVAL_VALUE, ARCHETYPE, SKILL }

    public enum LMSType { LORE, MIGHT, SURVIVAL }

    public enum StatModifierType { MINMAX_NONE, MINMAX_PERCENT, INTERVAL, ONE_NONE, ONE_PERCENT, ONE_SECOND }

    public enum WeaponType { SWORD, SPEAR, DAGGER, BOW, JAVELIN, AXE, BOMB }

}