﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.Collections;

namespace UIPanels.Trade {

    public class TooltipView : MonoBehaviour {

        public Image image;
        public TextMeshProUGUI tooltipText;

        private IEnumerator SetHeight() {

            tooltipText.rectTransform.sizeDelta = new Vector2(tooltipText.rectTransform.sizeDelta.x, 0);

            while (tooltipText.rectTransform.sizeDelta.y == 0)
                yield return new WaitForSeconds(0.2f);

            float height = tooltipText.rectTransform.sizeDelta.y;
            GetComponent<RectTransform>().sizeDelta = new Vector2(GetComponent<RectTransform>().sizeDelta.x, height);

        }

        public void PrepareTooltip(string tooltipText) {

            //image.color = new Color(image.color.r, image.color.g, image.color.b, 255f);
            //this.tooltipText.alpha = 255;
            this.tooltipText.text = tooltipText;
            StartCoroutine(SetHeight());

        }

        public void ClearTooltip() {

            //image.color = new Color(image.color.r, image.color.g, image.color.b, 0);
            //tooltipText.alpha = 0;
            tooltipText.text = string.Empty;

        }

    }

}