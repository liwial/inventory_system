﻿using UnityEngine;
using TMPro;

namespace UIPanels.Trade {

    public class ChooseQuantityView : MonoBehaviour {

        public TextMeshProUGUI quantityText;

        [SerializeField] private GameObject addOneButton;
        [SerializeField] private GameObject addTenButton;
        [SerializeField] private GameObject removeOneButton;
        [SerializeField] private GameObject removeTenButton;

        public bool GetAddOneButtonState() {
            return addOneButton.activeSelf;
        }

        public bool GetAddTenButtonState() {
            return addTenButton.activeSelf;
        }

        public bool GetRemoveOneButtonState() {
            return removeOneButton.activeSelf;
        }

        public bool GetRemoveTenButtonState() {
            return removeTenButton.activeSelf;
        }


        public void SetQuantityText(int quantity) {

            quantityText.text = quantity.ToString();

        }

        public void AddOneButtonSetActive(bool active) {

            addOneButton.SetActive(active);

        }

        public void AddTenButtonSetActive(bool active) {

            addTenButton.SetActive(active);

        }

        public void RemoveOneButtonSetActive(bool active) {

            removeOneButton.SetActive(active);

        }

        public void RemoveTenButtonSetActive(bool active) {

            removeTenButton.SetActive(active);

        }

    }

}