﻿using UnityEngine;
using TMPro;

namespace UIPanels.Trade {

    public class TradeSideView : MonoBehaviour {

        public TextMeshProUGUI bargainerName;
        public TextMeshProUGUI goldQuantityText;

        public void SetTraderName(string bargainerName) {

            this.bargainerName.text = bargainerName;

        }

        public void SetGoldQuantityText(int goldQuantity) {

            goldQuantityText.text = goldQuantity.ToString();

        }

    }

}