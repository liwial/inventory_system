﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace UIPanels.Trade {

    public class TradeInventoryView : MonoBehaviour {

        public Image imagePortrait;
        public TextMeshProUGUI goldText;
        

        public void SetImagePortrait(Sprite image) {
            imagePortrait.sprite = image;
        }

        public void SetGoldText(string goldText) {
            this.goldText.text = goldText;
        }



    }

}