﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace UIPanels.Trade {

    public class SlotTradeView : MonoBehaviour {

        public Sprite emptySprite;
        public Sprite emptyHighlightedSprite;

        public TextMeshProUGUI quantityText;

        private Image image;
        private Button button;

        void Awake() {

            image = GetComponent<Image>();
            button = GetComponent<Button>();

            quantityText.alpha = 0;

        }

        private void PrepareSlot(Sprite sprite, Sprite highlightedSprite, string quantity, string tooltipText) {

            image.sprite = sprite;

            SpriteState spriteState;
            spriteState.highlightedSprite = highlightedSprite;
            spriteState.pressedSprite = sprite;

            button.spriteState = spriteState;

            quantityText.text = quantity;

            if (quantity == string.Empty)
                quantityText.alpha = 0;
            else
                quantityText.alpha = 255f;

        }


        public void SetSlot(Sprite sprite, Sprite highlightedSprite, string quantity, string tooltipText) {

            PrepareSlot(sprite, highlightedSprite, quantity, tooltipText);

        }

        public void SetSlotEmpty() {

            PrepareSlot(emptySprite, emptyHighlightedSprite, string.Empty, string.Empty);

        }

        public void SetQuantityText(int quantity) {

            quantityText.text = quantity.ToString();

        }

    }

}