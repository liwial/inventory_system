﻿using UIPanels.Inventory;
using UnityEngine;
using UnityEngine.UI;

namespace UIPanels.Trade {

    public class BargainWindowManager : MonoBehaviour {

        [SerializeField] private GameObject traderSide;
        [SerializeField] private GameObject heroSide;

        private TradeSideController traderSideController { get { return traderSide.GetComponent<TradeSideController>(); } }
        private TradeSideController heroSideController { get { return heroSide.GetComponent<TradeSideController>(); } }

        private int traderSideGoldQuantity;
        private int heroSideGoldQuantity;

        #region Private Methods
        private void ChangeItems() {

            traderSideController.SlotsHolder.GetComponent<SlotsHolderManager>().TradeAllSlots();
            heroSideController.SlotsHolder.GetComponent<SlotsHolderManager>().TradeAllSlots();

        }

        private void CalculateGold(int valueForHero, int valueForTrader) {

            TradeManager.Instance.HeroInventory.GetComponent<HeroInventoryController>().UpdateGolQuantity(valueForHero);
            TradeManager.Instance.TraderInventory.GetComponent<TraderInventoryController>().UpdateGolQuantity(valueForTrader);

        }

        private void PromptQuestion() {

            TradeManager.Instance.PromptObject.GetComponent<PromptManager>().PromptShow();

        }

        private void ShowMessage() {

            TradeManager.Instance.MessageObject.GetComponent<MessageManager>().MessageShow();

        }

        public void TraderSideSetView(string traderName, int goldQuantity) {

            traderSideController.SetView(traderName, goldQuantity);
            traderSideGoldQuantity = goldQuantity;

        }

        public void HeroSideSetView(string heroName, int goldQuantity) {

            heroSideController.SetView(heroName, goldQuantity);
            heroSideGoldQuantity = goldQuantity;

        }
        #endregion

        #region Public Methods
        public void TraderSideSetItem(Item item, int quantity) {

            traderSideController.SlotsHolder.GetComponent<SlotsHolderManager>().SetItemOnSlot(item, quantity);

        }

        public void HeroSideSetItem(Item item, int quantity) {

            heroSideController.SlotsHolder.GetComponent<SlotsHolderManager>().SetItemOnSlot(item, quantity);

        }

        public void TraderSideSetGold(int price, int quantity, bool addIt) {

            if (addIt)
                traderSideGoldQuantity += quantity * price;
            else
                traderSideGoldQuantity -= quantity * price;

            traderSideController.UpdateGoldQuantity(traderSideGoldQuantity);

        }

        public void HeroSideSetGold(int price, int quantity, bool addIt) {

            if (addIt)
                heroSideGoldQuantity += quantity * price;
            else
                heroSideGoldQuantity -= quantity * price;

            heroSideController.UpdateGoldQuantity(heroSideGoldQuantity);

        }

        public void DoTheTrade(int valueForHero, int valueForTrader) {

            CalculateGold(valueForHero, valueForTrader);
            ChangeItems();

            traderSideGoldQuantity = 0;
            heroSideGoldQuantity = 0;

            traderSideController.UpdateGoldQuantity(traderSideGoldQuantity);
            heroSideController.UpdateGoldQuantity(heroSideGoldQuantity);

        }

        public void ProceedWithTrade() {

            int lp = heroSideGoldQuantity;
            int rp = traderSideGoldQuantity;

            int lm = TradeManager.Instance.HeroInventory.GetComponent<HeroInventoryController>().GoldQuantity;
            int rm = TradeManager.Instance.TraderInventory.GetComponent<TraderInventoryController>().GoldQuantity;

            if (lp == rp) {

                ChangeItems();

            } else if (lp > rp) {

                if (rm >= (lp - rp)) {

                    int value = lp - rp;
                    DoTheTrade(value, -value);

                } else {

                    PromptQuestion();

                }
 
            } else if (lp < rp) {

                if (lm >= (rp - lp)) {

                    int value = rp - lp;
                    DoTheTrade(-value, value);

                }
                else {

                    ShowMessage();

                }

            }

        }

        public void UndoTrade() {

            traderSideController.ReturnSlots();
            heroSideController.ReturnSlots();

            traderSideGoldQuantity = 0;
            heroSideGoldQuantity = 0;

            traderSideController.UpdateGoldQuantity(traderSideGoldQuantity);
            heroSideController.UpdateGoldQuantity(heroSideGoldQuantity);

        }

        public void CloseTrade(Button button) {

            button.enabled = false;
            button.enabled = true;

            UndoTrade();
            TradeManager.Instance.SetVisibility();

        }
        #endregion

    }

}