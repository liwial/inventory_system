﻿using System.Collections.Generic;
using UIPanels.Inventory;

namespace UIPanels.Trade {

    public class SlotsHolderInventoryManager : SlotsHolderManager {

        public void StickSameItemsTogether() {

            for (int i = slots.Count - 1; i > 0; i--) {

                if (!slots[i].GetComponent<SlotTraderController>().IsEmpty) {

                    for (int j = i - 1; j >= 0; j--) {

                        if (!slots[j].GetComponent<SlotTraderController>().IsEmpty) {

                            if (slots[i].GetComponent<SlotTraderController>().Item == slots[j].GetComponent<SlotTraderController>().Item) {

                                Item itemTmp = slots[j].GetComponent<SlotTraderController>().Item;
                                int quantityTmp = slots[i].GetComponent<SlotTraderController>().Quantity + slots[j].GetComponent<SlotTraderController>().Quantity;

                                slots[i].GetComponent<SlotTraderController>().SetSlot(itemTmp, quantityTmp);
                                slots[j].GetComponent<SlotTraderController>().ClearSlot();
                            }

                        }

                    }

                }

            }

        }

        public void ClearEmptySlotsInBetween() {

            for (int i = 0; i < slots.Count - 1; i++) {

                if (slots[i].GetComponent<SlotTraderController>().IsEmpty) {

                    for (int j = i + 1; j < slots.Count; j++) {

                        if (!slots[j].GetComponent<SlotTraderController>().IsEmpty) {

                            Item itemTmp = slots[j].GetComponent<SlotTraderController>().Item;
                            int quantityTmp = slots[j].GetComponent<SlotTraderController>().Quantity;

                            slots[i].GetComponent<SlotTraderController>().SetSlot(itemTmp, quantityTmp);

                            slots[j].GetComponent<SlotTraderController>().ClearSlot();

                            break;

                        }

                    }

                }

            }
        }

        public void SortByValue() {

            for (int i = 0; i < slots.Count - 1; i++) {

                if (!slots[i].GetComponent<SlotTraderController>().IsEmpty) {

                    for (int j = 0; j <= slots.Count - 1; j++) {

                        if (!slots[j].GetComponent<SlotTraderController>().IsEmpty) {

                            if (slots[i].GetComponent<SlotTraderController>().Item.Price > slots[j].GetComponent<SlotTraderController>().Item.Price) {
                                Item item1 = slots[i].GetComponent<SlotTraderController>().Item;
                                int quantity1 = slots[i].GetComponent<SlotTraderController>().Quantity;

                                Item item2 = slots[j].GetComponent<SlotTraderController>().Item;
                                int quantity2 = slots[j].GetComponent<SlotTraderController>().Quantity;

                                slots[i].GetComponent<SlotTraderController>().SetSlot(item2, quantity2);
                                slots[j].GetComponent<SlotTraderController>().SetSlot(item1, quantity1);
                            }

                        }

                    }

                }

            }
        }


        public List<DictionaryEntry> PrepareItemsToSave() {

            List<DictionaryEntry> resultList = new List<DictionaryEntry>();

            foreach (var slot in slots) {
                if (!slot.GetComponent<SlotTraderController>().IsEmpty) {
                    DictionaryEntry result = new DictionaryEntry(slot.GetComponent<SlotTraderController>().Item, slot.GetComponent<SlotTraderController>().Quantity);
                    resultList.Add(result);
                }
            }

            return resultList;

        }



    }

}