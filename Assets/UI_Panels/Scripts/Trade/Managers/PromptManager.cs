﻿using UIPanels.Gameplay;
using UnityEngine;

namespace UIPanels.Trade {

    [RequireComponent(typeof(CanvasGroup))]
    public class PromptManager : MonoBehaviour {

        public CanvasGroup canvasGroup;

        public void PromptShow() {

            UIGameplayManager.SetCanvasGroup(canvasGroup, true);

        }

        public void PromptHide() {

            UIGameplayManager.SetCanvasGroup(canvasGroup, false);

        }

        public void Proceed() {

            int value = TradeManager.Instance.TraderInventory.GetComponent<TraderInventoryController>().GoldQuantity;
            TradeManager.Instance.BargainWindow.GetComponent<BargainWindowManager>().DoTheTrade(value, -value);

        }

        public void Cancel() {

            PromptHide();

        }

    }

}
