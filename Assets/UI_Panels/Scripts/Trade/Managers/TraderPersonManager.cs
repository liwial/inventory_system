﻿using UnityEngine;
using UnityEngine.UI;

namespace UIPanels.Trade {

    [RequireComponent(typeof(BoxCollider2D))]
    public class TraderPersonManager : MonoBehaviour {

        public string nameUnique;


        private void OnTriggerEnter2D(Collider2D other) {

            if (other.tag == "Player") {

                TradeManager.Instance.PrepareTrader(nameUnique);

                InventoryManager.Instance.PromptObject.SetActive(true);
                InventoryManager.Instance.PromptObject.GetComponent<RectTransform>().position = new Vector3(transform.position.x - 1f, transform.position.y, 0);

                // change prompt text
                InventoryManager.Instance.PromptObject.GetComponentInChildren<Text>().text = "Press 'T' key \nto start trade";

            }
        }

        private void OnTriggerExit2D(Collider2D other) {

            if (other.tag == "Player") {

                TradeManager.Instance.ClearTrader(nameUnique);

                InventoryManager.Instance.PromptObject.SetActive(false);
            }

        }

    }

}