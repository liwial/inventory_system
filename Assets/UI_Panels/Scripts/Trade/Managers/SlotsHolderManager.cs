﻿using System.Collections.Generic;
using UIPanels.Inventory;
using UnityEngine;
using UnityEngine.UI;

namespace UIPanels.Trade {

    [RequireComponent(typeof(GridLayoutGroup))]
    [RequireComponent(typeof(RectTransform))]
    public class SlotsHolderManager : MonoBehaviour {

        public GameObject slotPrefab;

        public int columns;
        public int rows;

        private GridLayoutGroup gridLayoutGroup;
        private RectTransform rectTransform;

        //private float spacingX, spacingY;
        private float cellSizeX, cellSizeY;
        private float addToRow, addToColumn;

        protected List<GameObject> slots = new List<GameObject>();

        void Start() {

            gridLayoutGroup = GetComponent<GridLayoutGroup>();
            rectTransform = GetComponent<RectTransform>();

            //spacingX = gridLayoutGroup.spacing.x;
            //spacingY = gridLayoutGroup.spacing.y;

            cellSizeX = gridLayoutGroup.cellSize.x;
            cellSizeY = gridLayoutGroup.cellSize.y;

            addToRow = gridLayoutGroup.padding.left + gridLayoutGroup.padding.right;
            addToColumn = gridLayoutGroup.padding.top + gridLayoutGroup.padding.bottom;

            CreateSlots();

        }

        private void CreateSlots() {

            float width = columns * cellSizeX + addToColumn;
            float height = rows * cellSizeY + addToRow;

            rectTransform.sizeDelta = new Vector2(width, height);


            for (int i = 0; i < rows; i++) {

                for (int j = 0; j < columns; j++) {

                    GameObject emptySlot = Instantiate(slotPrefab, transform);
                    emptySlot.GetComponent<RectTransform>().localScale = Vector3.one;

                    slots.Add(emptySlot);
                }
            }

        }

        public void SetItemOnSlot(Item item, int quantity = 1) {

            foreach (var slot in slots) {
                if (slot.GetComponent<SlotTraderController>().IsEmpty) {
                    slot.GetComponent<SlotTraderController>().SetSlot(item, quantity);
                    break;
                }
            }

        }

        public void ClearAllSlots() {

            foreach (var slot in slots) {
                if (!slot.GetComponent<SlotTraderController>().IsEmpty) {
                    slot.GetComponent<SlotTraderController>().ClearSlot();
                }
            }

        }

        public void ReturnAllSlots() {

            foreach (var slot in slots) {
                if (!slot.GetComponent<SlotTraderController>().IsEmpty) {
                    slot.GetComponent<SlotTraderController>().ClickSlot();
                }
            }

        }

        public void TradeAllSlots() {

            foreach (var slot in slots) {
                if (!slot.GetComponent<SlotTraderController>().IsEmpty) {
                    slot.GetComponent<SlotTraderController>().TradeSlot();
                }
            }

        }



    }
}