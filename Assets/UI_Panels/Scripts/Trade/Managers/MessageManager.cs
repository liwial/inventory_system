﻿using UIPanels.Gameplay;
using UnityEngine;

namespace UIPanels.Trade {

    [RequireComponent(typeof(CanvasGroup))]
    public class MessageManager : MonoBehaviour {

        public CanvasGroup canvasGroup;

        public void MessageShow() {

            UIGameplayManager.SetCanvasGroup(canvasGroup, true);

        }

        public void MessageHide() {

            UIGameplayManager.SetCanvasGroup(canvasGroup, false);

        }

        public void PressOK() {

            MessageHide();

        }


    }

}