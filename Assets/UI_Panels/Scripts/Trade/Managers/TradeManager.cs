﻿using System.Collections;
using System.IO;
using UIPanels.Gameplay;
using UnityEngine;

namespace UIPanels.Trade {

    public class TradeManager : MonoBehaviour {

        public CanvasGroup canvasGroup;
        public Sprite heroIcon;

        [SerializeField] private GameObject traderInventory;
        [SerializeField] private GameObject heroInventory;
        [SerializeField] private GameObject bargainWindow;

        [SerializeField] private GameObject tooltipPrefab;
        [SerializeField] private GameObject chooseQuantityPrefab;
        [SerializeField] private GameObject promptPrefab;
        [SerializeField] private GameObject messagePrefab;


        private static TradeManager instance;

        private GameObject tooltipObject;
        private GameObject chooseQuantityObject;
        private GameObject promptObject;
        private GameObject messageObject;

        private bool isVisible = false;
        private bool isInstanceSet = false;
        private string saveFolderPath;

        private const string heroNameToShow = "Striver";

        #region Properties
        public static TradeManager Instance { get { return instance; } }

        public GameObject TraderInventory { get { return traderInventory; } }
        public GameObject HeroInventory { get { return heroInventory; } }
        public GameObject BargainWindow { get { return bargainWindow; } }

        public GameObject TooltipObject { get { return tooltipObject; } }
        public GameObject ChooseQuantityObject { get { return chooseQuantityObject; } }
        public GameObject PromptObject { get { return promptObject; } }
        public GameObject MessageObject { get { return messageObject; } }
        #endregion

        #region MonoBehaviour methods
        void Awake() {

            SetInstance();
            StartCoroutine(PrepareGameObjects());

        }

        void Start() {

            saveFolderPath = UIGameplayManager.TradeSaveFolderPath;

        }
        #endregion

        #region Private Methods
        private IEnumerator PrepareGameObjects() {

            yield return new WaitUntil(() => isInstanceSet);

            CreateTooltip();
            CreateChooseQuantity();
            CreatePrompt();
            CreateMessage();

        }

        private void SetInstance() {

            if (instance == null)
                instance = FindObjectOfType<TradeManager>();

            isInstanceSet = true;

        }

        private void CreateTooltip() {

            tooltipObject = Instantiate(tooltipPrefab, transform);
            tooltipObject.GetComponent<RectTransform>().localScale = Vector3.one;

            tooltipObject.transform.SetParent(transform);
            tooltipObject.GetComponent<TooltipController>().TooltipHide();

            tooltipObject.GetComponent<TooltipController>().CanBeShown = true;

        }

        private void CreateChooseQuantity() {

            chooseQuantityObject = Instantiate(chooseQuantityPrefab, transform);
            chooseQuantityObject.GetComponent<RectTransform>().localScale = Vector3.one;

            chooseQuantityObject.transform.SetParent(transform);
            chooseQuantityObject.GetComponent<ChooseQuantityController>().ChooseQuantityHide();

        }

        private void CreatePrompt() {

            promptObject = Instantiate(promptPrefab, transform);
            promptObject.GetComponent<RectTransform>().localScale = Vector3.one;

            promptObject.transform.SetParent(transform);
            promptObject.GetComponent<PromptManager>().PromptHide();


        }

        private void CreateMessage() {

            messageObject = Instantiate(messagePrefab, transform);
            messageObject.GetComponent<RectTransform>().localScale = Vector3.one;

            messageObject.transform.SetParent(transform);
            messageObject.GetComponent<MessageManager>().MessageHide();


        }
        #endregion

        #region Public Methods
        public void PrepareTrader(string traderName) {

            ScriptableTrader scriptableTrader = traderInventory.GetComponent<TraderInventoryController>().GetTrader(traderName);

            if (File.Exists(Path.Combine(saveFolderPath, traderName))) {
                SerializableTrader serializableTrader = BinaryTraderSaver.LoadTrader(Path.Combine(saveFolderPath, traderName));
                traderInventory.GetComponent<TraderInventoryController>().PrepareTraderInventory(scriptableTrader, serializableTrader);
            }
            else
                traderInventory.GetComponent<TraderInventoryController>().PrepareTraderInventory(scriptableTrader);

            bargainWindow.GetComponent<BargainWindowManager>().TraderSideSetView(scriptableTrader.nameToShow, 0);

        }

        public void ClearTrader(string traderName) {

            // save trader state of items and gold
            SerializableTrader serializableTrader = new SerializableTrader();
            serializableTrader.nameUnique = traderName;
            serializableTrader.items = traderInventory.GetComponent<TraderInventoryController>().TraderSlotsHolderManager.PrepareItemsToSave();
            serializableTrader.goldQuantity = traderInventory.GetComponent<TraderInventoryController>().GoldQuantity;

            BinaryTraderSaver.SaveTrader(serializableTrader, Path.Combine(saveFolderPath, traderName));

            // clear slots
            traderInventory.GetComponent<TraderInventoryController>().TraderSlotsHolderManager.ClearAllSlots();

        }

        public void UpdateTraderGoldQuantity(int goldQuantity) {

            traderInventory.GetComponent<TraderInventoryController>().SetGoldQuantity(goldQuantity);

        }

        public void SetVisibility() {

            isVisible = !isVisible;

            UIGameplayManager.SetCanvasGroup(canvasGroup, isVisible);

            if (!isVisible) {

                // saving trader items takes place in method ClearTrader() which is called from TraderPersonManager.cs

                heroInventory.GetComponent<HeroInventoryController>().ClearHeroInventoryItems();

                // temp slution
                InventoryManager.Instance.ShortcutBarHolder.SetActive(true);
                InventoryManager.Instance.GameUIStates = UIStates.DEFAULT;

            } else {

                // loading trader items takes place in method PrepareTrader() which is called from TraderPersonManager.cs

                heroInventory.GetComponent<HeroInventoryController>().LoadHeroInventoryItems();
                heroInventory.GetComponent<HeroInventoryController>().SetView(heroIcon);

                bargainWindow.GetComponent<BargainWindowManager>().HeroSideSetView(heroNameToShow, 0);

                // temp slution
                InventoryManager.Instance.PromptObject.SetActive(false);
                InventoryManager.Instance.ShortcutBarHolder.SetActive(false);
                InventoryManager.Instance.GameUIStates = UIStates.TRADE;
            }

        }
        #endregion

    }

}