﻿using System;
using System.Collections.Generic;

namespace UIPanels.Trade {

	[Serializable]
	public class SerializableTrader {

		public string nameUnique;
        public int goldQuantity;

        public List<DictionaryEntry> items;

	}

}