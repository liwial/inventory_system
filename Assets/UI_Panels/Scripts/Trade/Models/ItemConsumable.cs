﻿using System;

namespace UIPanels.Inventory {

    [Serializable]
    public class ItemConsumable : Item {

        public string Description { get; set; }

        public int FoodPoints { get; set; }
        public int StaminaPoints { get; set; }
        public int HealthPoints { get; set; }
        public int TraumaPoints { get; set; }
        public int WildernesSkillLevel { get; set; }

        public override string GetTooltipText() {

            string tooltip = base.GetTooltipText();

            tooltip += "\n" + Description;

            tooltip += "\n food points: " + FoodPoints.ToString();

            tooltip += "\n stamina points: " + StaminaPoints.ToString();

            tooltip += "\n health points: " + HealthPoints.ToString();

            tooltip += "\n trauma points: " + TraumaPoints.ToString();

            return tooltip;

        }


    }

}