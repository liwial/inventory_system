﻿using System.Xml.Serialization;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace UIPanels.Inventory {

    [XmlRoot("ItemsCollection")]
    public class ItemsContainer {

        [XmlArray("WeaponsList")]
        [XmlArrayItem("Weapon")]
        public List<ItemWeapon> WeaponsList = new List<ItemWeapon>();

        [XmlArray("ArmorsList")]
        [XmlArrayItem("Armor")]
        public List<ItemArmor> ArmorsList = new List<ItemArmor>();

        [XmlArray("BootsList")]
        [XmlArrayItem("Boots")]
        public List<ItemBoots> BootsList = new List<ItemBoots>();

        [XmlArray("AmmunitionList")]
        [XmlArrayItem("Ammo")]
        public List<ItemAmmo> AmmunitionList = new List<ItemAmmo>();

        [XmlArray("ConsumablesList")]
        [XmlArrayItem("Consumable")]
        public List<ItemConsumable> ConsumablesList = new List<ItemConsumable>();

        [XmlArray("CollectiblesList")]
        [XmlArrayItem("Collectible")]
        public List<ItemCollectible> CollectiblesList = new List<ItemCollectible>();

        [XmlArray("TrapsList")]
        [XmlArrayItem("Trap")]
        public List<ItemTrap> TrapsList = new List<ItemTrap>();


        /*
        public void Save(string path) {

            var serializer = new XmlSerializer(typeof(StatsContainer));
            var stream = new FileStream(path, FileMode.Create);
            serializer.Serialize(stream, this);
            stream.Close();

        }
        */

        public static ItemsContainer Load(string path) {

            var serializer = new XmlSerializer(typeof(ItemsContainer));
            var stream = new FileStream(path, FileMode.Open);
            var container = serializer.Deserialize(stream) as ItemsContainer;
            stream.Close();

            return container;

        }

    }

}