﻿using UnityEngine;

namespace UIPanels.Trade {

	[CreateAssetMenu(fileName = "New Trader", menuName = "Traders")]
	public class ScriptableTrader : ScriptableObject {

		public string nameUnique;
		public string nameToShow;
        public int goldQuantityAtStart;
        public Sprite icon;

        //if there is gonna be a need for a specified items
        public string[] itemNames;
        public int[] itemQuantities;

	}

}