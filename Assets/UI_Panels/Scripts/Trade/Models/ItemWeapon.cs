﻿using System;

namespace UIPanels.Inventory {

    [Serializable]
    public class ItemWeapon : ItemTrap {

        public string SpriteEquiped { get; set; }

        public WeaponType WeaponType { get; set; }

    }

}