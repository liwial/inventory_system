﻿using System;

namespace UIPanels.Inventory {

    [Serializable]
    public class ItemAmmo : ItemTrap {

        public string SpriteEquiped { get; set; }

        public int FireDmg { get; set; }
        public int PoisonDmg { get; set; }


        public override string GetTooltipText() {

            string tooltip = base.GetTooltipText();

            if (FireDmg != 0)
                tooltip += "\n fire damage: " + FireDmg.ToString();

            if (PoisonDmg != 0)
                tooltip += "\n poison damage: " + PoisonDmg.ToString();

            return tooltip;

        }

    }

}