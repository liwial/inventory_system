﻿using System;

namespace UIPanels.Inventory {

    [Serializable]
    public class ItemTrap : Item {

        public int DmgMin { get; set; }
        public int DmgMax { get; set; }
        public int ArmorPiercing { get; set; }
        public int CriticalHitChance { get; set; }
        public int CriticalHitChanceSecondValue { get; set; }
        public int CriticalHitMultiplicator { get; set; }
        public int EnergyCost { get; set; }


        public override string GetTooltipText() {

            string tooltip = base.GetTooltipText();

            tooltip += "\n minimal damage: " + DmgMin.ToString();

            tooltip += "\n maximal damage: " + DmgMax.ToString();

            tooltip += "\n armor piercing: " + ArmorPiercing.ToString();

            //...

            tooltip += "\n critical hit multiplicator: " + CriticalHitMultiplicator.ToString() + "%";

            tooltip += "\n energy cost: " + EnergyCost.ToString();

            return tooltip;

        }


    }

}