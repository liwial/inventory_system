﻿using System;
using System.Xml;
using System.Xml.Serialization;

namespace UIPanels.Inventory {

    [Serializable]
    public class Item {

        [XmlAttribute("nameUnique")]
        public string NameUnique { get; set; }
        public string NameToShow { get; set; }

        public ItemType Type { get; set; }

        public string SpriteNeutral { get; set; }
        public string SpriteHighlighted { get; set; }
        //public string SpriteEquiped;

        public int MaxSize { get; set; }

        public bool IsShortcutable { get; set; }
        public bool IsWearable { get; set; }
        public bool IsQuestRelated { get; set; }

        public int Price;


        public virtual string GetTooltipText() {

            return NameToShow;

        }

        public string GetRealPriceText() {

            string priceText = "Price: " + Price.ToString();

            return priceText;
        }

        public int CalculateTradePrice() {

            int priceTrade = UnityEngine.Mathf.FloorToInt(Price/3);

            return priceTrade;
        }

        public string GetTradePriceText() {

            string priceText = "Price: " + CalculateTradePrice();

            return priceText;
        }


    }

}