﻿using System;

namespace UIPanels.Inventory {

    [Serializable]
    public class ItemArmor : Item {

        public string SpriteEquiped { get; set; }

        public int PiercingDamageProtection { get; set; }
        public int CuttingDamageProtection { get; set; }
        public int ShockProtection { get; set; }
        public int FireProtection { get; set; }
        public RUN_STAMINA_USAGE RunStaminaUsage { get; set; }
        public int CriticalHitDefence { get; set; }


        public override string GetTooltipText() {

            string tooltip = base.GetTooltipText();

            tooltip += "\n piercing damage protection: " + PiercingDamageProtection.ToString();

            tooltip += "\n cutting damage protection: " + CuttingDamageProtection.ToString();

            tooltip += "\n shock protection: " + ShockProtection.ToString();

            tooltip += "\n fire protection: " + FireProtection.ToString();

            tooltip += "\n run stamina usage: " + RunStaminaUsage.ToString();

            tooltip += "\n critical hit defence: " + CriticalHitDefence.ToString();

            return tooltip;

        }


    }

}