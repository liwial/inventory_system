﻿using System.Collections.Generic;
using UnityEngine;

namespace UIPanels.Trade {

    public class GetAssetsFromAssetBundleTraders : MonoBehaviour {

        public List<ScriptableTrader> GetAssets(string assetBundleName) {

            List<ScriptableTrader> scriptableTraderList = new List<ScriptableTrader>();
            StartCoroutine(LoadAssetsFromAssetBundles.LoadAssetsFromAssetBundle(assetBundleName, scriptableTraderList));

            return scriptableTraderList;
        }

    }

}