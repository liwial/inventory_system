﻿using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

namespace UIPanels.Trade {

    public class BinaryTraderSaver : MonoBehaviour {

        public SerializableTrader serializableTrader;

        public static void SaveTrader(SerializableTrader data, string path) {

            BinaryFormatter binaryFormatter = new BinaryFormatter();
            using (FileStream fileStream = File.Open (path, FileMode.OpenOrCreate)) {
                binaryFormatter.Serialize(fileStream, data);
            }

        }

        public static SerializableTrader LoadTrader(string path) {

            BinaryFormatter binaryFormatter = new BinaryFormatter();
            using (FileStream fileStream = File.Open(path, FileMode.Open)) {
                return (SerializableTrader)binaryFormatter.Deserialize(fileStream);
            }

        }



    }

}