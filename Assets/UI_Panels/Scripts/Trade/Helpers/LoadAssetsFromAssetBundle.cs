﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

namespace UIPanels.Trade {

	public class LoadAssetsFromAssetBundles {

		private ScriptableTrader scriptableTrader;

		public static IEnumerator LoadAssetsFromAssetBundle(string assetBundleName, List<ScriptableTrader> scriptableTraderList) {

			string filePath = Path.Combine (Application.streamingAssetsPath, "AssetBundles");
			filePath = Path.Combine (filePath, assetBundleName);

			var assetBundleCreateRequest = AssetBundle.LoadFromFileAsync (filePath);
			yield return assetBundleCreateRequest;

			AssetBundle assetBundle = assetBundleCreateRequest.assetBundle;
			AssetBundleRequest asset = assetBundle.LoadAllAssetsAsync<ScriptableTrader> ();
			Object[] tempAssetArray = asset.allAssets;

			foreach (var a in tempAssetArray)
                scriptableTraderList.Add (a as ScriptableTrader);

		}
			
		private IEnumerator LoadAsset(string assetBundleName, string objectNameToLoad) {

			string filePath = Path.Combine (Application.streamingAssetsPath, "AssetBundles");
			filePath = Path.Combine (filePath, assetBundleName);

			var assetBundleCreateRequest = AssetBundle.LoadFromFileAsync (filePath);
			yield return assetBundleCreateRequest;

			AssetBundle assetBundle = assetBundleCreateRequest.assetBundle;

			AssetBundleRequest asset = assetBundle.LoadAssetAsync<ScriptableTrader> (objectNameToLoad);
			yield return asset;

            scriptableTrader = asset.asset as ScriptableTrader;

		}

    }

}