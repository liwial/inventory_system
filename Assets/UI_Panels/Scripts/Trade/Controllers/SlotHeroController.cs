﻿using UIPanels.Inventory;
using UnityEngine;

namespace UIPanels.Trade {

    public class SlotHeroController : SlotTraderController {

        public int TradePrice { get; set; }

        protected override void AddPrice() {

            tooltipText += "\nPrice: " + TradePrice;

        }

        public override void SetSlot(Item item, int quantity) {

            TradePrice = Mathf.FloorToInt(item.Price / 3);

            base.SetSlot(item, quantity);

        }

        public override void ClickSlot() {

            if (!IsEmpty) {

                TooltipHide();

                if (quantity == 1) {

                    TradeManager.Instance.BargainWindow.GetComponent<BargainWindowManager>().HeroSideSetItem(item, quantity);
                    TradeManager.Instance.BargainWindow.GetComponent<BargainWindowManager>().HeroSideSetGold(TradePrice, quantity, true);
                    ClearSlot();

                } else if (quantity > 1) {

                    TradeManager.Instance.TooltipObject.GetComponent<TooltipController>().CanBeShown = false;

                    float xPos = transform.position.x;
                    float yPos = transform.position.y - (GetComponent<RectTransform>().sizeDelta.x / 100);

                    TradeManager.Instance.ChooseQuantityObject.SetActive(true);
                    TradeManager.Instance.ChooseQuantityObject.GetComponent<ChooseQuantityController>().ChooseQuantityShow(this, false, new Vector3(xPos, yPos, transform.position.z));

                }

            }

        }

    }

}