﻿using UIPanels.Inventory;
using UnityEngine;

namespace UIPanels.Trade {

    public class SlotHeroSideController : SlotTraderController {

        public int TradePrice { get; set; }

        protected override void AddPrice() {

            tooltipText += "\nPrice: " + TradePrice;

        }

        public override void SetSlot(Item item, int quantity) {

            TradePrice = Mathf.FloorToInt(item.Price / 3);

            base.SetSlot(item, quantity);

        }

        public override void ClickSlot() {

            if (!IsEmpty) {

                TooltipHide();

                TradeManager.Instance.HeroInventory.GetComponent<HeroInventoryController>().SlotsHolderManager.SetItemOnSlot(item, quantity);
                TradeManager.Instance.BargainWindow.GetComponent<BargainWindowManager>().HeroSideSetGold(TradePrice, quantity, false);

                ClearSlot();

            }

        }

        public override void TradeSlot() {

            TradeManager.Instance.TraderInventory.GetComponent<TraderInventoryController>().SetItem(item, quantity);
            ClearSlot();

        }

    }

}