﻿using UIPanels.Inventory;
using UnityEngine;
using UnityEngine.UI;

namespace UIPanels.Trade {

    public class ChooseQuantityController : MonoBehaviour {

        public ChooseQuantityView chooseQuantityView;
        public CanvasGroup canvasGroup;
        public RectTransform rectTransform;

        private bool toTrader;

        private SlotTraderController slot;
        private Item item;
        private int quantity;
        private int quantityMax;

        private bool CalculateQuantity(int value) {

            int newValue = quantity + value;

            if (newValue <= quantityMax && newValue > 0)
                return true;
            else
                return false;

        }

        private void CheckButtonsVisibility() {

            if (quantity < 10) {
                if (chooseQuantityView.GetRemoveTenButtonState())
                    chooseQuantityView.RemoveTenButtonSetActive(false);
            }

            if (quantity <= 1) {
                if (chooseQuantityView.GetRemoveOneButtonState())
                    chooseQuantityView.RemoveOneButtonSetActive(false);
            }

            if (quantity > (quantityMax - 10)) {
                if (chooseQuantityView.GetAddTenButtonState())
                    chooseQuantityView.AddTenButtonSetActive(false);
            }

            if (quantity == quantityMax) {
                if (chooseQuantityView.GetAddOneButtonState())
                    chooseQuantityView.AddOneButtonSetActive(false);
            }


            if (quantity >= 10) {
                if (!chooseQuantityView.GetRemoveTenButtonState())
                    chooseQuantityView.RemoveTenButtonSetActive(true);
            }

            if (quantity == 2) {
                if (!chooseQuantityView.GetRemoveOneButtonState())
                    chooseQuantityView.RemoveOneButtonSetActive(true);
            }

            if (quantity < (quantityMax - 10)) {
                if (!chooseQuantityView.GetAddTenButtonState())
                    chooseQuantityView.AddTenButtonSetActive(true);
            }

            if (quantity == quantityMax - 1) {
                if (!chooseQuantityView.GetAddOneButtonState())
                    chooseQuantityView.AddOneButtonSetActive(true);
            }

        }



        public void AddOne() {

            if (CalculateQuantity(1)) {
                quantity = quantity + 1;

                CheckButtonsVisibility();

                chooseQuantityView.SetQuantityText(quantity);
            }

        }

        public void AddTen() {

            if (CalculateQuantity(10)) {
                quantity = quantity + 10;

                CheckButtonsVisibility();

                chooseQuantityView.SetQuantityText(quantity);
            }
        }

        public void RemoveOne() {

            if (CalculateQuantity(-1)) {
                quantity = quantity - 1;

                CheckButtonsVisibility();

                chooseQuantityView.SetQuantityText(quantity);
            }

        }

        public void RemoveTen() {

            if (CalculateQuantity(-10)) {
                quantity = quantity - 10;

                CheckButtonsVisibility();

                chooseQuantityView.SetQuantityText(quantity);
            }

        }

        public void PressOK(Button button) {

            button.enabled = false;
            button.enabled = true;

            if (toTrader) {
                TradeManager.Instance.BargainWindow.GetComponent<BargainWindowManager>().TraderSideSetItem(item, quantity);
                TradeManager.Instance.BargainWindow.GetComponent<BargainWindowManager>().TraderSideSetGold(item.Price, quantity, true);
            } else { 
                TradeManager.Instance.BargainWindow.GetComponent<BargainWindowManager>().HeroSideSetItem(item, quantity);
                TradeManager.Instance.BargainWindow.GetComponent<BargainWindowManager>().HeroSideSetGold(slot.GetComponent<SlotHeroController>().TradePrice, quantity, true);
            }

            if (quantityMax - quantity > 0)
                slot.SetSlot(item, quantityMax - quantity);

            ChooseQuantityHide();
        }

        public void PresssCancel(Button button) {

            button.enabled = false;
            button.enabled = true;
            ChooseQuantityHide();

        }

        public void ChooseQuantityShow(SlotTraderController slot, bool toTrader, Vector3 position) {

            this.toTrader = toTrader;

            this.slot = slot;
            item = slot.Item;
            quantity = slot.Quantity;
            quantityMax = slot.Quantity;

            chooseQuantityView.SetQuantityText(quantity);

            CheckButtonsVisibility();

            canvasGroup.alpha = 1;
            canvasGroup.interactable = true;
            canvasGroup.blocksRaycasts = true;

            Vector3 rightSideScreenLocal = transform.InverseTransformPoint(Camera.main.ViewportToWorldPoint(new Vector3(1f, 0f, 0f)));
            Vector3 positionLocal = transform.InverseTransformPoint(position);

            float rightSideDiff = rightSideScreenLocal.x - (positionLocal.x + rectTransform.sizeDelta.x);

            if (rightSideDiff <= 0)
                rectTransform.pivot = new Vector2(1f, 1f);
            else
                rectTransform.pivot = new Vector2(0f, 1f);

            transform.position = position;
        }

        public void ChooseQuantityHide() {

            chooseQuantityView.SetQuantityText(0);

            canvasGroup.alpha = 0;
            canvasGroup.interactable = false;
            canvasGroup.blocksRaycasts = false;

            TradeManager.Instance.TooltipObject.GetComponent<TooltipController>().CanBeShown = true;

        }

    }

}