﻿using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UIPanels.Inventory;

namespace UIPanels.Trade {

    public class HeroInventoryController : MonoBehaviour {

        public TradeInventoryView traderInventoryView;
        [SerializeField] private GameObject slotsHolder;

        private int goldQuantity;

        public SlotsHolderManager SlotsHolderManager { get { return slotsHolder.GetComponent<SlotsHolderManager>(); } }
        public int GoldQuantity { get { return goldQuantity; } }

        void Start() {

            //slotsHolderManager = slotsHolder.GetComponent<SlotsHolderManager>();

        }

        public void LoadHeroInventoryItems() {

            InventoryManager.Instance.GetInventoryAndSaveForTrade();

            string content = PlayerPrefs.GetString("heroTrade");

            string[] splitContent = content.Split(';');

            for (int x = 0; x < splitContent.Length - 1; x++) {

                string[] splitValues = splitContent[x].Split('-');
                //int index = System.Int32.Parse(splitValues[0]);
                //ItemType type = (ItemType)System.Enum.Parse(typeof(ItemType), splitValues[1]);
                string itemName = splitValues[2];
                int amount = System.Int32.Parse(splitValues[3]);
                bool isEquiped = System.Convert.ToBoolean(splitValues[4]);

                if (!isEquiped) {
                    Item newItem = TradeManager.Instance.TraderInventory.GetComponent<TraderInventoryController>().GetItem(itemName);
                    SlotsHolderManager.SetItemOnSlot(newItem, amount);

                }

            }

        }

        public void ClearHeroInventoryItems() {

            SlotsHolderManager.ClearAllSlots();

        }

        public void SetView(Sprite icon) {

            traderInventoryView.SetImagePortrait(icon);
            SetGoldQuantity(System.Int32.Parse(InventoryManager.Instance.goldTxt.text));

        }

        public void SetItem(Item item, int goldQuantity) {

            slotsHolder.GetComponent<SlotsHolderInventoryManager>().SetItemOnSlot(item, goldQuantity);

        }

        //public void SetGold(Item item, int goldQuantity) {

        //    int tmpGoldQuantity = this.goldQuantity + goldQuantity * item.Price;
        //    UpdateGoldQuantity(goldQuantity);

        //}

        public void SetGoldQuantity(int goldQuantity) {

            this.goldQuantity = goldQuantity;
            traderInventoryView.SetGoldText(goldQuantity.ToString());

        }

        public void UpdateGolQuantity(int value) {

            goldQuantity += value;
            traderInventoryView.SetGoldText(goldQuantity.ToString());

        }

    }

}