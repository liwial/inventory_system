﻿using UnityEngine;

namespace UIPanels.Trade {

    public class TradeSideController : MonoBehaviour {

        public TradeSideView tradeSideView;
        [SerializeField] private GameObject slotsHolder;

        public GameObject SlotsHolder { get { return slotsHolder; } }


        public void SetView(string bargainerName, int goldQuantity) {

            tradeSideView.SetTraderName(bargainerName);
            UpdateGoldQuantity(goldQuantity);

        }

        public void UpdateGoldQuantity(int goldQuantity) {

            tradeSideView.SetGoldQuantityText(goldQuantity);

        }

        public void ReturnSlots() {

            slotsHolder.GetComponent<SlotsHolderManager>().ReturnAllSlots();

        }

    }

}