﻿namespace UIPanels.Trade {

    public class SlotTraderSideController : SlotTraderController {

        
        public override void ClickSlot() {

            if (!IsEmpty) {

                TooltipHide();

                TradeManager.Instance.TraderInventory.GetComponent<TraderInventoryController>().SetItem(item, quantity);
                TradeManager.Instance.BargainWindow.GetComponent<BargainWindowManager>().TraderSideSetGold(item.Price, quantity, false);

                ClearSlot();

            }

        }

        public override void TradeSlot() {

            TradeManager.Instance.HeroInventory.GetComponent<HeroInventoryController>().SetItem(item, quantity);
            ClearSlot();

        }

    }

}