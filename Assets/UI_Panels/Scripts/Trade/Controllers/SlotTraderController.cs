﻿using UIPanels.Inventory;
using UnityEngine;


namespace UIPanels.Trade {

    public class SlotTraderController : MonoBehaviour {

        public SlotTradeView slotTradeView;

        protected Item item;

        private Sprite spriteNeutral;
        private Sprite spriteHighlighted;

        protected int quantity;
        protected string tooltipText;

        public bool IsEmpty { get; set; }
        public Item Item { get { return item; } }
        public int Quantity { get { return quantity; } }


        void Awake() {

            IsEmpty = true;

        }

        protected virtual void AddPrice() {

            tooltipText += "\nPrice: " + item.Price;

        }

        public string GetItemNameUnique() {

            return item.NameUnique;

        }

        public virtual void SetSlot(Item item, int quantity) {

            this.item = item;
            this.quantity = quantity;

            tooltipText = item.GetTooltipText();
            
            string tmp1 = "ItemIcons/" + item.SpriteNeutral;
            string tmp2 = "ItemIcons/" + item.SpriteHighlighted;

            spriteNeutral = (Sprite)Resources.Load(tmp1, typeof(Sprite));
            spriteHighlighted = (Sprite)Resources.Load(tmp2, typeof(Sprite));

            slotTradeView.SetSlot(spriteNeutral, spriteHighlighted, quantity.ToString(), item.GetTooltipText());

            IsEmpty = false;

            AddPrice();
        }

        public void ClearSlot() {

            slotTradeView.SetSlotEmpty();
            item = null;
            IsEmpty = true;

        }

        public void TooltipShow() {

            if (!IsEmpty) {

                float xPos = transform.position.x; 
                float yPos = transform.position.y - (GetComponent<RectTransform>().sizeDelta.x / 100);

                TradeManager.Instance.TooltipObject.GetComponent<TooltipController>().TooltipShow(tooltipText, new Vector3(xPos, yPos, transform.position.z));

            }

        }

        public void TooltipHide() {

            if (!IsEmpty)
                TradeManager.Instance.TooltipObject.GetComponent<TooltipController>().TooltipHide();

        }

        public virtual void ClickSlot() {

            if (!IsEmpty) {

                TooltipHide();

                if (quantity == 1) {

                    TradeManager.Instance.BargainWindow.GetComponent<BargainWindowManager>().TraderSideSetItem(item, quantity);
                    TradeManager.Instance.BargainWindow.GetComponent<BargainWindowManager>().TraderSideSetGold(item.Price, quantity, true);
                    ClearSlot();

                } else if (quantity > 1) {

                    TradeManager.Instance.TooltipObject.GetComponent<TooltipController>().CanBeShown = false;


                    float xPos = transform.position.x;
                    float yPos = transform.position.y - (GetComponent<RectTransform>().sizeDelta.x / 100);

                    TradeManager.Instance.ChooseQuantityObject.SetActive(true);
                    TradeManager.Instance.ChooseQuantityObject.GetComponent<ChooseQuantityController>().ChooseQuantityShow(this, true, new Vector3(xPos, yPos, transform.position.z));

                }

            }

        }

        public virtual void TradeSlot() {

        }

    }

}