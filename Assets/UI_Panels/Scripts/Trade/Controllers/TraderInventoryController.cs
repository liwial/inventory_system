﻿using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UIPanels.Inventory;

namespace UIPanels.Trade {

    public class TraderInventoryController : MonoBehaviour {

        public TradeInventoryView traderInventoryView;

        [SerializeField] private GameObject slotsHolder;

        public int numberOfItemsMin;
        public int numberOfItemsMax;

        public int quantityForStackablesMin;
        public int quantityForStackablesMax;

        public int weaponsWeight;
        public int armorsWeight;
        public int bootsWeight;
        public int ammunitionWeight;
        public int consumablesWeight;
        public int trapsWeight;

        private const string assetNameTrade = "traders";
        private int goldQuantity;

        public SlotsHolderInventoryManager TraderSlotsHolderManager { get { return slotsHolder.GetComponent<SlotsHolderInventoryManager>(); } }
        public int GoldQuantity { get { return goldQuantity; } }

        private List<ItemWeapon> weaponsList = new List<ItemWeapon>();
        private List<ItemArmor> armorsList = new List<ItemArmor>();
        private List<ItemBoots> bootsList = new List<ItemBoots>();
        private List<ItemAmmo> ammunitionList = new List<ItemAmmo>();
        private List<ItemConsumable> consumablesList = new List<ItemConsumable>();
        private List<ItemTrap> trapsList = new List<ItemTrap>();

        private List<ItemCollectible> collectiblesList = new List<ItemCollectible>();


        private List<ScriptableTrader> ScriptableTradersList { get; set; }


		void Awake() {

			PrepareItems();
            PrepareTraders();

		}

        private void PrepareItems() {

            string path = Path.Combine(Application.streamingAssetsPath, "XMLS");
            path = Path.Combine(path, "Items3.xml");

            if (File.Exists(path)) {

                ItemsContainer itemsContainer = ItemsContainer.Load(path);

                weaponsList = new List<ItemWeapon>(itemsContainer.WeaponsList);
                armorsList = new List<ItemArmor>(itemsContainer.ArmorsList);
                bootsList = new List<ItemBoots>(itemsContainer.BootsList);
                ammunitionList = new List<ItemAmmo>(itemsContainer.AmmunitionList);
                consumablesList = new List<ItemConsumable>(itemsContainer.ConsumablesList);
                trapsList = new List<ItemTrap>(itemsContainer.TrapsList);

            }

        }

        public Item GetItem(string itemName) {

            foreach (var i in weaponsList) {
                if (i.NameToShow == itemName) {
                    return i;
                }
            }

            foreach (var i in armorsList) {
                if (i.NameToShow == itemName) {
                    return i;
                }
            }

            foreach (var i in bootsList) {
                if (i.NameToShow == itemName) {
                    return i;
                }
            }

            foreach (var i in ammunitionList) {
                if (i.NameToShow == itemName) {
                    return i;
                }
            }

            foreach (var i in consumablesList) {
                if (i.NameToShow == itemName) {
                    return i;
                }
            }

            foreach (var i in trapsList) {
                if (i.NameToShow == itemName) {
                    return i;
                }
            }

            foreach (var i in collectiblesList) {
                if (i.NameToShow == itemName) {
                    return i;
                }
            }

            return null;

        }

        private void PrepareTraders() {

			ScriptableTradersList = GetAssetsFromAssetBundle(assetNameTrade);

        }

        private List<ScriptableTrader> GetAssetsFromAssetBundle(string assetBundleName) {

            gameObject.AddComponent<GetAssetsFromAssetBundleTraders>();
            GetAssetsFromAssetBundleTraders g = GetComponent<GetAssetsFromAssetBundleTraders>();

            return g.GetAssets(assetBundleName);

        }

        private DictionaryEntry ChooseItem(ItemType itemType) {

            Item item = new Item();
            int quantity = 0;

            switch (itemType) {
                case ItemType.WEAPON:
                    quantity = 1;
                    item = weaponsList[Random.Range(0, weaponsList.Count - 1)];
                    break;
                case ItemType.ARMOR:
                    quantity = 1;
                    item = armorsList[Random.Range(0, armorsList.Count - 1)];
                    break;
                case ItemType.BOOTS:
                    quantity = 1;
                    item = bootsList[Random.Range(0, bootsList.Count - 1)];
                    break;
                case ItemType.AMMO:
                    quantity = Random.Range(quantityForStackablesMin, quantityForStackablesMax);
                    item = ammunitionList[Random.Range(0, ammunitionList.Count - 1)];
                    break;
                case ItemType.CONSUMABLE:
                    quantity = Random.Range(quantityForStackablesMin, quantityForStackablesMax);
                    item = consumablesList[Random.Range(0, consumablesList.Count - 1)];
                    break;
                case ItemType.TRAP:
                    quantity = Random.Range(quantityForStackablesMin, quantityForStackablesMax);
                    item = trapsList[Random.Range(0, trapsList.Count - 1)];
                    break;
                default:
                    break;
            }

            DictionaryEntry result = new DictionaryEntry(item, quantity);

            return result;

        }

        private List<DictionaryEntry> ChooseItems() {

            List<ItemType> weightList = new List<ItemType>();
            List<DictionaryEntry> traderItems = new List<DictionaryEntry>();

            for (int i = 0; i < weaponsWeight; i++)
                weightList.Add(ItemType.WEAPON);
            for (int i = 0; i < armorsWeight; i++)
                weightList.Add(ItemType.ARMOR);
            for (int i = 0; i < bootsWeight; i++)
                weightList.Add(ItemType.BOOTS);
            for (int i = 0; i < ammunitionWeight; i++)
                weightList.Add(ItemType.AMMO);
            for (int i = 0; i < consumablesWeight; i++)
                weightList.Add(ItemType.CONSUMABLE);
            for (int i = 0; i < trapsWeight; i++)
                weightList.Add(ItemType.TRAP);


            int numberOfItems = Random.Range(numberOfItemsMin, numberOfItemsMax);

            for (int i = 0; i < numberOfItems; i++) {

                int resultInt = Random.Range(0, weightList.Count - 1);

                ItemType resultType = weightList[resultInt];

                DictionaryEntry dictionaryEntry = ChooseItem(resultType);


                traderItems.Add(dictionaryEntry);

                weightList.RemoveAt(resultInt);

            }

            return traderItems;

        }

        private void SetView(Sprite icon, int goldQuantity) {

            traderInventoryView.SetImagePortrait(icon);
            SetGoldQuantity(goldQuantity);

        }

        public ScriptableTrader GetTrader(string traderName) {

            foreach (var trader in ScriptableTradersList) {

                if (trader.nameUnique == traderName)
                    return trader;

            }

            Debug.LogError("Could not find trader with specified name");
            return null;

        }

        public void PrepareTraderInventory(ScriptableTrader scriptableTrader, SerializableTrader serializableTrader = null) {

            List<DictionaryEntry> traderItems = new List<DictionaryEntry>();

            if (serializableTrader != null) {
                SetView(scriptableTrader.icon, serializableTrader.goldQuantity);
                traderItems = serializableTrader.items;
            } else {
                SetView(scriptableTrader.icon, scriptableTrader.goldQuantityAtStart);
                traderItems = ChooseItems();
            }

            foreach (var d in traderItems)
                TraderSlotsHolderManager.SetItemOnSlot(d.item, d.quantity);

            slotsHolder.GetComponent<SlotsHolderInventoryManager>().StickSameItemsTogether();
            slotsHolder.GetComponent<SlotsHolderInventoryManager>().ClearEmptySlotsInBetween();
            slotsHolder.GetComponent<SlotsHolderInventoryManager>().SortByValue();

        }

        public void SetItem(Item item, int goldQuantity) {

            slotsHolder.GetComponent<SlotsHolderInventoryManager>().SetItemOnSlot(item, goldQuantity);

        }

        public void SetGoldQuantity(int goldQuantity) {

            this.goldQuantity = goldQuantity;
            traderInventoryView.SetGoldText(goldQuantity.ToString());

        }

        public void UpdateGolQuantity(int value) {

            goldQuantity += value;
            traderInventoryView.SetGoldText(goldQuantity.ToString());

        }

    }

}