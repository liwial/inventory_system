﻿using UnityEngine;

namespace UIPanels.Trade {

    public class TooltipController : MonoBehaviour {

        public TooltipView tooltipView;
        public CanvasGroup canvasGroup;
        public RectTransform rectTransform;

        public bool CanBeShown { get; set; }

        public void TooltipShow(string tooltipText, Vector3 position) {

            if (CanBeShown) {

                tooltipView.PrepareTooltip(tooltipText);
                canvasGroup.alpha = 1;
                canvasGroup.interactable = true;
                canvasGroup.blocksRaycasts = true;

                Vector3 rightSideScreenLocal = transform.InverseTransformPoint(Camera.main.ViewportToWorldPoint(new Vector3(1f, 0f, 0f)));
                Vector3 positionLocal = transform.InverseTransformPoint(position);

                float rightSideDiff = rightSideScreenLocal.x - (positionLocal.x + rectTransform.sizeDelta.x);

                if (rightSideDiff <= 0)
                    rectTransform.pivot = new Vector2(1f, 1f);
                else
                    rectTransform.pivot = new Vector2(0f, 1f);

                transform.position = position;

            }

        }

        public void TooltipHide() {

            tooltipView.ClearTooltip();
            canvasGroup.alpha = 0;
            canvasGroup.interactable = false;
            canvasGroup.blocksRaycasts = false;

        }

    }

}