﻿using UnityEngine;

namespace UIPanels.CharacterStats {

    public class StatControllerMinMax: StatController {

        #region Protected Methods
        private int IncrementStatValueSecond() {
            statPreserved.valueSecond = statPreserved.valueSecond + 1;
            return statPreserved.valueSecond;
        }

        private int DecrementStatValueSecond() {
            statPreserved.valueSecond = statPreserved.valueSecond - 1;
            return statPreserved.valueSecond;
        }
        #endregion

        #region Public Methods
        public bool IncreaseStatValueSecond(){

            UpdateValue updateValueMethodToCall = IncrementStatValueSecond;
            UpdateValueText updateTextMethodToCall = statViev.UpdateValueSecondText;

            return DoIncrementStatValue(statPreserved.valueSecond, updateValueMethodToCall, updateTextMethodToCall);

        }

        public bool DecreaseStatValueSecond() {

            UpdateValue updateValueMethodToCall = DecrementStatValue;
            UpdateValueText updateTextMethodToCall = statViev.UpdateValueSecondText;

            return DoDecrementStatValue(statPreserved.valueSecond, updateValueMethodToCall, updateTextMethodToCall);

        }
        #endregion

    }

}