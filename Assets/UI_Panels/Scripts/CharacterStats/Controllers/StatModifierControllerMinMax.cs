﻿namespace UIPanels.CharacterStats {

    public class StatModifierControllerMinMax : StatModifierController {

        private float valueSecond;

        public float ValueSecond {
            get { return valueSecond; }
        }


        public override void SetStat(ScriptableStatModifier scriptableStatModifier) {

            this.scriptableStatModifier = scriptableStatModifier;

            string valueText = CalculateValue();

            string valueSecondText = CalculateValueSecond();

            statModifierViev.SetStatModifierTexts(scriptableStatModifier, valueText, valueSecondText);
        }

        private string CalculateValueSecond() {

            //TODO
            //set float valueSecond here also

            return string.Empty;
        }

    }

}