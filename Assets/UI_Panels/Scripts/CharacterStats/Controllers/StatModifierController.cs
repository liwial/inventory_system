﻿using UnityEngine;

namespace UIPanels.CharacterStats {

    public class StatModifierController : MonoBehaviour {

        public StatModifierViev statModifierViev;

        protected ScriptableStatModifier scriptableStatModifier;

        protected float value;

        public float Value {
            get { return value; }
        }


        public virtual void SetStat(ScriptableStatModifier scriptableStatModifier) {

            this.scriptableStatModifier = scriptableStatModifier;

            string valueText = CalculateValue();

            statModifierViev.SetStatModifierTexts(scriptableStatModifier, valueText);
        }

        protected string CalculateValue() {

            //TODO
            //set float value here also

            return string.Empty;
        }

    }

}