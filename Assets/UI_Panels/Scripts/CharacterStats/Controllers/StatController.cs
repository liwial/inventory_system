﻿using UnityEngine;

namespace UIPanels.CharacterStats {

    public class StatController : MonoBehaviour {

        public StatViev statViev;

        public int maxStatValue;
        public int minStatValue;

        protected ScriptableStat scriptableStat;
        protected ToPreserveStat statPreserved;

        protected delegate int UpdateValue();
        protected delegate void UpdateValueText(int value);

        #region Protected Methods
        protected int IncrementStatValue() {
            statPreserved.value = statPreserved.value + 1;
            return statPreserved.value;
        }

        protected int DecrementStatValue() {
            statPreserved.value = statPreserved.value - 1;
            return statPreserved.value;
        }

        protected bool DoIncrementStatValue(int value, UpdateValue updateValueMethod, UpdateValueText updateValueTextMethod) {

            if (!scriptableStat)
                return false;
            else {
                if (value <= maxStatValue) {
                    int newValue = updateValueMethod();
                    updateValueTextMethod(newValue);

                    return true;
                }
                else
                    return false;

            }

        }

        protected bool DoDecrementStatValue(int value, UpdateValue updateValueMethod, UpdateValueText updateValueTextMethod) {

            if (!scriptableStat)
                return false;
            else {
                if (value >= minStatValue) {

                    int newValue = updateValueMethod();

                    updateValueTextMethod(newValue);

                    return true;
                }
                else
                    return false;

            }

        }
        #endregion

        #region Public Methods
        public virtual void SetStat(ScriptableStat scriptableStat, ToPreserveStat statPreserved) {

            this.scriptableStat = scriptableStat;
            this.statPreserved = statPreserved; 

            statViev.SetStatTexts(scriptableStat, statPreserved);
        }

        public int GetStatValue() {
            return statPreserved.value;
        }

        public string GetStatNameUnique() {
            return statPreserved.nameUnique;
        }

        public StatType GetStatType() {
            return scriptableStat.statType;
        }

        public virtual LMSType GetArchetypeType() {
            return LMSType.LORE;
        }

        public virtual bool IncreaseStatValue() {

            UpdateValue updateValueMethodToCall = IncrementStatValue;
            UpdateValueText updateTextMethodToCall = statViev.UpdateValueText;

            return DoIncrementStatValue(statPreserved.value, updateValueMethodToCall, updateTextMethodToCall);
        }

        public virtual bool DecreaseStatValue() {

            UpdateValue updateValueMethodToCall = DecrementStatValue;
            UpdateValueText updateTextMethodToCall = statViev.UpdateValueText;

            return DoDecrementStatValue(statPreserved.value, updateValueMethodToCall, updateTextMethodToCall);
        }
        #endregion

    }

}