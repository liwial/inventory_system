﻿using System.Collections;

namespace UIPanels.CharacterStats {

    public class StatControllerSkill : StatController {

        private int skillPoints;

        private int pointsForSession = 0;

        //private LMSType lmsType;

        private LMSManager lmsManager;


        #region Private Methods
        private void PrepareButtons() {

            lmsManager = transform.parent.GetComponent<LMSManager>();

            StartCoroutine(SetNeededStatValuesAndPrepareButtons());

            if (pointsForSession <= 0)
                statViev.ActivateRemoveButton(false);

        }

        private IEnumerator SetNeededStatValuesAndPrepareButtons() {

            yield return StartCoroutine(lmsManager.MakeSureStatsAreSetFirst());

            skillPoints = GetSkillPoints();
            //lmsType = GetArchetypeType();

            if (statPreserved.value < maxStatValue && skillPoints > 0)
                statViev.ActivateAddButton(true);
        }

        private int GetSkillPoints() {
            return lmsManager.SkillPoints;
        }
        #endregion

        #region Public Methods
        public override void SetStat(ScriptableStat scriptableStat, ToPreserveStat statPreserved) {

            base.SetStat(scriptableStat, statPreserved);

            PrepareButtons();

        }

        public override LMSType GetArchetypeType() {

            ScriptableStatSkill scriptableStatSkill = (ScriptableStatSkill)scriptableStat;

            return scriptableStatSkill.lmsType;
        }

        public override bool IncreaseStatValue() {

            if (statPreserved.value + 1 <= maxStatValue) {

                if (statPreserved.value + 1 == maxStatValue)
                    statViev.ActivateAddButton(false);

                statPreserved.value = statPreserved.value + 1;

                statViev.UpdateValueText(statPreserved.value);

                return true;
            }

            return false;
        }

        public override bool DecreaseStatValue() {

            if (statPreserved.value - 1 >= minStatValue) {

                if (statPreserved.value - 1 == minStatValue)
                    statViev.ActivateRemoveButton(false);

                statPreserved.value = statPreserved.value - 1;

                statViev.UpdateValueText(statPreserved.value);

                return true;
            }

            return false;
        }

        public void PressAddButton() {

            bool result;

            if (GetSkillPoints() > 0)
                result = IncreaseStatValue();
            else
                result = false;

            if (result) {
                lmsManager.DecreaseSkillPoints();

                if (GetSkillPoints() <= 0)
                    lmsManager.ActivateAllAddButtonsSkill(false);

                if (pointsForSession == 0)
                    statViev.ActivateRemoveButton(true);

                pointsForSession++;
                
            }

        }

        public void PressRemoveButton() {

            bool result;

            if (pointsForSession > 0)
                result = DecreaseStatValue();
            else
                result = false;

            if (result) {
                lmsManager.IncreaseSkillPoints();

                lmsManager.PrepareAllAddButtonsSkill();


                if (pointsForSession == 1)
                    statViev.ActivateRemoveButton(false);

                pointsForSession--;
            }
        }

        public void CleaPointsForSession() {
            pointsForSession = 0;
        }
        #endregion

    }

}