﻿using System;
using System.Collections;

namespace UIPanels.CharacterStats {

    public class StatControllerArchetype : StatController {

        private int level;
        private int archetypePoints;
  
        private int pointsForSession = 0;

        private LMSType lmsType;

        private LMSManager lmsManager;


        #region Private Methods
        private void PrepareButtons() {

            lmsManager = transform.parent.GetComponent<LMSManager>();

            StartCoroutine(SetNeededStatValuesAndPrepareButtons());

            if (pointsForSession <= 0)
                statViev.ActivateRemoveButton(false);

        }

        private IEnumerator SetNeededStatValuesAndPrepareButtons() {

            yield return StartCoroutine(lmsManager.MakeSureStatsAreSetFirst());

            level = GetLevel();
            archetypePoints = GetArchetypePoints();
            lmsType = GetArchetypeType();

            maxStatValue = level;

            if (statPreserved.value < level && archetypePoints > 0)
                statViev.ActivateAddButton(true);
        }

        private int GetLevel() {
            return Math.Min(lmsManager.Level, maxStatValue);
        }

        private int GetArchetypePoints() {
            return lmsManager.ArchetypePoints;
        }
        #endregion

        #region Public Methods
        public override void SetStat(ScriptableStat scriptableStat, ToPreserveStat statPreserved) {

            base.SetStat(scriptableStat, statPreserved);

            PrepareButtons();

        }

        public override LMSType GetArchetypeType() {

            ScriptableStatArchetype scriptableStatSkill = (ScriptableStatArchetype)scriptableStat;

            return scriptableStatSkill.lmsType;
        }

        public override bool IncreaseStatValue() {

            if (statPreserved.value + 1 <= level) {

                if (statPreserved.value + 1 == level)
                    statViev.ActivateAddButton(false);

                statPreserved.value = statPreserved.value + 1;

                lmsManager.DecreaseArchetypePoints();
                if (GetArchetypePoints() <= 0)
                    lmsManager.ActivateAllAddButtonsArchetype(false);

                if (pointsForSession == 0)
                    statViev.ActivateRemoveButton(true);

                statViev.UpdateValueText(statPreserved.value);

                lmsManager.AddToCorespondingSkills(lmsType);

                return true;
            }

            return false;
        }

        public override bool DecreaseStatValue() {

            if (statPreserved.value - 1 >= minStatValue) {

                if (statPreserved.value - 1 == minStatValue || pointsForSession == 1)
                    statViev.ActivateRemoveButton(false);

                statPreserved.value = statPreserved.value - 1;

                lmsManager.IncreaseArchetypePoints();
                lmsManager.PrepareAllAddButtonsArchetype();

                statViev.UpdateValueText(statPreserved.value);

                lmsManager.RemoveFromCorespondingSkills(lmsType);

                return true;
            }

            return false;
        }

        public void PressAddButton() {
            
            bool result = IncreaseStatValue();

            if (result)
                pointsForSession++;

        }

        public void PressRemoveButton() {

            bool result = DecreaseStatValue();

            if (result)
                pointsForSession--;
        }

        public void CleaPointsForSession() {
            pointsForSession = 0;
        }
        #endregion

    }

}