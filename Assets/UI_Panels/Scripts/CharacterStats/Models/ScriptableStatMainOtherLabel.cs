﻿using UnityEngine;

namespace UIPanels.CharacterStats {

	[CreateAssetMenu(fileName = "New Main Other Label Stat", menuName = "Stats/MainOtherLabel")]
	public class ScriptableStatMainOtherLabel : ScriptableStat {

	}

}