﻿using UnityEngine;

namespace UIPanels.CharacterStats {

	[CreateAssetMenu(fileName = "New Reputation Stat", menuName = "Stats/Reputation")]
	public class ScriptableStatReputation : ScriptableStat {

	}

}