﻿using System.Xml;
using System.Xml.Serialization;

namespace UIPanels.CharacterStats {

    public class ToPreserveStat {

        [XmlAttribute("nameUnique")]
        public string nameUnique;

        public int value;

        public int valueSecond;

        public ToPreserveStat() {

        }

        public ToPreserveStat(string nameUnique, int value) {
            this.nameUnique = nameUnique;
            this.value = value;
        }

        public ToPreserveStat(string nameUnique, int value, int valueSecond) {
            this.nameUnique = nameUnique;
            this.value = value;
            this.valueSecond = valueSecond;
        }

    }

}