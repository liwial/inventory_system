﻿using UnityEngine;

namespace UIPanels.CharacterStats {

	[CreateAssetMenu(fileName = "New Stat Modifier", menuName = "Stats/Modifier")]
	public class ScriptableStatModifier : ScriptableStat {

		public StatModifierType statModifierType;

	}

}