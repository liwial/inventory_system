﻿using System.Xml.Serialization;
using System.Collections.Generic;
using System.IO;

namespace UIPanels.CharacterStats {

    [XmlRoot("StatsCollection")]
    public class StatsContainer {

        [XmlArray("StatsToPreserve")]
        [XmlArrayItem("StatPreserve")]
        public List<ToPreserveStat> StatsToPreserve = new List<ToPreserveStat>();

        public void Save(string path) {

            var serializer = new XmlSerializer(typeof(StatsContainer));
            var stream = new FileStream(path, FileMode.Create);
            serializer.Serialize(stream, this);
            stream.Close();

        }

        public static StatsContainer Load(string path) {

            var serializer = new XmlSerializer(typeof(StatsContainer));
            var stream = new FileStream(path, FileMode.Open);
            var container = serializer.Deserialize(stream) as StatsContainer;
            stream.Close();

            return container;

        }

    }

}