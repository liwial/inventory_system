﻿using UnityEngine;

namespace UIPanels.CharacterStats {

	[CreateAssetMenu(fileName = "New Archetype Stat", menuName = "Stats/Archetype")]
	public class ScriptableStatArchetype : ScriptableStat {

        public LMSType lmsType;

    }

}