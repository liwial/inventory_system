﻿using UnityEngine;

namespace UIPanels.CharacterStats {

	[CreateAssetMenu(fileName = "New Main Label Stat", menuName = "Stats/MainLabel")]
	public class ScriptableStatMainLabel : ScriptableStat {

	}

}