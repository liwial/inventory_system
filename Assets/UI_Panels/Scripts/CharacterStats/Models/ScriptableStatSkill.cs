﻿using UnityEngine;

namespace UIPanels.CharacterStats {

	[CreateAssetMenu(fileName = "New Skill Stat", menuName = "Stats/Skill")]
	public class ScriptableStatSkill : ScriptableStat {

        public LMSType lmsType;

    }

}