﻿using UnityEngine;

namespace UIPanels.CharacterStats {

	//[CreateAssetMenu(fileName = "New Stat", menuName = "Stats")]
	public class ScriptableStat : ScriptableObject {

		public StatType statType;
		public int id;
		public string nameUnique;
		public string nameToShow;

	}

}