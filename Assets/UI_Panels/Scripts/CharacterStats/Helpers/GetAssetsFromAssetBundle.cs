﻿using System.Collections.Generic;
using UnityEngine;

namespace UIPanels.CharacterStats {

    public class GetAssetsFromAssetBundle : MonoBehaviour {

        public List<ScriptableStat> GetAssets(string assetBundleName) {

            List<ScriptableStat> scriptableStatList = new List<ScriptableStat>();
            StartCoroutine(LoadAssetsFromAssetBundles.LoadAssetsFromAssetBundle(assetBundleName, scriptableStatList));

            return scriptableStatList;
        }
    }

}