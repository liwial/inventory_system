﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

namespace UIPanels.CharacterStats {

	public class LoadAssetsFromAssetBundles {

		private ScriptableStat scriptableStat;

		public static IEnumerator LoadAssetsFromAssetBundle(string assetBundleName, List<ScriptableStat> scriptableStatList) {

			string filePath = Path.Combine (Application.streamingAssetsPath, "AssetBundles");
			filePath = Path.Combine (filePath, assetBundleName);

			var assetBundleCreateRequest = AssetBundle.LoadFromFileAsync (filePath);
			yield return assetBundleCreateRequest;

			AssetBundle assetBundle = assetBundleCreateRequest.assetBundle;
			AssetBundleRequest asset = assetBundle.LoadAllAssetsAsync<ScriptableStat> ();
			Object[] tempAssetArray = asset.allAssets;

			foreach (var a in tempAssetArray)
				scriptableStatList.Add (a as ScriptableStat);

		}
			
		private IEnumerator LoadAsset(string assetBundleName, string objectNameToLoad) {

			string filePath = Path.Combine (Application.streamingAssetsPath, "AssetBundles");
			filePath = Path.Combine (filePath, assetBundleName);

			var assetBundleCreateRequest = AssetBundle.LoadFromFileAsync (filePath);
			yield return assetBundleCreateRequest;

			AssetBundle assetBundle = assetBundleCreateRequest.assetBundle;

			AssetBundleRequest asset = assetBundle.LoadAssetAsync<ScriptableStat> (objectNameToLoad);
			yield return asset;
		
			scriptableStat = asset.asset as ScriptableStat;

		}

    }

}