﻿using UnityEngine;
using TMPro;

namespace UIPanels.CharacterStats {

    public class StatVievMinMax : StatViev {

        public TextMeshProUGUI statValueSecondText;

        public override void SetStatTexts(ScriptableStat scriptableStat, ToPreserveStat statPreserved) {

            base.SetStatTexts(scriptableStat, statPreserved);

            UpdateValueSecondText(statPreserved.valueSecond);

        }

        public override void UpdateValueSecondText(int value) {

            statValueSecondText.text = value.ToString();

        }

    }

}