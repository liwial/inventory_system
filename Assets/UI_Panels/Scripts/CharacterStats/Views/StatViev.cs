﻿using UnityEngine;
using TMPro;

namespace UIPanels.CharacterStats {

    public class StatViev : MonoBehaviour {

        public TextMeshProUGUI statNameText;
        public TextMeshProUGUI statValueText;

        public virtual void SetStatTexts(ScriptableStat scriptableStat, ToPreserveStat statPreserved) {

            statNameText.text = scriptableStat.nameToShow;
            UpdateValueText(statPreserved.value);

        }

        public virtual void UpdateValueText(int value) {

            statValueText.text = value.ToString();

        }

        public virtual void UpdateValueSecondText(int value) { }

        public virtual void ActivateRemoveButton(bool active) { }

        public virtual void ActivateAddButton(bool active) { }

    }
}