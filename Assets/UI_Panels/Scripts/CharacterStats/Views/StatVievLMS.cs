﻿using UnityEngine;

namespace UIPanels.CharacterStats {

    public class StatVievLMS : StatViev {

        public GameObject removeButton;
        public GameObject addButton;

        public override void ActivateRemoveButton(bool active) {

            removeButton.SetActive(active);

        }

        public override void ActivateAddButton(bool active) {
            
            addButton.SetActive(active);

        }

    }

}