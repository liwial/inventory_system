﻿namespace UIPanels.CharacterStats {

    public class StatVievInterval : StatViev {

        private string GetState(int value) {

            int foodPoints = value;
            string resultText;


            if (foodPoints <= 0)
                resultText = "Starving";
            else if (foodPoints > 0 && foodPoints < 20)
                resultText = "Very Hungry";
            else if (foodPoints >= 20 && foodPoints < 40)
                resultText = "Hungry";
            else if (foodPoints >= 40 && foodPoints < 60)
                resultText = "Feeling Empty";
            else if (foodPoints >= 60 && foodPoints < 80)
                resultText = "Comfortable";
            else if (foodPoints >= 80)
                resultText = "Full";
            else
                resultText = string.Empty;

            return resultText;

        }

        public override void UpdateValueText(int value) {

            statValueText.text = GetState(value);

        }


    }

}