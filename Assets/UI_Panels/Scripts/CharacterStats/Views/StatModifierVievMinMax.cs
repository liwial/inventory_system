﻿using UnityEngine;
using TMPro;

namespace UIPanels.CharacterStats {

    public class StatModifierVievMinMax : StatModifierViev {

        public TextMeshProUGUI statValueSecondText;

        public override void SetStatModifierTexts(ScriptableStatModifier scriptableStatModifier, string valueText, string valueSecondText = null) {

            base.SetStatModifierTexts(scriptableStatModifier, valueText, valueSecondText);

            UpdateValueSecondText(valueSecondText);

        }

        public override void UpdateValueSecondText(string valueText) {

            statValueSecondText.text = valueText;

        } 

    }

}