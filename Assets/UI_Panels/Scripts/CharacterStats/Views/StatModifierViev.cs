﻿using UnityEngine;
using TMPro;

namespace UIPanels.CharacterStats {

    public class StatModifierViev : MonoBehaviour {

        public TextMeshProUGUI statNameText;
        public TextMeshProUGUI statValueText;

        public virtual void SetStatModifierTexts(ScriptableStatModifier scriptableStatModifier, string valueText, string valueSecondText = null) {

            statNameText.text = scriptableStatModifier.nameToShow;

            UpdateValueText(valueText);
        }

        public virtual void UpdateValueText(string valueText) {

            statValueText.text = valueText;

        }

        public virtual void UpdateValueSecondText(string valueText) { } 

    }

}