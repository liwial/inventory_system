﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UIPanels.CharacterStats {

    public class LMSManager : MonoBehaviour {

        public GameObject mainLabelsHolder;
        public GameObject mainOtherLabelsHolder;

        private StatsManager mainLabelsManager;
        private StatsManager mainOtherLabelsManager;

        private bool areStatsSet = false;
        private bool readyFirst = true;

        private int level;
        private int archetypePoints;
        private int skillPoints;

        private int skillMax = 20;

        private List<GameObject> archetypeAllObjets = new List<GameObject>();
        private List<GameObject> skillLoreObjets = new List<GameObject>();
        private List<GameObject> skillMightObjets = new List<GameObject>();
        private List<GameObject> skillSurvivalObjets = new List<GameObject>();

        #region Properties
        public int Level {
            get { return level; }
        }

        public int ArchetypePoints {
            get { return archetypePoints; }
        }

        public int SkillPoints {
            get { return skillPoints; }
        }
        #endregion

        #region MonoBehaviour Methods
        void Awake() {
            mainLabelsManager = mainLabelsHolder.GetComponent<StatsManager>();
            mainOtherLabelsManager = mainOtherLabelsHolder.GetComponent<StatsManager>();

            StartCoroutine(SetNeededStatValues());
        }

        void Update() {

        }
        #endregion

        #region Private Methods
        private IEnumerator SetNeededStatValues() {

            yield return StartCoroutine(mainLabelsManager.MakeSureStatsAreSetFirst());
            archetypePoints = mainOtherLabelsManager.GetStatValue("ArchetypePointsStat");
            skillPoints = mainOtherLabelsManager.GetStatValue("SkillPointsStat");

            yield return StartCoroutine(mainOtherLabelsManager.MakeSureStatsAreSetFirst());
            level = mainLabelsManager.GetStatValue("LevelStat");

            areStatsSet = true;
        }

        private void UpdateArchetypePoints() {
            archetypePoints = mainOtherLabelsManager.GetStatValue("ArchetypePointsStat");
        }

        private void UpdateSkillPoints() {
            skillPoints = mainOtherLabelsManager.GetStatValue("SkillPointsStat");
        }
        #endregion

        #region Public Methods
        public IEnumerator MakeSureStatsAreSetFirst() {

            yield return new WaitUntil(() => areStatsSet);

            if (readyFirst) {
                int childCount = transform.childCount;
                for (int i = 0; i < childCount; i++) {
                    if (transform.GetChild(i).GetComponent<StatController>().GetStatType() == StatType.ARCHETYPE)
                        archetypeAllObjets.Add(transform.GetChild(i).gameObject);
                    else if (transform.GetChild(i).GetComponent<StatController>().GetStatType() == StatType.SKILL) {

                        switch (transform.GetChild(i).GetComponent<StatController>().GetArchetypeType()) {
                            case LMSType.LORE:
                                skillLoreObjets.Add(transform.GetChild(i).gameObject);
                                break;
                            case LMSType.MIGHT:
                                skillMightObjets.Add(transform.GetChild(i).gameObject);
                                break;
                            case LMSType.SURVIVAL:
                                skillSurvivalObjets.Add(transform.GetChild(i).gameObject);
                                break;
                            default:
                                break;
                        }

                    }
                }

                readyFirst = false;
            }

        }

        public void IncreaseArchetypePoints() {
            mainOtherLabelsManager.IncreaseStatValue("ArchetypePointsStat");
            UpdateArchetypePoints();
        }

        public void DecreaseArchetypePoints() {
            mainOtherLabelsManager.DecreaseStatValue("ArchetypePointsStat");
            UpdateArchetypePoints();
        }

        public void IncreaseSkillPoints() {
            mainOtherLabelsManager.IncreaseStatValue("SkillPointsStat");
            UpdateSkillPoints();
        }

        public void DecreaseSkillPoints() {
            mainOtherLabelsManager.DecreaseStatValue("SkillPointsStat");
            UpdateSkillPoints();
        }

        public void PrepareAllAddButtonsArchetype() {
            foreach (var g in archetypeAllObjets) {
                if (g.GetComponent<StatController>().GetStatValue() < level)
                    g.GetComponent<StatVievLMS>().ActivateAddButton(true);
            }
        }

        public void PrepareAllAddButtonsSkill() {
            foreach (var g in skillLoreObjets) {
                if (g.GetComponent<StatController>().GetStatValue() < skillMax)
                    g.GetComponent<StatVievLMS>().ActivateAddButton(true);
            }
            foreach (var g in skillMightObjets) {
                if (g.GetComponent<StatController>().GetStatValue() < skillMax)
                    g.GetComponent<StatVievLMS>().ActivateAddButton(true);
            }
            foreach (var g in skillSurvivalObjets) {
                if (g.GetComponent<StatController>().GetStatValue() < skillMax)
                    g.GetComponent<StatVievLMS>().ActivateAddButton(true);
            }
        }

        public void ActivateAllAddButtonsArchetype(bool activate) {
            foreach (var g in archetypeAllObjets)
                g.GetComponent<StatVievLMS>().ActivateAddButton(activate);
        }

        public void ActivateAllRemoveButtonsArchetype(bool activate) {
            foreach (var g in archetypeAllObjets)
                g.GetComponent<StatVievLMS>().ActivateRemoveButton(activate);
        }

        public void ActivateAllAddButtonsSkill(bool activate) {
            foreach (var g in skillLoreObjets)
                g.GetComponent<StatVievLMS>().ActivateAddButton(activate);
            foreach (var g in skillMightObjets)
                g.GetComponent<StatVievLMS>().ActivateAddButton(activate);
            foreach (var g in skillSurvivalObjets)
                g.GetComponent<StatVievLMS>().ActivateAddButton(activate);
        }

        public void ActivateAllRemoveButtonsSkill(bool activate) {
            foreach (var g in skillLoreObjets)
                g.GetComponent<StatVievLMS>().ActivateRemoveButton(activate);
            foreach (var g in skillMightObjets)
                g.GetComponent<StatVievLMS>().ActivateRemoveButton(activate);
            foreach (var g in skillSurvivalObjets)
                g.GetComponent<StatVievLMS>().ActivateAddButton(activate);
        }

        public void AddToCorespondingSkills(LMSType lmsType) {

            switch(lmsType) {
                case LMSType.LORE:
                    foreach (var o in skillLoreObjets)
                        o.GetComponent<StatController>().IncreaseStatValue();
                    break;
                case LMSType.MIGHT:
                    foreach (var o in skillMightObjets)
                        o.GetComponent<StatController>().IncreaseStatValue();
                    break;
                case LMSType.SURVIVAL:
                    foreach (var o in skillSurvivalObjets)
                        o.GetComponent<StatController>().IncreaseStatValue();
                    break;
                default:
                    break;
            }

        }

        public void RemoveFromCorespondingSkills(LMSType lmsType) {

            switch (lmsType) {
                case LMSType.LORE:
                    foreach (var o in skillLoreObjets)
                        o.GetComponent<StatController>().DecreaseStatValue();
                    break;
                case LMSType.MIGHT:
                    foreach (var o in skillMightObjets)
                        o.GetComponent<StatController>().DecreaseStatValue();
                    break;
                case LMSType.SURVIVAL:
                    foreach (var o in skillSurvivalObjets)
                        o.GetComponent<StatController>().DecreaseStatValue();
                    break;
                default:
                    break;
            }

        }

        public void ClearButtonsAfterSession() {
            foreach (var g in archetypeAllObjets)
                g.GetComponent<StatControllerArchetype>().CleaPointsForSession();

            foreach (var g in skillLoreObjets)
                g.GetComponent<StatControllerSkill>().CleaPointsForSession();
            foreach (var g in skillMightObjets)
                g.GetComponent<StatControllerSkill>().CleaPointsForSession();
            foreach (var g in skillSurvivalObjets)
                g.GetComponent<StatControllerSkill>().CleaPointsForSession();

            ActivateAllRemoveButtonsArchetype(false);
            ActivateAllRemoveButtonsSkill(false);
        }
        #endregion

    }

}