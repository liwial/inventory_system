﻿using System.Collections.Generic;
using UIPanels.Gameplay;
using UnityEngine;

namespace UIPanels.CharacterStats {

    public class CharacterStatsManager : MonoBehaviour {

        public CanvasGroup canvasGroup;
        private LMSManager lmsManager;

        private List<GameObject> holders = new List<GameObject>();
        private bool isVisible = false;

        #region MonoBehaviour Methods
        void Start() {

            int childCount = transform.childCount;
            for (int i = 0; i < childCount; i++) {

                if (transform.GetChild(i).GetComponent<StatsManager>())
                    holders.Add(transform.GetChild(i).gameObject);

                if (transform.GetChild(i).GetComponent<LMSManager>())
                    lmsManager = transform.GetChild(i).GetComponent<LMSManager>();

            }

        }

        void Update() {

        }
        #endregion

        #region Private Methods
        private void SaveStats() {

            foreach (var h in holders)
                h.GetComponent<StatsManager>().SaveStats();

        }

        private void ClearLMSButtons() {
            lmsManager.ClearButtonsAfterSession();
        }
        #endregion

        public void SetVisibility() {

            isVisible = !isVisible;

            UIGameplayManager.SetCanvasGroup(canvasGroup, isVisible);

            if (!isVisible) {
                // SaveStats();
                ClearLMSButtons();

                // temp slution
                InventoryManager.Instance.ShortcutBarHolder.SetActive(true);
                InventoryManager.Instance.GameUIStates = UIStates.DEFAULT;
            } else {
                // temp slution
                InventoryManager.Instance.PromptObject.SetActive(false);
                InventoryManager.Instance.ShortcutBarHolder.SetActive(false);
                InventoryManager.Instance.GameUIStates = UIStates.CHARACTER_STATS;
            }
            

        }


    }

}