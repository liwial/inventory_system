﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace UIPanels.CharacterStats {

    [RequireComponent(typeof(VerticalLayoutGroup))]
	public class StatsModifiersManager : MonoBehaviour {

        public GameObject statModOnePrefab;
        public GameObject statModMinMaxPrefab;

        public string[] assetBundleNames;

        private bool isStatSet = false;

        private List<List<ScriptableStat>> scriptableStatList = new List<List<ScriptableStat>>();
        private List<List<ScriptableStatModifier>> scriptableStatModifierList = new List<List<ScriptableStatModifier>>();
        private List<GameObject> statObjectsList = new List<GameObject>();


        #region MonoBehaviour Methods
        void Awake () {

            foreach (var n in assetBundleNames) {
                List<ScriptableStat> tmpList = GetAssetsFromAssetBundle(n);
                scriptableStatList.Add(tmpList);
            }

        }

        void Start() {

            foreach (var list in scriptableStatList) {
                List<ScriptableStatModifier> statModList = new List<ScriptableStatModifier>();

                foreach (var t in list) {
                    statModList.Add((ScriptableStatModifier)t);
                }

                scriptableStatModifierList.Add(statModList);
            }


            foreach (var list in scriptableStatModifierList)
                list.Sort((s1, s2) => s1.id.CompareTo(s2.id));


            for (int i = 0; i < assetBundleNames.Length; i++) {

                foreach (var stat in scriptableStatModifierList[i]) {

                    switch (stat.statModifierType) {

                        case StatModifierType.MINMAX_NONE:
                            CreateStatObject(i, statModMinMaxPrefab, stat);
                            break;
                        case StatModifierType.MINMAX_PERCENT:
                            CreateStatObject(i, statModMinMaxPrefab, stat);
                            break;
                        case StatModifierType.ONE_NONE:
                            CreateStatObject(i, statModOnePrefab, stat);
                            break;
                        case StatModifierType.ONE_PERCENT:
                            CreateStatObject(i, statModOnePrefab, stat);
                            break;
                        case StatModifierType.ONE_SECOND:
                            CreateStatObject(i, statModOnePrefab, stat);
                            break;
                        case StatModifierType.INTERVAL:
                            CreateStatObject(i, statModOnePrefab, stat);
                            break;
                        default:
                            break;
                    }

                }
            }

            RegulatePanelHeight();

            isStatSet = true;
        }

        void Update() {

        }
        #endregion

        #region Private Methods
        private List<ScriptableStat> GetAssetsFromAssetBundle(string assetBundleName) {

            gameObject.AddComponent<GetAssetsFromAssetBundle>();
            GetAssetsFromAssetBundle g = GetComponent<GetAssetsFromAssetBundle>();

            return g.GetAssets(assetBundleName);
        }

        private void CreateStatObject(int index, GameObject prefab, ScriptableStatModifier scriptableStat) {

            GameObject statObject = (GameObject)Instantiate(prefab, transform.position, Quaternion.identity);
            statObject.name = scriptableStat.nameUnique;
            statObject.GetComponent<RectTransform>().SetParent(transform, true);
            statObject.GetComponent<RectTransform>().localScale = new Vector3(1.0f, 1.0f, 1.0f);

            statObject.GetComponent<StatModifierController>().SetStat(scriptableStat);

            statObjectsList.Add (statObject);
		}

        private void RegulatePanelHeight() {
            float spacing = GetComponent<VerticalLayoutGroup>().spacing;
            int childCount = transform.childCount;
            float panelHeight = 0f;

            for (int i = 0; i < childCount; i++)
                panelHeight += transform.GetChild(i).GetComponent<RectTransform>().sizeDelta.y + spacing;
            
            panelHeight -= spacing;

            GetComponent<RectTransform>().sizeDelta = new Vector2(GetComponent<RectTransform>().sizeDelta.x, panelHeight);
        }
        #endregion

        public IEnumerator MakeSureStatsAreSetFirst() {

            yield return new WaitUntil(() => isStatSet);

        }

        public int GetStatValue(string statNameUnique) {

            //TODO

            return 0;

        }

        public int GetStatValueSecond(string statNameUnique) {

            //TODO

            return 0;

        }

    }

}