﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UIPanels.Gameplay;
using UnityEngine;
using UnityEngine.UI;

namespace UIPanels.CharacterStats {

    [RequireComponent(typeof(VerticalLayoutGroup))]
	public class StatsManager : MonoBehaviour {

        public GameObject statPrefab;
        public GameObject statMinMaxPrefab;
        public GameObject statIntervalPrefab;
        public GameObject archetypePrefab;
        public GameObject skillPrefab;

        public string[] assetBundleNames; // used also for xml files
        private string saveFolderPath;

        private bool isStatSet = false;

        protected List<List<ScriptableStat>> scriptableStatList = new List<List<ScriptableStat>> ();

        private List<List<ToPreserveStat>> preservedStatsList = new List<List<ToPreserveStat>>();
        protected List<List<ToPreserveStat>> preservedStatsAfterList = new List<List<ToPreserveStat>>();

        protected List<GameObject> statObjectsList = new List<GameObject>();

        private List<bool> saveDataExists = new List<bool>();


        #region MonoBehaviour Methods
        protected virtual void Awake () {

            foreach (var n in assetBundleNames) {
                List<ScriptableStat> tmpList = GetAssetsFromAssetBundle(n);
                scriptableStatList.Add(tmpList);

                saveDataExists.Add(false);

                preservedStatsAfterList.Add(new List<ToPreserveStat>());
            }

        }

        void Start() {

            saveFolderPath = UIGameplayManager.StatsSaveFolderPath;

            foreach (var list in scriptableStatList)
                list.Sort((s1, s2) => s1.id.CompareTo(s2.id));

            for (int i = 0; i < assetBundleNames.Length; i++) {

                // loading save data from xml if exists
                if (File.Exists(Path.Combine(saveFolderPath, assetBundleNames[i]))) {

                    StatsContainer statsContainer = StatsContainer.Load(Path.Combine(saveFolderPath, assetBundleNames[i]));
                    List<ToPreserveStat> preservedStatsListAsset = new List<ToPreserveStat>(statsContainer.StatsToPreserve);

                    preservedStatsList.Add(preservedStatsListAsset);

                    saveDataExists[i] = true;
                }


                foreach (var stat in scriptableStatList[i]) {

                    if (saveDataExists[i]) {
                        ToPreserveStat statPreserved = GetStatPreserved(stat, i); // finding for every stat corresponding xml fragment

                        switch (stat.statType)
                        {
                            case StatType.ONE_VALUE:
                                CreateStatObject(i, statPrefab, stat, statPreserved);
                                break;
                            case StatType.MINMAX_VALUE:
                                CreateStatObject(i, statMinMaxPrefab, stat, statPreserved);
                                break;
                            case StatType.INTERVAL_VALUE:
                                CreateStatObject(i, statIntervalPrefab, stat, statPreserved);
                                break;
                            case StatType.ARCHETYPE:
                                CreateStatObject(i, archetypePrefab, stat, statPreserved);
                                break;
                            case StatType.SKILL:
                                CreateStatObject(i, skillPrefab, stat, statPreserved);
                                break;
                            default:
                                break;
                        }
                    } else {
                        switch (stat.statType)
                        {
                            case StatType.ONE_VALUE:
                                CreateStatObject(i, statPrefab, stat);
                                break;
                            case StatType.MINMAX_VALUE:
                                CreateStatObject(i, statMinMaxPrefab, stat);
                                break;
                            case StatType.INTERVAL_VALUE:
                                CreateStatObject(i, statIntervalPrefab, stat);
                                break;
                            case StatType.ARCHETYPE:
                                CreateStatObject(i, archetypePrefab, stat);
                                break;
                            case StatType.SKILL:
                                CreateStatObject(i, skillPrefab, stat);
                                break;
                            default:
                                break;
                        }
                    }

                }
            }

            RegulatePanelHeight();

            isStatSet = true;
        }

        void Update() {

            // DEBUG STUFF

            if (Input.GetKeyDown(KeyCode.P)) {

                TestIncreasingValues();

            }

            if (Input.GetKeyDown(KeyCode.S)) {

                SaveStats();

            }

        }
        #endregion

        #region Private Methods
        private List<ScriptableStat> GetAssetsFromAssetBundle(string assetBundleName) {

            gameObject.AddComponent<GetAssetsFromAssetBundle>();
            GetAssetsFromAssetBundle g = GetComponent<GetAssetsFromAssetBundle>();

            return g.GetAssets(assetBundleName);

        }

        private ToPreserveStat GetStatPreserved(ScriptableStat stat, int index) {

            ToPreserveStat statPreserved = new ToPreserveStat();

            if (preservedStatsList[index].Count != 0) {

                foreach (var p in preservedStatsList[index]) {
                    if (p.nameUnique == stat.nameUnique) {
                        statPreserved = p;
                        break;
                    }

                }

            }

            return statPreserved;
        }

        private void RegulatePanelHeight() {

            float spacing = GetComponent<VerticalLayoutGroup>().spacing;
            int childCount = transform.childCount;
            float panelHeight = 0f;

            for (int i = 0; i < childCount; i++)
                panelHeight += transform.GetChild(i).GetComponent<RectTransform>().sizeDelta.y + spacing;

            panelHeight -= spacing;

            GetComponent<RectTransform>().sizeDelta = new Vector2(GetComponent<RectTransform>().sizeDelta.x, panelHeight);

        }

        // for Debug
        private void TestIncreasingValues() {

            IncreaseStatValue("ArchetypePointsStat");

            /*
            foreach (var s in statObjectsList) { 
                s.GetComponent<StatController>().IncreaseStatValue();

                if (s.GetComponent<StatControllerMinMax>())
                    s.GetComponent<StatControllerMinMax>().IncreaseStatValueSecond();
            }
            */

        }
        #endregion

        #region Protected Methods
        protected virtual void CreateStatObject(int index, GameObject prefab, ScriptableStat scriptableStat, ToPreserveStat statPreserved = null) {

            GameObject statObject = (GameObject)Instantiate(prefab, transform.position, Quaternion.identity);
            statObject.name = scriptableStat.nameUnique;
            statObject.GetComponent<RectTransform>().SetParent(transform, true);
            statObject.GetComponent<RectTransform>().localScale = new Vector3(1.0f, 1.0f, 1.0f);


            if (statPreserved == null) {
                if (scriptableStat.statType == StatType.MINMAX_VALUE)
                    statPreserved = new ToPreserveStat(scriptableStat.nameUnique, 0, 0);
                else
                    statPreserved = new ToPreserveStat(scriptableStat.nameUnique, 0);

            }

            statObject.GetComponent<StatController>().SetStat(scriptableStat, statPreserved);

            preservedStatsAfterList[index].Add(statPreserved);


            statObjectsList.Add(statObject);
        }
        #endregion

        #region Public Methods
        public void SaveStats() {

            for (int i = 0; i < preservedStatsAfterList.Count; i++) {
                StatsContainer statsContainer = new StatsContainer();
                statsContainer.StatsToPreserve = new List<ToPreserveStat>(preservedStatsAfterList[i]);

                if (Directory.Exists(saveFolderPath))
                    statsContainer.Save(Path.Combine(saveFolderPath, assetBundleNames[i]));
            }

        }

        public void IncreaseStatValue(string statNameUnique) {

            foreach (var s in statObjectsList) {
                if (s.GetComponent<StatController>().GetStatNameUnique() == statNameUnique)
                    s.GetComponent<StatController>().IncreaseStatValue();
            }

        }

        public void IncreaseStatValueSecond(string statNameUnique) {

            foreach (var s in statObjectsList) {
                if (s.GetComponent<StatController>().GetStatNameUnique() == statNameUnique && s.GetComponent<StatControllerMinMax>())
                    s.GetComponent<StatControllerMinMax>().IncreaseStatValueSecond();
            }

        }

        public void DecreaseStatValue(string statNameUnique) {

            foreach (var s in statObjectsList) {
                if (s.GetComponent<StatController>().GetStatNameUnique() == statNameUnique)
                    s.GetComponent<StatController>().DecreaseStatValue();
            }

        }

        public void DecreaseStatValueSecond(string statNameUnique) {

            foreach (var s in statObjectsList) {
                if (s.GetComponent<StatController>().GetStatNameUnique() == statNameUnique && s.GetComponent<StatControllerMinMax>())
                    s.GetComponent<StatControllerMinMax>().DecreaseStatValueSecond();
            }

        }

        public IEnumerator MakeSureStatsAreSetFirst() {

            yield return new WaitUntil(() => isStatSet);

        }

        public int GetStatValue(string statNameUnique) {

            foreach (var list in preservedStatsAfterList) {
                foreach (var p in list) {
                    if (p.nameUnique == statNameUnique)
                        return p.value;
                }
            }

            return 0;

        }

        public int GetStatValueSecond(string statNameUnique) {

            foreach (var list in preservedStatsAfterList)
            {
                foreach (var p in list)
                {
                    if (p.nameUnique == statNameUnique)
                        return p.valueSecond;
                }
            }
            return 0;

        }
        #endregion

    }

}