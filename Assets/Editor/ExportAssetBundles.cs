﻿using UnityEngine;
using UnityEditor;
using System.IO;

namespace UIPanels {

	// After creation of all necessary Assets for AssetBundles remember to use 
	// 'Assets' -> 'Build AssetBundles' in Editor
	public class ExportAssetBundles {

		[MenuItem("Assets/Build AssetBundles")]
		static void ExportResource() {

			string folderName = "AssetBundles";
			string filePath = Path.Combine (Application.streamingAssetsPath, folderName);

			BuildPipeline.BuildAssetBundles (filePath, BuildAssetBundleOptions.None, BuildTarget.StandaloneWindows64);

			//for LL from the future - put here code to build for other platforms

			AssetDatabase.Refresh ();
		}

	}

}