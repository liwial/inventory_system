# Inventory, Trade, Character Stats systems for 2D Action RPG game

Project uses Unity 2018.3.7.

_Inventory Window_

 - It consists of two tabs. One is for general, the other one is for quest related items. Both tabs got 8x8 slots
 - Items can be equipped / used by pressing right mouse button (RMB)
 - Equipped items got different backgrounds than not equipped ones
 - Items can be moved by pressing left mouse button (LMB)
 - Items with 'stackable' property and quantity greather than 1 can be divided by pressing Shift + LMB

_Items_

 - All items parameters can be found in prepared XML files
 - In one moment it can be equipped one weapon, one armor, one pair of boots and one type of ammo
 - Items with 'stackable' property can be put in amount of 1-99 on one slot, other items in amount of 1
 - Items with 'shortcutable' property can be put on Shortcut Bar on the bottom of the screen
 - Moving items on occupied slots causes the swap of slots

![alt text](Screenshots/lms01.png "")
Inventory, Enemy Chest and Skill Windows

_Enemy Chest / Chest Window_

 - Items can be taken from Chest to Inventory by pressing LMB
 - They can be also taken by pressing button 'Take All'
 - Player located in the area of Chest's trigger gets info that by pressing key (temporary 'F') they can open this chest.
 - In case of standard enemy items are generated randomly
 - In case of special enemy generated items are specified

_Shortcut Bar_

 - Player can put here weapons, ammo, consumables, traps and skills
 - Icons can be moved by pressing LMB
 - Icons can be activated by pressing RMB or keys 0-9
    
_Skill Window_

 - Skills can be in one of three states: Invisible, Active and Inactive
 - Active skills can be used by player
 - Inactive skills lacks some of requirements to use them
 - Active and Inactive skills can be moved to Shortcut Bar
 - Active and Inactive skills changes their states when the equipped weapon is changing
    
_Queue_

 - Can keep up to ten skills
 - Skill in Queue is released after pressing key (temporary 'R')
 - In case of changing the weapon, queue is cleared 

![alt text](Screenshots/lms02.png "")
Trade Window

_Trade Window_

 - Pressing LMB on item in inventory causes moving that item to trade space on the hero's side
 - Pressing LMB on item in trade space on the hero's side causes moving that item to inventory
 - Everything works similary for trader side
 - Bargain is possible when sum on the side of hero is bigger than sum of trader side (otherwise, there is shown info that bargain is unacceptable)
 - In case that trader doesn't have enough coins to give the player -> question appears about if continue the bargain or cancel it

![alt text](Screenshots/lms03.png "")
Character Stats Window

_Character Stats Window_

 - To Archetypes can be added max 12 points
 - To Skills can be added max 8 points
 - In case there is no archetype or skill points for disposal, there are no buttons '+' visible
 - Buttons '-' are present only to change recently added points. After Character Stats Window is closed buttons '-' are not gonna be active.
 - Player Level is simultaneously the maximum value that can be added to Archetypes and to individual skills
 - Combat Modifiers are changing depending on points added to Archetypes and Skills and equipped weapon

Catalog with executable files for Windows systems available here:

https://my.pcloud.com/publink/show?code=XZiO867Zo2PV5FCOeDywAC5nVeTdv8zxPDmy